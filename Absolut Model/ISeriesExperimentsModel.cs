﻿using System;
using System.Collections.Generic;
using Absolut_Model.CreationStrategies;
using Properties;

namespace Absolut_Model
{
    public interface ISeriesExperimentsModel
    {
        void Subscribe(Guid id, ISeriesEventsListener listener);
        void Unsubscribe(Guid id, ISeriesEventsListener listener);
        void Subscribe(ISeriesEventsListener listener);
        void Unsubscribe(ISeriesEventsListener listener);
        Guid CreateNewSeries(string experimentName, ISeriesCreator nameCreator = null);
        void DeleteSeries(Guid id);
        void StartNextInSeries(Guid id);
        SeriesExperimentResult GetSeriesExperimentResult(Guid id);
        ExperimentResult GetLastExperimentInfo(Guid id);


        void StartSeries(Guid id);
        void ShowSubExperiment(Guid id, Guid subId);
        List<PropertyInfo> GetProperties(Guid id);
        void SetProperties(Guid id, Dictionary<string, object> propsValues);
        object GetProperty(string propertyName, Guid id);
    }
}