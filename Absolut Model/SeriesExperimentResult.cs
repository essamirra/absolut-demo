﻿using System;
using System.Collections.Generic;
using System.Drawing;


namespace Absolut_Model
{
    public class SeriesExperimentResult
    {
        public int ExperimentsCount { get; set; }
        public Dictionary<Guid, Tuple<int, bool>> WasProblemSolved;
        public List<PointF> OperationalChars = new List<PointF>();

    }
}