﻿using System;
using System.Collections.Generic;
using System.Linq;
using Algorithms;
using Algorithms.Methods;
using Functions;
using Properties;

namespace Absolut_Model
{
    public class Model : IModel
    {
        //private readonly AlgFactory _algs;
        //private readonly FuncFactory _func;
        private readonly List<IEventListener> _commonSubs;
        private readonly Dictionary<Guid, KeyValuePair<string, ISearchAlg>> _experiments;
        private readonly Dictionary<Guid, List<IEventListener>> _subscribers;
        private Dictionary<Guid,Dictionary<int, IFunction>> _slices = new Dictionary<Guid, Dictionary<int, IFunction>>();

        public Model()
        {
            _subscribers = new Dictionary<Guid, List<IEventListener>>();
            _experiments = new Dictionary<Guid, KeyValuePair<string, ISearchAlg>>();
            _commonSubs = new List<IEventListener>();
            _slices = new Dictionary<Guid, Dictionary<int, IFunction>>();
        }

        #region Operations with Experiments

        public MethodPoint GetLastPoint(Guid experimentId)
        {
            if (!_experiments.ContainsKey(experimentId))
                throw new ArgumentException("GetLastPoint failed - alg with that id doesnt exists");
            return _experiments[experimentId].Value.LastPoint();
        }

        public Guid AddExperiment(string name, ISearchAlg alg)
        {
            var id = Guid.NewGuid();
            _experiments.Add(id, new KeyValuePair<string, ISearchAlg>(name, alg));
            _subscribers.Add(id, new List<IEventListener>());
            _slices.Add(id, new Dictionary<int, IFunction>());
            ExperimentAdded();
            return id;
        }

        public void DeleteExperiment(Guid experimentId)
        {
            if (!_experiments.ContainsKey(experimentId))
                throw new ArgumentException("DeleteExperiment failed - alg with that id doesnt exists");
            _subscribers[experimentId].Clear();
            _subscribers.Remove(experimentId);
            _experiments.Remove(experimentId);
            _slices.Remove(experimentId);
            ExperimentDeleted();
        }


        public IEnumerable<MethodPoint> GetExpEnumerator(Guid id, int start = 0)
        {
            return _experiments[id].Value.CalculatedPoints(start);
        }

        public ISearchAlg GetAlg(Guid id)
        {
            if (_experiments.ContainsKey(id))
            {
                return _experiments[id].Value;
            }
            throw new ArgumentException("GetAlg failed - alg with that id doesnt exists");
        }

        public void SetAlg(Guid experimentId, ISearchAlg alg)
        {
            if (_experiments.ContainsKey(experimentId))
            {
                var name = _experiments[experimentId].Key;
                _experiments.Remove(experimentId);
                _experiments.Add(experimentId, new KeyValuePair<string, ISearchAlg>(name, alg));
                AlgChanged(experimentId);
            }
            else
            {
                throw new ArgumentException("SetAlg failed - alg with that id doesnt exists");
            }
        }

        #endregion

        #region Events handlers

        public void SubscribeExperiment(IEventListener e, Guid experimentId)
        {
            if (_experiments.ContainsKey(experimentId))
                _subscribers[experimentId].Add(e);
            else throw new ArgumentException("SubscribeExperiment failed - alg with that id doesnt exists");
        }

        private void UnsubscribeExperiment(IEventListener e, Guid experimentId)
        {
            if (_experiments.ContainsKey(experimentId))
                _subscribers[experimentId].Remove(e);
            else throw new ArgumentException("UnbscribeExperiment failed - alg with that id doesnt exists");
        }

        private void AlgChanged(Guid experimentId)
        {
            if (_experiments.ContainsKey(experimentId))
            {
                foreach (var e in _subscribers[experimentId])
                {
                    e.OnMethodChanged();
                    e.OnPointsReset();
                }
            }
            else throw new ArgumentException("AlgCnhanged failed - alg with that id doesnt exists");
        }

        private void ExperimentAdded()
        {
            foreach (var e in _subscribers.SelectMany(@event => @event.Value))
                e.OnExperimentAdded();
            foreach (var r in _commonSubs)
                r.OnExperimentAdded();
        }

        private void ExperimentDeleted()
        {
            foreach (var e in _subscribers.SelectMany(@event => @event.Value))
                e.OnExperimentDeleted();
            foreach (var r in _commonSubs)
            {
                r.OnExperimentDeleted();
            }
        }

        private void PointsReset()
        {
            foreach (var e in _subscribers.SelectMany(@event => @event.Value))
                e.OnPointsReset();
        }

        public void NextStep(Guid algId)
        {
            _experiments[algId].Value.NextStep();
            foreach (var e in _subscribers[algId])
                e.OnNextStep();
        }

        public void PrevStep(Guid experimentId)
        {
            _experiments[experimentId].Value.PrevStep();
            foreach (var e in _subscribers[experimentId])
                e.OnPrevStep();
        }

        private void FunctionChanged()
        {
            foreach (var e in _subscribers.SelectMany(@event => @event.Value))
            {
                //e.OnMainFunctionChanged();
                e.OnProblemChanged();
            }
        }

        #endregion

        #region Set data in model

        public void SetProperty(string name, object value, Guid id)
        {
            if(!_experiments.ContainsKey(id)) throw new ArgumentException();
            _experiments[id].Value.SetProperty(name, value);
            AlgChanged(id);
            if (name == "Function")
            {
                throw new InvalidOperationException("DO NOT USE IT");
                FillSlices((IFunction)value, id);
                FunctionChanged();
            }
        }

        #endregion

        #region Get data from model

        public object GetProperty(string name, Guid id)
        {
            if(name=="Function") throw new InvalidOperationException("DO NOT USE IT");
            if (!_experiments.ContainsKey(id)) throw new ArgumentException();
            return _experiments[id].Value.GetProperty(name);
        }

        public string GetMethodName(Guid id)
        {
            return _experiments[id].Value.Name;
        }

        public List<PropertyInfo> GetPropsNames(Guid id)
        {
            return _experiments[id].Value.GetPropertiesInfo();
        }

        public MethodPoint GetBestPoint(Guid id)
        {
            return _experiments[id].Value.ArgminTask();
        }

        public int GetCurrentInter(Guid experimentId)
        {
            return _experiments[experimentId].Value.CurrentIter();
        }

        public void SetProperties(Guid experimentId, Dictionary<string, object> propsValues)
        {
            if(!_experiments.ContainsKey(experimentId)) throw new ArgumentException();
            _experiments[experimentId].Value.SetProperties(propsValues);
            AlgChanged(experimentId);
        }

        public bool HasNext(Guid experimentId)
        {
            if (!_experiments.ContainsKey(experimentId)) throw new ArgumentException();
            return _experiments[experimentId].Value.HasNext();
        }

        public void Subscribe(IEventListener e)
        {
            _commonSubs.Add(e);
        }

        public void Unsubcribe(IEventListener e)
        {
            _commonSubs.Remove(e);
        }

        public Dictionary<Guid, string> GetExperimentsNames()
        {
            return _experiments.ToDictionary(e => e.Key, e => e.Value.Key);
        }

        public void SetSlicesDictionary(Dictionary<int, IFunction> slices, Guid expId)
        {
            _slices[expId] = slices;
            foreach (var sub in _subscribers[expId])
            {
                sub.OnSlicesChanged();
            }
        }

        public Dictionary<int, IFunction> GetSlicesDictionary(Guid expId)
        {
            return _slices[expId];
        }

        private void FillSlices(IFunction newFunction, Guid id)
        {
            if (newFunction.Left.Count <= 2)
            {
                _slices[id].Clear();
            }
            else
            {
                _slices[id].Clear();
                for (int i = 0; i < newFunction.Left.Count-2; i++)
                {
                    double fixedVarValue = (newFunction.Right[i] + newFunction.Left[i]) / 2;
                    _slices[id].Add( i, new Plane(0,0,fixedVarValue, newFunction.Left.GetRange(0,newFunction.Left.Count-2), 
                        newFunction.Right.GetRange(0, newFunction.Right.Count - 2)));
                }
            }
        }

        public IProblem GetProblem(Guid experimentId)
        {
            return _experiments[experimentId].Value.Problem;
        }

        public void SetProblem(IProblem problem, Guid experimentId)
        {
            _experiments[experimentId].Value.Problem = problem;
            _experiments[experimentId].Value.RecalcPoints();
            AlgChanged(experimentId);
            FunctionChanged();
        }

        #endregion
    }
}