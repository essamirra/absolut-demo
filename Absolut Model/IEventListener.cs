﻿namespace Absolut_Model
{
    public interface IEventListener
    {
        void OnPointsReset();
        [System.Obsolete("use onProblemChanged", true)]
        void OnMainFunctionChanged();
        void OnSlicesChanged();
        void OnMethodChanged();
        void OnNextStep();
        void OnPrevStep();
        void OnExperimentAdded();
        void OnExperimentDeleted();
        void OnProblemChanged();
    }
}