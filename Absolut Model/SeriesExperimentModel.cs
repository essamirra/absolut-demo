﻿using System;
using System.Collections.Generic;
using Absolut_Model.CreationStrategies;
using Properties;

namespace Absolut_Model
{
    public class SeriesExperimentModel : ISeriesExperimentsModel
    {
        private Dictionary<Guid, List<ISeriesEventsListener>> _subscribers =
            new Dictionary<Guid, List<ISeriesEventsListener>>();

        private Dictionary<Guid, SeriesExperiment> _experiments = new Dictionary<Guid, SeriesExperiment>();
        private List<ISeriesEventsListener> _overallSubscribers = new List<ISeriesEventsListener>();

        public void Subscribe(Guid id, ISeriesEventsListener listener)
        {
            if (!_subscribers.ContainsKey(id))
            {
                _subscribers.Add(id, new List<ISeriesEventsListener>());
            }
            _subscribers[id].Add(listener);
        }

        public void Unsubscribe(Guid id, ISeriesEventsListener listener)
        {
            _subscribers[id].Remove(listener);
            if (_subscribers[id].Count == 0)
                _subscribers.Remove(id);
        }

        public Guid CreateNewSeries(string experimentName, ISeriesCreator nameCreator = null)
        {
            var key = Guid.NewGuid();
            SeriesExperiment exp;
            if (nameCreator == null)
                exp = new SeriesExperiment() {Name = experimentName, Id = key};
            else
            {
                exp = nameCreator.Create();
                exp.Name = experimentName;
                exp.Id = key;
            }

            _experiments.Add(key, exp);

            foreach (var seriesEventsListener in _overallSubscribers)
            {
                seriesEventsListener.OnNewExperimentAdded(key, experimentName, exp.Count);
            }

            return key;
        }

        public void DeleteSeries(Guid id)
        {
            _experiments.Remove(id);

            foreach (var seriesEventsListener in _overallSubscribers)
            {
                seriesEventsListener.OnExperimentDeleted(id);
            }
        }

        public void StartNextInSeries(Guid id)
        {
            var isThereNext = _experiments[id].StartNext();
            if (!isThereNext)
            {
                foreach (var seriesEventsListener in _subscribers[id])
                {
                    seriesEventsListener.OnSeriesFinished(id);
                }
                return;
            }

            foreach (var seriesEventsListener in _subscribers[id])
            {
                seriesEventsListener.OnSubExperimentStarted(id, _experiments[id].GetCurrentSubExperiment(),
                    _experiments[id].GetCurrentSubExperimentId());
            }

            var result = _experiments[id].StartCurrent();

            foreach (var seriesEventsListener in _subscribers[id])
            {
                seriesEventsListener.OnSubExperimentFinished(id, result);
            }
        }

        public SeriesExperimentResult GetSeriesExperimentResult(Guid id)
        {
            return _experiments[id].GetSeriesExperimentResult();
        }

        public ExperimentResult GetLastExperimentInfo(Guid id)
        {
            return _experiments[id].GetLastExperimentInfo();
        }

        public void Subscribe(ISeriesEventsListener listener)
        {
            _overallSubscribers.Add(listener);
        }

        public void Unsubscribe(ISeriesEventsListener listener)
        {
            _overallSubscribers.Remove(listener);
        }

        public void StartSeries(Guid id)
        {
            _experiments[id].Start();
            foreach (var seriesEventsListener in _subscribers[id])
            {
                seriesEventsListener.OnSeriesStarted(id);
            }
        }

        public void ShowSubExperiment(Guid id, Guid subId)
        {
            foreach (var seriesEventsListener in _overallSubscribers)
            {
                seriesEventsListener.OnShowSubExperiment(_experiments[id][subId]);
            }
        }

        public List<PropertyInfo> GetProperties(Guid id)
        {
            return _experiments[id].GetPropertiesInfo();
        }

        public void SetProperties(Guid id, Dictionary<string, object> propsValues)
        {
            _experiments[id].SetProperties(propsValues);
        }

        public object GetProperty(string propertyName, Guid id)
        {
            return _experiments[id].GetProperty(propertyName);
        }
    }
}