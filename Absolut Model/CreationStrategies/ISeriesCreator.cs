﻿namespace Absolut_Model.CreationStrategies
{
    public interface ISeriesCreator
    {
        SeriesExperiment Create();
        void SetCount(int count);
        void UseMethod(string name);
        void UseExamin(string examinExaminPath, string examinDllPath, string examinConfigPath);
    }
}