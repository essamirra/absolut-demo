﻿using System;
using Algorithms;

namespace Absolut_Model
{
    public interface ISeriesEventsListener
    {
        void OnNewExperimentAdded(Guid id, string name, int count);
        void OnExperimentDeleted(Guid id);
        void OnSubExperimentStarted(Guid id, ISearchAlg subId, Guid subExpId);
        void OnSubExperimentFinished(Guid id, ExperimentResult subId);
        void OnSeriesStarted(Guid id);
        void OnSeriesFinished(Guid id);
        void OnShowSubExperiment(ISearchAlg alg);

    }
}