﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Drawing;
using System.Linq;
using Algorithms;
using Functions;
using Properties;

namespace Absolut_Model
{
    public class SeriesExperiment : IPropertyUser
    {
        public const string PathToGrishagin = "grishagin.dll";
        public const string PathToGrishaginConf = "grishagin_conf.xml";
        private Guid _id;
        private Dictionary<Guid, ISearchAlg> _algs = new Dictionary<Guid, ISearchAlg>();
        private Dictionary<Guid, ISearchAlg>.Enumerator _enumerator;
        private double epsilon = 0.01;
        private int _expCount;
        public string Name { get; set; }

        public Guid Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public int Count
        {
            get { return _expCount; }
            set { _expCount = value; }
        }

        public SeriesExperiment(int count = 100)
        {
            _expCount = count;
            for (int i = 1; i <= count; i++)
            {
                var searchAlg = AlgFactory.Build(MethodsNames.Agp);

                searchAlg.Problem = ProblemFactory.BuildGKLS(i);
                _algs.Add(Guid.NewGuid(), searchAlg);
            }

            _enumerator = _algs.GetEnumerator();
        }

        public SeriesExperiment(List<ISearchAlg> algs)
        {
            _expCount = algs.Count;
            foreach (var a in algs)
            {
                _algs.Add(Guid.NewGuid(), a);
            }
            _enumerator = _algs.GetEnumerator();
        }

        public bool StartNext()
        {
            return _enumerator.MoveNext();
        }

        public ISearchAlg GetCurrentSubExperiment()
        {
            return _enumerator.Current.Value;
        }

        public ExperimentResult StartCurrent()
        {
            DateTime time = DateTime.Now;
            _enumerator.Current.Value.RecalcPoints();
            var end = DateTime.Now - time;
            var wasMinFound = WasMinFound(_enumerator.Current.Value);
            return new ExperimentResult(wasMinFound.Item2, wasMinFound.Item1, end.TotalSeconds);
        }

        public Tuple<bool, int> WasMinFound(ISearchAlg a)
        {
            List<double> optimalPoint = a.Problem.OptimalPoint;
            double sum = 0;
            foreach (var point in a.CalculatedPoints())
            {
                sum = 0;
                int i;
                for (i = 0; i < a.Problem.Dimension; i++)
                {
                    sum += ((point.x[i] - optimalPoint[i]) * (point.x[i] - optimalPoint[i]));
                }
                if (Math.Sqrt(sum) < epsilon)
                    return new Tuple<bool, int>(true, i);
            }
            return new Tuple<bool, int>(false, a.IterationsPerformed);
        }

        public SeriesExperimentResult GetSeriesExperimentResult()
        {
            SeriesExperimentResult res = new SeriesExperimentResult();
            int i = 0;
            List<Tuple<bool, int>> results = new List<Tuple<bool, int>>();
            foreach (var v in _algs.Values)
            {
                results.Add(WasMinFound(v));
            }
            res.OperationalChars.Add(new PointF(0f, 0f));
            foreach (var result in results.OrderBy(x => x.Item2))
            {
                if (result.Item1)
                {
                    i++;
                }
                var b = res.OperationalChars.FirstOrDefault(x => Math.Abs(x.X - result.Item2) < 0.00001);
                if (!b.IsEmpty)
                {
                    res.OperationalChars.Remove(b);
                }

                res.OperationalChars.Add(new PointF(result.Item2, (float) i / _expCount * 100.0f));
            }
            return res;
        }

        public ExperimentResult GetLastExperimentInfo()
        {
            throw new NotImplementedException();
        }

        public void Start()
        {
            _enumerator = _algs.GetEnumerator();
        }

        public ISearchAlg this[Guid id] => _algs[id];

        public Guid GetCurrentSubExperimentId()
        {
            return _enumerator.Current.Key;
        }

        public void SetProperty(string name, object value)
        {
            foreach (var alg in _algs)
            {
                alg.Value.SetProperty(name, value);
            }
        }

        public void SetProperties(Dictionary<string, object> propsValues)
        {
            foreach (var alg in _algs)
            {
                alg.Value.SetPropertiesWithoutRecalc(propsValues);
            }
        }

        public object GetProperty(string name)
        {
            return _algs.FirstOrDefault().Value.GetProperty(name);
        }

        public List<PropertyInfo> GetPropertiesInfo()
        {
            return _algs.FirstOrDefault().Value.GetPropertiesInfo();
        }

        public void RegisterProperty(PropertyInfo p)
        {
            throw new NotImplementedException();
        }
    }
}