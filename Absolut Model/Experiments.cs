using System;
using System.Collections.Generic;
using System.Linq;
using Algorithms;

namespace Absolut_Model
{
    public class Experiments
    {
        private readonly Dictionary<Guid, ISearchAlg> _listAlg;

        public Experiments()
        {
            _listAlg = new Dictionary<Guid, ISearchAlg>();
        }

        public void AddAlg(Guid id, ISearchAlg alg)
        {
            _listAlg.Add(id, alg);
        }

        public void AddAlgs(List<Guid> id, List<ISearchAlg> alg)
        {
            for (var i = 0; i < alg.Count; i++)
                _listAlg.Add(id[i], alg[i]);
        }

        public Dictionary<Guid, ISearchAlg> GetAlgs()
        {
            return _listAlg;
        }

        public ISearchAlg GetAlg(Guid id)
        {
            return _listAlg[id];
        }

        public void Start() //������ ����������
        {
            foreach (var e in _listAlg)
                _listAlg[e.Key].RecalcPoints();
        }

        public Dictionary<Guid, bool> GetExperimentStatus()
            //������ �������� � �� �������� �������������, ���� �������� �������
        {
            return _listAlg.ToDictionary(e => e.Key, e => _listAlg[e.Key].IsIterOverMin());
        }

        public Dictionary<Guid, int> GetExperimentIterToSolve() //����� ���������, ���� �������� �������
        {
            return _listAlg.ToDictionary(e => e.Key, e => _listAlg[e.Key].IterOverMin());
        }

        public int GetCountSolvedProblems() //���������� �������� �������������, ���� �������� �������
        {
            return _listAlg.Count(e => _listAlg[e.Key].IsIterOverMin());
        }
    }
}