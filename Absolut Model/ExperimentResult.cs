﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Absolut_Model
{
    public class ExperimentResult
    {
        public int Iterations;
        public bool WasProblemSolved;
        public double Time;
        public ExperimentResult(int valueIterationsPerformed, bool wasProblemSolved, double time = 0)
        {
            Iterations = valueIterationsPerformed;
            WasProblemSolved = wasProblemSolved;
            Time = time;
        }
    }
}
