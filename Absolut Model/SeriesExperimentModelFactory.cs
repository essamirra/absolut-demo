﻿namespace Absolut_Model
{
    public class SeriesExperimentModelFactory
    {
       private static ISeriesExperimentsModel _m = new SeriesExperimentModel();

        public static ISeriesExperimentsModel Build()
        {
            return _m;
        }
    }
}