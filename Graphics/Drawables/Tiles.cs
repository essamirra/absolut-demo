﻿using System.Collections.Generic;
using Properties;
using OpenTK.Graphics.OpenGL;

namespace Graphics.Drawables
{
    public class Tiles : IDrawable, IPropertyUser
    {
        private string _name = "Tiles";
        private PropertyProvider _propertyProvider = new PropertyProvider();
        private IPainter p = PainterFactory.BuildPainter();
        private IDrawable _main;
        private List<IDrawable> _children;

        public Tiles(IDrawable main)
        {
            _main = main;
        }

        public void Draw()
        {
            GL.PushMatrix();
            GL.Translate(-0.5, 0.5,  0);
            GL.Scale(0.5, 0.5, 0.5);
            _main.Draw();
            GL.PopMatrix();
        }

        public string Name
        {
            get { return _name; }
        }

        public void SetProperty(string name, object value)
        {
            _propertyProvider.SetProperty(name, value);
        }

        public void SetProperties(Dictionary<string, object> propsValues)
        {
            _propertyProvider.SetProperties(propsValues);
        }

        public object GetProperty(string name)
        {
            return _propertyProvider.GetProperty(name);
        }

        public List<PropertyInfo> GetPropertiesInfo()
        {
            return _propertyProvider.GetPropertiesInfo();
        }

        public void RegisterProperty(PropertyInfo p)
        {
            _propertyProvider.RegisterProperty(p);
        }
    }
}