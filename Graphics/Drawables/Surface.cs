﻿using System;
using System.Collections.Generic;
using System.Drawing;
using Functions;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using Properties;

namespace Graphics.Drawables
{

    public class Surface : IDrawable, IPropertyUser
    {
        private readonly IPainter _p;
        private readonly List<GridSquare> _adaptivePolygons = new List<GridSquare>();
        private double[] _center;
        private int _gridPoints;
        private int _initialGridCount = 12;
        private bool _isGridSet;
        private double[] _max;
        private double[] _mins;
        private readonly List<GridSquare> _polygons = new List<GridSquare>();
        private readonly PropertyProvider _propertyProvider = new PropertyProvider();
        private double[] _x;
        private double[,] _y;
        private double[] _z;
        private IFunction _f;

        public Surface(IPainter p)
        {
            _p = p;
            _propertyProvider.RegisterProperty(new BoolProperty("Adaptive grid", false));
            _propertyProvider.RegisterProperty(new BoolProperty("Mesh", true));
            _propertyProvider.RegisterProperty(new BoolProperty("Edge", true));
            _propertyProvider.RegisterProperty(new BoolProperty("Axis", true));
            _propertyProvider.RegisterProperty(new ColorProperty("Edge color", Color.Blue));
            _propertyProvider.RegisterProperty(new ColorProperty("Mesh color", Color.Black));
            _propertyProvider.RegisterProperty(new BoolProperty("Surface material", true));
            _propertyProvider.RegisterProperty(new IntProperty("Grid points", 8, 64, 8, 42));
            _propertyProvider.RegisterProperty(new IntProperty("Adaptive grid points", 4, 14, 2, 8));
        }

        protected Surface(int gridPointsCount, double[] x, double[] y, double[,] z)
        {
            _gridPoints = gridPointsCount;
            _x = x;
            _z = y;
            _y = z;
            _isGridSet = true;
        }

        protected Surface(int gridPointsCount, double[] x, double[] y, double[,] z, IPainter p)
        {
            _gridPoints = gridPointsCount;
            _x = x;
            _z = y;
            _y = z;
            _isGridSet = true;
            _p = p;
        }

        public void Draw()
        {
            if (_isGridSet && _p != null) DrawSurface();
        }

        public string Name => "Surface";


        public void SetProperty(string name, object value)
        {
            _propertyProvider.SetProperty(name, value);
        }


        public void SetProperties(Dictionary<string, object> propsValues)
        {
            _propertyProvider.SetProperties(propsValues);
            if (propsValues.ContainsKey("Grid points"))
                FillPolygons(_f);
            if (propsValues.ContainsKey("Adaptive grid points"))
                FillAdaptivePolygons();
        }

        public object GetProperty(string name) => _propertyProvider.GetProperty(name);
        public List<PropertyInfo> GetPropertiesInfo() => _propertyProvider.GetPropertiesInfo();

        public void RegisterProperty(PropertyInfo p) => _propertyProvider.RegisterProperty(p);

        public void SetSurface(IFunction func)
        {
            FillPolygons(func);
            FillAdaptivePolygons();

            _isGridSet = true;
        }

        private void FillPolygons(IFunction func)
        {
            _polygons.Clear();

            _f = func;
            int gridPoint = (int) GetProperty("Grid points");
            double[] x = Utils.FillCoordArrays(func, 0, gridPoint),
                y = Utils.FillCoordArrays(func, 1, gridPoint);

            int funcCount = func.GetNumberOfFunctions();
            double[][,] zz = new double[funcCount][,];

            for (int i = 0; i < funcCount; i++)
            {
                func.SetFunctionNumber(i);
                zz[i] = new double[gridPoint, gridPoint];
                Utils.FillZCMatrix(gridPoint, gridPoint, zz, x, y, func, i);
            }


            double[] min, max, center;
            Utils.GetInfo(x, y, zz[funcCount - 1], out min, out max, out center);
            Utils.AdjustTo01(x, y, zz[funcCount - 1], gridPoint);
            _gridPoints = gridPoint;
            _x = x;
            _z = y;
            _y = zz[funcCount - 1];

            for (var xCell = 0; xCell < _gridPoints - 1; xCell++)
            for (var yCell = 0; yCell < _gridPoints - 1; yCell++)
            {
                bool f = true;
                for (int i = 0; i < (funcCount - 1); i++)
                {
                    if (((float) zz[i][xCell, yCell] > 0) ||
                        ((float) zz[i][xCell + 1, yCell] > 0) ||
                        ((float) zz[i][xCell, yCell + 1] > 0) ||
                        ((float) zz[i][xCell + 1, yCell + 1] > 0))
                        f = false;
                }
                if (f)
                    _polygons.Add(new GridSquare(_x, _z, _y, xCell, yCell));
            }
        }

        private void FillAdaptivePolygons()
        {
            _adaptivePolygons.Clear();
            _initialGridCount = (int) GetProperty("Adaptive grid points");
            double[] x = Utils.FillCoordArrays(_f, 0, _initialGridCount),
                y = Utils.FillCoordArrays(_f, 1, _initialGridCount);
            var z = Utils.FillValues(_f, _initialGridCount);
            Utils.FillZMatrix(_initialGridCount, _initialGridCount, z, x, y, _f);
            Utils.GetInfo(x, y, z, out _mins, out _max, out _center);
            for (var xCell = 0; xCell < _initialGridCount - 1; xCell++)
            for (var yCell = 0; yCell < _initialGridCount - 1; yCell++)
                _adaptivePolygons.Add(new GridSquare(x, y, z, xCell, yCell));
            var level = Utils.GetAverageGrad(_adaptivePolygons, _f);
            var splitContainer = _adaptivePolygons;
            var tempContainer = new Dictionary<GridSquare, List<GridSquare>>();
            for (var i = 0; i < 2; i++)
            {
                foreach (var p in splitContainer)
                    if (Utils.NeedToSpleetPolygon(p, _f, level))
                        Spleet(p, tempContainer);

                foreach (var t in tempContainer)
                {
                    splitContainer.Remove(t.Key);
                    splitContainer.AddRange(t.Value);
                }
                tempContainer.Clear();
                level = Utils.GetAverageGrad(_adaptivePolygons, _f);
            }
            Utils.AdjustTo01(_adaptivePolygons, _mins, _max);
        }

        private void Spleet(GridSquare gridSquare, Dictionary<GridSquare, List<GridSquare>> tmp)
        {
            var lu = gridSquare.LeftUpper;
            var lb = gridSquare.LeftBottom;
            var ru = gridSquare.RightUpper;
            var rb = gridSquare.RightBottom;
            var x = gridSquare.X;
            var y = gridSquare.Y;

            var left = (gridSquare.LeftUpper + gridSquare.LeftBottom) / 2;
            var right = (gridSquare.RightUpper + gridSquare.RightBottom) / 2;
            var bottom = (gridSquare.LeftBottom + gridSquare.RightBottom) / 2;
            var upper = (gridSquare.LeftUpper + gridSquare.RightUpper) / 2;
            var center = (left + right) / 2;

            var leftPoint = new List<double> {left.X, left.Z};
            var rightPoint = new List<double> {right.X, right.Z};
            var bottomPoint = new List<double> {bottom.X, bottom.Z};
            var upperPoint = new List<double> {upper.X, upper.Z};
            var centerPoint = new List<double> {center.X, center.Z};
            float[] values =
            {
                (float) _f.Calc(leftPoint), (float) _f.Calc(rightPoint), (float) _f.Calc(bottomPoint),
                (float) _f.Calc(upperPoint), (float) _f.Calc(centerPoint)
            };
            foreach (var v in values)
            {
                if (v < _mins[2])
                    _mins[2] = v;
                if (v > _max[2])
                    _max[2] = v;
            }
            left = new Vector3(left.X, values[0], left.Z);
            right = new Vector3(right.X, values[1], right.Z);
            bottom = new Vector3(bottom.X, values[2], bottom.Z);
            upper = new Vector3(upper.X, values[3], upper.Z);
            center = new Vector3(center.X, values[4], center.Z);


            var val = new List<GridSquare>
            {
                new GridSquare(left, lb, center, bottom, x, y),
                new GridSquare(center, bottom, right, rb, x, y),
                new GridSquare(lu, left, upper, center, x, y),
                new GridSquare(upper, center, ru, right, x, y)
            };
            tmp.Add(gridSquare, val);
        }

        private void DrawSurface()
        {
            GL.PushMatrix();
            GL.Scale(1, 1, -1);
            var source = _polygons;
            if ((bool) GetProperty("Axis")) DrawAxis();
            if ((bool) GetProperty("Adaptive grid"))
                source = _adaptivePolygons;
            foreach (var polygon in source)
            {
                if ((bool) GetProperty("Surface material")) DrawSurfaceMaterial(polygon);
                if ((bool) GetProperty("Mesh")) DrawBorder(polygon);
            }
            GL.PopMatrix();
        }

        private void DrawBorder(GridSquare polygon)
        {
            var a = polygon.LeftBottom;
            var b = polygon.RightBottom;
            var c = polygon.LeftUpper;
            var d = polygon.RightUpper;

            var borderColor = (Color) GetProperty("Mesh color");
            var outsideBorderColor = (bool) GetProperty("Edge")
                ? (Color) GetProperty("Edge color")
                : (Color) GetProperty("Mesh color");
            var outsideBorderWidth = (bool) GetProperty("Edge") ? 3 : 1;
            const int borderWidth = 1;
            _p.DrawLine((Vector3d) a, (Vector3d) b, Math.Abs(a.Z + 1) < 0.001 ? outsideBorderColor : borderColor,
                Math.Abs(a.Z + 1) < 0.001 ? outsideBorderWidth : borderWidth);
            _p.DrawLine((Vector3d) a, (Vector3d) c, Math.Abs(a.X + 1) < 0.001 ? outsideBorderColor : borderColor,
                Math.Abs(a.X + 1) < 0.001 ? outsideBorderWidth : borderWidth);
            _p.DrawLine((Vector3d) d, (Vector3d) b, Math.Abs(d.X - 1) < 0.001 ? outsideBorderColor : borderColor,
                Math.Abs(d.X - 1) < 0.001 ? outsideBorderWidth : borderWidth);
            _p.DrawLine((Vector3d) d, (Vector3d) c, Math.Abs(d.Z - 1) < 0.001 ? outsideBorderColor : borderColor,
                Math.Abs(d.Z - 1) < 0.001 ? outsideBorderWidth : borderWidth);
        }

        private void DrawSurfaceMaterial(GridSquare polygon)
        {
            _p.DrawTriangle(polygon.LeftBottom, polygon.LeftBottomColor, polygon.RightUpper, polygon.RightUpperColor,
                polygon.RightBottom, polygon.RightBottomColor);
            _p.DrawTriangle(polygon.LeftBottom, polygon.LeftBottomColor, polygon.LeftUpper, polygon.LeftUpperColor,
                polygon.RightUpper, polygon.RightUpperColor);
        }

        private void DrawAxis()
        {
            var axisPos = new Vector3d(-1.3, -1, -1);
            const double axisLen = 2.3;
            GL.PushMatrix();
            GL.Translate(axisPos);
            _p.DrawCylinder(new Vector3d(0, 0, 0), new Vector3d(1, 0, 0), 0.01, axisLen, Color.Red);
            _p.DrawCylinder(new Vector3d(0, 0, 0), new Vector3d(0.001, 1, 0), 0.01, axisLen, Color.Blue);
            _p.DrawCylinder(new Vector3d(0, 0, 0), new Vector3d(0, 0, 1), 0.01, axisLen, Color.Green);
            GL.PopMatrix();
        }
    }
}