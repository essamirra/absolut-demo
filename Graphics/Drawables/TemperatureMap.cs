﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace Graphics
{
    public class TemperatureMap : IDrawable

    {
        private double[] _x;
        private double[] _z;
        private double[,] _y;
        private int _gridPoints;
        private bool _isGridSet;
        private readonly Color surfaceColor = Color.FromArgb(255, 0, 0);
        private Color gridColor = Color.Black;
        private Color edgeColor = Color.Blue;
        private double[] zExtrem;
        private bool surfaceEnabled = true;
        private bool edgeEnabled = true;
        private bool gridEnabled = true;
        private const int ColorLevelsCount = 5;
        private IPainter painter;


        public TemperatureMap(IPainter p)
        {
            painter = p;
        }

        public TemperatureMap(int gridPointsCount, double[] x, double[] y, double[,] z)
        {
            _gridPoints = gridPointsCount;
            _x = x;
            _z = y;
            _y = z;
            _isGridSet = true;

        }


        public TemperatureMap(int gridPointsCount, double[] x, double[] y, double[,] z, IPainter p)
        {
            _gridPoints = gridPointsCount;
            _x = x;
            _z = y;
            _y = z;
            _isGridSet = true;
            painter = p;

        }
        public void DrawAxis()
        {
            var axisPos = new Vector3d(-1.3, 0, -1.3);
            var axisLen = 2.3;
            GL.PushMatrix();
            GL.Translate(axisPos);
            painter.DrawCylinder(new Vector3d(0, 0, 0), new Vector3d(1, 0, 0), 0.01, axisLen, Color.Red);
            //p.DrawCylinder(new Vector3d(0, 0, 0), new Vector3d(0, 1, 0), 0.01, axisLen, Color.Blue);
            painter.DrawCylinder(new Vector3d(0, 0, 0), new Vector3d(0, 0, 1), 0.01, axisLen, Color.Green);
            GL.PopMatrix();
        }
        private void DrawMap()
        {
            GL.PushMatrix();
            GL.Scale(1,1,-1);
            DrawAxis();
            var levelsColors = Utils.FillColorsForLevels(surfaceColor, ColorLevelsCount);

            for (var xCell = 0; xCell < _gridPoints - 1; xCell++)
            {
                for (var yCell = 0; yCell < _gridPoints - 1; yCell++)
                {
                    var a = new Vector3d(_x[xCell], _y[xCell, yCell], _z[yCell]);
                    var b = new Vector3d(_x[xCell + 1], _y[xCell + 1, yCell], _z[yCell]);
                    var c = new Vector3d(_x[xCell], _y[xCell, yCell + 1], _z[yCell + 1]);
                    var d = new Vector3d(_x[xCell + 1], _y[xCell + 1, yCell + 1], _z[yCell + 1]);
                    if (surfaceEnabled) DrawSurfaceMaterial(levelsColors, ColorLevelsCount, a, d, b, c);
                   // if (gridEnabled) DrawBorder(yCell, a, b, xCell, c, d);
                }
            }
            GL.PopMatrix();
        }

        
        private void DrawSurfaceMaterial(Color[] levelsColors, int n, Vector3d a, Vector3d d,
            Vector3d b, Vector3d c)
        {
            var deltaColor = 2.0f / (n - 1);
            Vector3d a1 = a, b1 = b, d1 = d, c1 = c;
            a1.Y = 0;
            b1.Y = 0;
            d1.Y = 0;
            c1.Y = 0;
            painter.DrawTriangled(a1, Utils.GetColour(a.Y, -1, 1), d1, Utils.GetColour(d.Y, -1, 1),
                b1, Utils.GetColour(b.Y, -1, 1));
            painter.DrawTriangled(a1, Utils.GetColour(a.Y, -1, 1), c1, Utils.GetColour(c.Y, -1, 1),
                d1, Utils.GetColour(d.Y, -1, 1));
        }
        public void SetSurface(int gridPointsCount, double[] x, double[] y, double[,] z)
        {
            _gridPoints = gridPointsCount;
            _x = x;
            _z = y;
            _y = z;
            _isGridSet = true;
        }
        public void Draw()
        {
            if (_isGridSet && painter != null) DrawMap();
        }

        public string Name => "Temperature map";
       
    }
}
