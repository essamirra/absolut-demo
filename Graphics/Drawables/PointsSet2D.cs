﻿using System.Collections.Generic;
using System.Drawing;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace Graphics
{
    public class PointsSet2D : IDrawable
    {
        protected readonly Dictionary<int, List<Vector3d>> Bins;
        private readonly List<Color> _allColors = new List<Color>() { Color.Red, Color.Blue, Color.Indigo, Color.DeepPink, Color.DodgerBlue, Color.LightSeaGreen, Color.Cyan, Color.Magenta };

        public PointsSet2D()
        {
           Bins = new Dictionary<int, List<Vector3d>>();
        }

        public void Draw()
        {
            GL.PushMatrix();
            GL.Scale(1, 1, -1);
            GL.Begin(BeginMode.Points);
            for (int i = 0; i < Bins.Count; i++)
            {
                GL.Color3(_allColors[i]);
                foreach (var t in Bins[i])
                {
                   
                    GL.Vertex3(t);
                }
            }
            GL.End();
            GL.PopMatrix();
        }

        public string Name => "Points2d";
      
    }
}