﻿using System.Collections.Generic;
using Graphics.Oxyplot;
using OpenTK.Graphics.OpenGL;
using Properties;
using System.Drawing;
using OpenTK;
using OxyPlot;
using OxyPlot.Series;

namespace Graphics.Drawables
{
    public class Line2d : IDrawable, IOxyPlotDrawable, IPropertyUser
    {
        private PropertyProvider _propertyUserImplementation;
        private IPainter _painter = new SimplePainter();
        private List<Point> _points = new List<Point>();


        public string Name
        {
            get { return "Line2d"; }
        }

        public void SetProperty(string name, object value)
        {
            _propertyUserImplementation.SetProperty(name, value);
        }

        public void SetProperties(Dictionary<string, object> propsValues)
        {
            _propertyUserImplementation.SetProperties(propsValues);
        }

        public object GetProperty(string name)
        {
            return _propertyUserImplementation.GetProperty(name);
        }

        public List<PropertyInfo> GetPropertiesInfo()
        {
            return _propertyUserImplementation.GetPropertiesInfo();
        }

        public void RegisterProperty(PropertyInfo p)
        {
            _propertyUserImplementation.RegisterProperty(p);
        }

        public void SetPoints(List<Point> points)
        {
            _points = points;
        }

        public void Draw()
        {
            GL.PushMatrix();
            GL.Scale(1, 1, -1);
            GL.Begin(BeginMode.Lines);
            GL.Color3(Color.Black);
            foreach (var t in _points)
            {
                GL.Vertex2(new Vector2(t.X, t.Y));
            }

            GL.End();
            GL.PopMatrix();
        }

        public Series GetSeries()
        {
            LineSeries s = new LineSeries();
            foreach (var point in _points)
            {
                s.Points.Add(new DataPoint(point.X, point.Y));
            }

            return s;

        }
    }
}