using System.Collections.Generic;
using System.Drawing;
using Algorithms;
using Functions;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using Properties;

namespace Graphics
{
    public class ProblemaIsobars : IDrawable //IPopertyUser
    {
        PropertyProvider p = new PropertyProvider();
        SimplePainter painter = new SimplePainter();

        private int _gridPoints;
        private bool _isGridSet;

        private double[] _x;
        private double[,] _y;
        private double[] _z;
        protected IFunction _f;
        private IProblem _p;
        private float lineWidth = 1;

        public ProblemaIsobars()
        {
          
        }

        public string Name => "Isobars";
  
        public void SetProperty(string name, object value)
        {
            p.SetProperty(name, value);
        }

        public void SetProperties(Dictionary<string, object> propsValues)
        {
            p.SetProperties(propsValues);
        }

        public object GetProperty(string name)
        {
            return p.GetProperty(name);
        }

        public List<PropertyInfo> GetPropertiesInfo()
        {
            return p.GetPropertiesInfo();
        }

        public void RegisterProperty(PropertyInfo p)
        {
            this.p.RegisterProperty(p);
        }

        public void DrawAxis()
        {
            var axisPos = new Vector3d(-1.3, 0, -1.3);
            var axisLen = 2.3;
            GL.PushMatrix();
            GL.Translate(axisPos);
            painter.DrawCylinder(new Vector3d(0, 0, 0), new Vector3d(1, 0, 0), 0.01, axisLen, Color.Red);
            //p.DrawCylinder(new Vector3d(0, 0, 0), new Vector3d(0, 1, 0), 0.01, axisLen, Color.Blue);
            painter.DrawCylinder(new Vector3d(0, 0, 0), new Vector3d(0, 0, 1), 0.01, axisLen, Color.Green);
            GL.PopMatrix();
        }
        protected void DrawMap()
        {
            GL.PushMatrix();
            GL.Scale(1, 1, -1);
            DrawAxis();
            //  double level = -0.25;//(double)GetProperty("Level");
            double[] x = Utils.FillCoordArrays(_p.LowerBound, _p.UpperBound, 0, _gridPoints),
                y = Utils.FillCoordArrays(_p.LowerBound, _p.UpperBound, 1, _gridPoints);
            int funcCount = _p.NumberOfFunctions;
            double[][,] zz = new double[funcCount][,];

            for (int i = 0; i < funcCount; i++)
            {
                var f = _p.Functionals[i];
                zz[i] = new double[_gridPoints, _gridPoints];
                Utils.FillZCMatrix(_gridPoints, _gridPoints, zz, x, y, f, i);
            }

            for (double level = -1; level <= 1; level+=0.25)
            {
                Color lineColor = Utils.GetColour(level, -1, 1);
                for (var xCell = 0; xCell < _gridPoints - 1; xCell++)
                {
                    for (var yCell = 0; yCell < _gridPoints - 1; yCell++)
                    {
                        var a = new Vector3d(_x[xCell], _y[xCell, yCell], _z[yCell]);
                        var b = new Vector3d(_x[xCell + 1], _y[xCell + 1, yCell], _z[yCell]);
                        var c = new Vector3d(_x[xCell], _y[xCell, yCell + 1], _z[yCell + 1]);
                        var d = new Vector3d(_x[xCell + 1], _y[xCell + 1, yCell + 1], _z[yCell + 1]);
                        //c------d
                        //|      | 
                        //|      |
                        //a------b

                        bool f = true;
                        for (int i = 0; i < (funcCount - 1); i++)
                        {
                            if (((float)zz[i][xCell, yCell] > 0) ||
                                ((float)zz[i][xCell + 1, yCell] > 0) ||
                                ((float)zz[i][xCell, yCell + 1] > 0) ||
                                ((float)zz[i][xCell + 1, yCell + 1] > 0))
                                f = false;
                        }
                        if(!f) continue;

                        //������ 1 - ��� ������� � ����� ������
                        if (a.Y > level && b.Y > level && c.Y > level && d.Y > level) continue;
                        if (a.Y <= level && b.Y <= level && c.Y <= level && d.Y <= level) continue;
                        //C����� 2 - ���� ����, ��������� ����
                        //a-b+c+d+ level-a > 0, c-level > 0
                        if (a.Y <= level && b.Y > level && c.Y > level && d.Y > level) 
                        {
                            var point1 = new Vector3d(a.X, 0, a.Z + (level - a.Y)*(c.Z - a.Z));
                            var point2 = new Vector3d(a.X+(level-a.Y)*(b.X -a.X), 0, a.Z);
                            painter.DrawLine(point1,point2, lineColor, lineWidth);
                        }
                        //a+b-c-d- a-level>0 level-c > 0
                        if ((a.Y > level && b.Y <= level && c.Y <= level && d.Y <=level))
                        {
                            var point1 = new Vector3d(a.X, 0, a.Z + ( a.Y - level) * (c.Z - a.Z));
                            var point2 = new Vector3d(a.X + (a.Y - level) * (b.X - a.X), 0, a.Z);
                            painter.DrawLine(point1, point2, lineColor, lineWidth);
                        }

                        //a+b-c+d+ level - b > 0 d - level > 0 a-level > 0
                        if (a.Y > level && b.Y <= level && c.Y > level && d.Y > level)
                        {
                            var point1 = new Vector3d(b.X, 0, b.Z + (level - b.Y) * (d.Z - b.Z));
                            var point2 = new Vector3d(a.X + (a.Y - level) * (b.X - a.X), 0, a.Z);
                            painter.DrawLine(point1, point2, lineColor, lineWidth);
                        }

                        //a-b+c-d- b-level > 0 level-d > 0 level - a > 0
                        if (a.Y <= level && b.Y > level && c.Y <= level && d.Y <= level)
                        {
                            var point1 = new Vector3d(b.X, 0, b.Z + (b.Y - level) * (d.Z - b.Z));
                            var point2 = new Vector3d(a.X + (level - a.Y) * (b.X - a.X), 0, a.Z);
                            painter.DrawLine(point1, point2, lineColor, lineWidth);
                        }
                        //a+b+c-d+ level-c >0 d-level >0
                        if (a.Y > level && b.Y > level && c.Y < level && d.Y > level)
                        {
                            var point1  = new Vector3d(a.X, 0, a.Z + (a.Y - level)*(c.Z - a.Z));
                            var point2 = new Vector3d(c.X + (level-c.Y)*(d.X - c.X), 0, c.Z);
                            painter.DrawLine(point1, point2, lineColor, lineWidth);
                        }
                        //a-b-c+d- c - level >0 level - d >0
                        if (a.Y < level && b.Y < level && c.Y > level && d.Y < level)
                        {
                            var point1 = new Vector3d(a.X, 0, a.Z + (level - a.Y) * (c.Z - a.Z));
                            var point2 = new Vector3d(c.X + (c.Y - level) * (d.X - c.X), 0, c.Z);
                            painter.DrawLine(point1, point2, lineColor, lineWidth);
                        }
                        //a+b+c+d- level - d > 0
                        if (a.Y > level && b.Y > level && c.Y > level && d.Y < level)
                        {
                            var point1 = new Vector3d(c.X + (c.Y - level)*(d.X - c.X), 0, c.Z);
                            var point2 = new Vector3d(b.X, 0, b.Z + (level - b.Y)*(d.Y - b.Y));
                            painter.DrawLine(point1, point2, lineColor, lineWidth);
                        }
                        //a-b-c-d+ d-level > 0
                        if (a.Y < level && b.Y < level && c.Y < level && d.Y > level)
                        {
                            var point1 = new Vector3d(c.X + (level - c.Y) * (d.X - c.X), 0, c.Z);
                            var point2 = new Vector3d(b.X, 0, b.Z + (b.Y - level) * (d.Y - b.Y));
                            painter.DrawLine(point1, point2, lineColor, lineWidth);
                        }



                        //������ 3 - ��� �������, ������� ����, ��������� ����� ������
                        //a-b- level -a > 0 level - b > 0
                        if (a.Y < level && b.Y < level && c.Y > level && d.Y > level)
                        {
                            var point1 = new Vector3d(a.X, 0, a.Z + (level - a.Y)*(c.Y - a.Y) );
                            var point2 = new Vector3d(b.X, 0, b.Z + (level - b.Y) * (d.Y - b.Y));
                            painter.DrawLine(point1, point2, lineColor, lineWidth);
                        }
                        //a-c- level -a > 0 level - c >0
                        if (a.Y < level && b.Y > level && c.Y < level && d.Y > level)
                        {
                            var point1 = new Vector3d(a.X + (level-a.Y)*(b.X - a.X), 0, a.Z);
                            var point2 = new Vector3d(c.X + (level - c.Y) * (d.X - c.X), 0, c.Z);
                            painter.DrawLine(point1, point2, lineColor, lineWidth);
                        }
                        //c-d-
                        if (a.Y > level && b.Y > level && c.Y <= level && d.Y < level)
                        {
                            var point1 = new Vector3d(a.X, 0, a.Z + (a.Y - level) * (c.Y - a.Y));
                            var point2 = new Vector3d(b.X, 0, b.Z + (b.Y - level) * (d.Y - b.Y));
                            painter.DrawLine(point1, point2, lineColor, lineWidth);
                        }
                        //d-b-
                        if (a.Y > level && b.Y < level && c.Y > level && d.Y < level)
                        {
                            var point1 = new Vector3d(a.X + (a.Y - level) * (b.X - a.X), 0, a.Z);
                            var point2 = new Vector3d(c.X + (c.Y - level) * (d.X - c.X), 0, c.Z);
                            painter.DrawLine(point1, point2, lineColor, lineWidth);
                        }
                        //������ 4
                        //a-d-
                        if (a.Y < level && b.Y > level && c.Y > level && d.Y < level)
                        {
                            var center = new List<double>() {(a.X +b.X)/2, (a.Y + c.Y)/2};
                            var functionValue = _f.Calc(center);
                            var point1 = new Vector3d(a.X, 0, a.Z + (level - a.Y) * (c.Z - a.Z));
                            var point2 = new Vector3d(c.X + (c.Y - level) * (d.X - c.X), 0, c.Y);
                            var point3 = new Vector3d(a.X + (level - a.Y) * (b.X - a.X), 0, a.Z);
                            var point4 = new Vector3d(b.X, 0, b.Z + (b.Y - level)*(d.Z - b.Z));
                            if (functionValue > level)
                            {
                           
                                painter.DrawLine(point1, point2, lineColor, lineWidth);
                                painter.DrawLine(point3, point4, lineColor, lineWidth);

                            }
                            else if(functionValue <= level)
                            {
                           
                                painter.DrawLine(point1, point3, lineColor, lineWidth);
                                painter.DrawLine(point2, point4, lineColor, lineWidth);
                            }
                        }
                        //c-b-
                        if (a.Y > level && b.Y <= level && c.Y <= level && d.Y > level)
                        {
                            var center = new List<double> { (a.X + b.X) / 2, (a.Y + c.Y) / 2 };
                            var functionValue = _f.Calc(center);
                            var point1 = new Vector3d(a.X, 0, a.Z + (a.Y - level) * (c.Z - a.Z));
                            var point2 = new Vector3d(c.X + (level - c.Y) * (d.X - c.X), 0, c.Y);
                            var point3 = new Vector3d(a.X + (a.Y - level) * (b.X - a.X), 0, a.Z);
                            var point4 = new Vector3d(b.X, 0, b.Z + (level - b.Y) * (d.Z - b.Z));
                            if (functionValue > level)
                            {

                                painter.DrawLine(point1, point2, lineColor, lineWidth);
                                painter.DrawLine(point3, point4, lineColor, lineWidth);

                            }
                            else if (functionValue <= level)
                            {
                                painter.DrawLine(point1, point2, lineColor, lineWidth);
                                painter.DrawLine(point3, point4, lineColor, lineWidth);
                           
                            }
                        }

                    }
                }
            }
            GL.PopMatrix();
        }


        private void DrawSurfaceMaterial(Color[] levelsColors, int n, Vector3d a, Vector3d d,
            Vector3d b, Vector3d c)
        {
            var deltaColor = 2.0f / (n - 1);
            Vector3d a1 = a, b1 = b, d1 = d, c1 = c;
            a1.Y = 0;
            b1.Y = 0;
            d1.Y = 0;
            c1.Y = 0;
            painter.DrawTriangled(a1, Utils.GetColour(a.Y, -1, 1), d1, Utils.GetColour(d.Y, -1, 1),
                b1, Utils.GetColour(b.Y, -1, 1));
            painter.DrawTriangled(a1, Utils.GetColour(a.Y, -1, 1), c1, Utils.GetColour(c.Y, -1, 1),
                d1, Utils.GetColour(d.Y, -1, 1));
        }
        public void SetSurface(int gridPointsCount, double[] x, double[] y, double[,] z)
        {
            _gridPoints = gridPointsCount;
            _x = x;
            _z = y;
            _y = z;
            _isGridSet = true;
        }

        public void SetDrawedFunction(IFunction f)
        {
            _f = f;
        }
        public void Draw()
        {
            if (_isGridSet && painter != null) DrawMap();
        }

        public void UpdateProblem(IProblem problem)
        {
            _p = problem;
        }
    }
}