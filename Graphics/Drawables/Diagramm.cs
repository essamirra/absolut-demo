﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using Properties;

namespace Graphics
{
    public class Diagramm : IDrawable, IPropertyUser
    {
        protected PropertyProvider PropertyProvider = new PropertyProvider();
        protected double[] _x;
        protected double[] _y;
        public int _gridSize = 10;
        protected int[,] pointsCount;
        private IPainter p;
        protected bool isGridSet = false;

        public Diagramm(IPainter painter)
        {
            p = painter;
            RegisterProperty(new ColorProperty("Back color", Color.Bisque));
            RegisterProperty(new ColorProperty("Wire color", Color.DarkGreen));
            RegisterProperty(new ColorProperty("Columns color", Color.DarkSeaGreen));
        }

        public void SetGrid(double[] x, double[] y, int size)
        {
            _x = x;
            _y = y;
            pointsCount = new int[size - 1, size - 1];
            _gridSize = size;
            isGridSet = true;
        }

        private void DrawAxis()
        {
            var axisPos = new Vector3d(-1.3, 0, -1.3);
            var axisLen = 2.3;
            GL.PushMatrix();
            GL.Translate(axisPos);
            p.DrawCylinder(new Vector3d(0, 0, 0), new Vector3d(1, 0, 0), 0.01, axisLen, Color.Red);

            p.DrawCylinder(new Vector3d(0, 0, 0), new Vector3d(0, 0, 1), 0.01, axisLen, Color.Green);
            GL.PopMatrix();
        }

        public void Draw()
        {
            if (!isGridSet) return;
            GL.PushMatrix();
            GL.Scale(1, 1, -1);
            DrawAxis();
            double maxCount = pointsCount.Cast<int>().Max();
           
            for (int i = 0; i < _gridSize - 1; i++)
            {
                for (int j = 0; j < _gridSize - 1; j++)
                {
                    Vector3d a = new Vector3d(_x[i], 0, _y[j]);
                    Vector3d b = new Vector3d(_x[i], 0, _y[j + 1]);
                    Vector3d c = new Vector3d(_x[i + 1], 0, _y[j + 1]);
                    Vector3d d = new Vector3d(_x[i + 1], 0, _y[j]);
                    var aColor = (Color)GetProperty("Back color");
                    p.DrawTriangled(a, aColor, b, aColor, c, aColor);
                    p.DrawTriangled(a, aColor, c, aColor, d, aColor);
                    var wireColor = (Color)GetProperty("Wire color");
                    p.DrawLineLoop(a, b, c, d, wireColor);

                    if (pointsCount[i, j] > 0)
                    {
                        var f = 0.05;
                        Vector3d aUp = new Vector3d(_x[i] + f, pointsCount[i, j]/maxCount, _y[j] + f);
                        Vector3d bUp = new Vector3d(_x[i] + f, pointsCount[i, j]/maxCount, _y[j + 1] - f);
                        Vector3d dUp = new Vector3d(_x[i + 1] - f, pointsCount[i, j]/maxCount, _y[j] + f);
                        Vector3d cUp = new Vector3d(_x[i + 1] - f, pointsCount[i, j]/maxCount, _y[j + 1] - f);

                        a = new Vector3d(_x[i] + f, 0, _y[j] + f);
                        b = new Vector3d(_x[i] + f, 0, _y[j + 1] - f);
                        c = new Vector3d(_x[i + 1] - f, 0, _y[j + 1] - f);
                        d = new Vector3d(_x[i + 1] - f, 0, _y[j] + f);

                        p.DrawWiredParallepiped(a, b, c, d, aUp, bUp, cUp, dUp, wireColor);
                        var surfColor = (Color)GetProperty("Columns color");
                        p.DrawFilledParallepiped(a, b, c, d, aUp, bUp, cUp, dUp, surfColor);
                    }
                }
            }
            GL.PopMatrix();
        }

        public string Name => "Diagramm";
        public void SetProperty(string name, object value)
        {
            PropertyProvider.SetProperty(name, value);
        }

        public void SetProperties(Dictionary<string, object> propsValues)
        {
            PropertyProvider.SetProperties(propsValues);
        }

        public object GetProperty(string name)
        {
            return PropertyProvider.GetProperty(name);
        }

        public List<PropertyInfo> GetPropertiesInfo()
        {
            return PropertyProvider.GetPropertiesInfo();
        }

        public void RegisterProperty(PropertyInfo p)
        {
            PropertyProvider.RegisterProperty(p);
        }
    }
}