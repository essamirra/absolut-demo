﻿using System.Collections.Generic;
using System.Drawing;
using System.Reflection;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace Graphics
{
    public class PointsSet3D : IDrawable
    {
        private readonly IPainter painter;
        //protected readonly List<Vector3d> points;
        protected readonly Dictionary<int, List<Vector3d>> Bins;
        private List<Color> allColors = new List<Color>() {Color.Red, Color.Blue, Color.Indigo, Color.DeepPink, Color.DodgerBlue, Color.LightSeaGreen, Color.Cyan, Color.Magenta};

        public PointsSet3D()
        {
            painter = new SimplePainter();
            Bins = new Dictionary<int, List<Vector3d>>();
        }

        public void Draw()
        {
            GL.PushMatrix();
            GL.Scale(1, 1, -1);

            for (int i = 0; i < Bins.Count; i++)
            {
                foreach (var t in Bins[i])
                {
                    painter.DrawColorPoint(t, allColors[i]);
                }
            }

            GL.PopMatrix();
        }

        public string Name => "Points";

    }
}