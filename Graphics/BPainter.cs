﻿using System;
using System.Collections.Generic;

namespace Graphics
{
    public static class BPainter
    {
        public static List<string> Options = new List<string>()
        {
           "Simple painter"
        };
        public static IPainter Build(string option = "Simple painter")
        {
            if(option == "Simple painter")
                return new SimplePainter();
            else throw new ArgumentOutOfRangeException();
        }
    }
}