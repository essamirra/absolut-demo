﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Input;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using Properties;
using ViewDemo.Views;

namespace Graphics
{
    public class DrawablesControl : GLControl, IPropertyUser
    {
        private const double RotationSpeed = Math.PI / 2; // скорость поворота объектов

        private readonly Camera _camera; // управление поворотом

        private readonly Dictionary<PropertyInfo, object> _parameters // словарь свойств данного окна для рисования
            = new Dictionary<PropertyInfo, object>();

        private readonly PropertyProvider _property = new PropertyProvider(); // класс для работы со свойствами

        private readonly List<IPropertyUser> _propertyUsers // список объектов, имеющих собственые свойства
            = new List<IPropertyUser>();

        private readonly Timer _updateTimer = new Timer(); // таймер для отрисовки
        protected readonly List<IDrawable> Drawables = new List<IDrawable>(); // рисуемые объекты

        private bool _isLoaded; // флаг загрузки OpenGL контекста
        private double _lastUpdateTime; // время последней отрисовки 

        private Button _menuButton; // кнопка вызова меню

        private bool _needRedraw; // нужно ли перерисовывать объекты во время следующего сигнала таймера
        private bool _needToCreateChangeParamsDialog;
        private IContainer components; // компоненты данного элемента управления
        private ToolStripMenuItem screenshotToolStripMenuItem;
        protected ContextMenuStrip PropetiesMenuStrip; // контекстное меню

        protected DrawablesControl() : base(new GraphicsMode(32, 24, 8, 4), 3, 0, GraphicsContextFlags.Default)
        {
            InitializeComponent();
            Load += OnLoad;
            Paint += OnPaint;
            Resize += OnResize;
            _updateTimer.Interval = 32;
            _updateTimer.Tick += UpdateTimerOnTick;
            _updateTimer.Start();
            _camera = new Camera(new Vector3d(0, 3, 6));
        }

        public void SetProperty(string name, object value)
        {
            _property.SetProperty(name, value);
        }

        public void SetProperties(Dictionary<string, object> propsValues)
        {
            _property.SetProperties(propsValues);
        }

        public object GetProperty(string name)
        {
            return _property.GetProperty(name);
        }

        public List<PropertyInfo> GetPropertiesInfo()
        {
            return _property.GetPropertiesInfo();
        }

        public void RegisterProperty(PropertyInfo p)
        {
            _property.RegisterProperty(p);
        }


        private void OnResize(object sender, EventArgs eventArgs)
        {
            if (DesignMode) return;
            if (!_isLoaded) return;
            MakeCurrent();
            OpenGlHelper.ResizeGLControl(this);
        }

        private void UpdateTimerOnTick(object sender, EventArgs eventArgs)
        {
            var currentTime = Environment.TickCount / 1000.0;
            var delta = currentTime - _lastUpdateTime;
            _lastUpdateTime = currentTime;

            if (Keyboard.IsKeyDown(Key.W))
            {
                _camera.RotateAroundX(RotationSpeed * delta);
                _needRedraw = true;
            }
            if (Keyboard.IsKeyDown(Key.A))
            {
                _camera.RotateAroundY(-RotationSpeed * delta);
                _needRedraw = true;
            }
            if (Keyboard.IsKeyDown(Key.S))
            {
                _camera.RotateAroundX(-RotationSpeed * delta);
                _needRedraw = true;
            }
            if (Keyboard.IsKeyDown(Key.D))
            {
                _camera.RotateAroundY(RotationSpeed * delta);
                _needRedraw = true;
            }

            if (!_needRedraw) return;
            Invalidate();
            _needRedraw = false;
        }

        private void OnPaint(object sender, PaintEventArgs paintEventArgs)
        {
            if (DesignMode) return;
            if (!_isLoaded) return;
            MakeCurrent();
            _camera.Setup();
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            foreach (var d in Drawables)
                d.Draw();

            SwapBuffers();
        }

        private void OnLoad(object sender, EventArgs eventArgs)
        {
            if (DesignMode) return;
            _camera.Viewport = new Viewport(Width, Height);
            OpenGlHelper.Init3dWithSimpleLight();
            _isLoaded = true;
        }

        protected void AddDrawableObject(IDrawable drawableObject)
        {
            // добавление объекта в список отображаемых
            Drawables.Add(drawableObject);
            // проверка на использование объектом настраиваемых параметров
            var propsUser = drawableObject as IPropertyUser;
            if (propsUser == null) return;
            // добавление в список пользователей свойств
            _propertyUsers.Add(propsUser);
            // если существует элемент с таким же именем, заменить на новый
            if (_propertyUsers.Find(p => p.Name == propsUser.Name) != null) return;
            // создание пункта меню
            var newObjectMenuItem = new ToolStripMenuItem(drawableObject.Name);
            PropetiesMenuStrip.Items.Add(newObjectMenuItem);
            // генерация пунктов меню для свойств 
            foreach (var p in propsUser.GetPropertiesInfo())
            {
                var menuItemFromProperty = CreateMenuItemFromProperty(p, propsUser);
                if (menuItemFromProperty != null)
                    newObjectMenuItem.DropDownItems.Add(menuItemFromProperty);
            }
            // пункт меню для настройки числовых свойств, если такие имелись
            if (!_needToCreateChangeParamsDialog) return;
            var paramsItem = new ToolStripMenuItem("Other...");
            paramsItem.Click += OnClick;
            newObjectMenuItem.DropDownItems.Add(paramsItem);
            _needToCreateChangeParamsDialog = false;
        }

        private ToolStripMenuItem CreateMenuItemFromProperty(PropertyInfo p, IPropertyUser propsUser)
        {
            ToolStripMenuItem result = null;
            if (p is BoolProperty)
            {
                result = new ToolStripMenuItem(p.name)
                {
                    Checked = (bool) propsUser.GetProperty(p.name),
                    CheckOnClick = true
                };
                result.CheckedChanged += BoolPropertyOnCheckedChanged;
            }
            else if (p is ColorProperty)
            {
                result = new ToolStripMenuItem(p.name);
                result.Click += ColorPropertyOnClick;
            }
            else if (p is DoubleProperty)
            {
                _needToCreateChangeParamsDialog = true;
                _parameters.Add(p, (double) propsUser.GetProperty(p.name));
            }
            else if (p is IntProperty)
            {
                _needToCreateChangeParamsDialog = true;
                _parameters.Add(p, (int) propsUser.GetProperty(p.name));
            }
            return result;
        }

        private void OnClick(object sender, EventArgs eventArgs)
        {
            using (var k = new ParametersDialog(_parameters))
            {
                var dr = k.ShowDialog();
                if (dr != DialogResult.OK) return;
                var me = sender as ToolStripMenuItem;
                if (me == null) return;
                var nameOfDrawableToChange = me.OwnerItem.Text;
                var propsUser = _propertyUsers.FindAll(p => p.Name == nameOfDrawableToChange);
                foreach (var t in propsUser)
                {
                    t.SetProperties(k.changedValues);
                    _parameters.Clear();
                    foreach (var g in t.GetPropertiesInfo())
                        if (g is DoubleProperty)
                            _parameters.Add(g, (double) t.GetProperty(g.name));
                        else if (g is IntProperty)
                            _parameters.Add(g, (int) t.GetProperty(g.name));
                }
            }
        }

        private void ColorPropertyOnClick(object sender, EventArgs eventArgs)
        {
            using (var paramsChoose = new ColorDialog())
            {
                var dr = paramsChoose.ShowDialog();
                if (dr != DialogResult.OK) return;
                var me = sender as ToolStripMenuItem;
                if (me == null) return;
                var nameOfDrawableToChange = me.OwnerItem.Text;
                var propsUser = _propertyUsers.FindAll(p => p.Name == nameOfDrawableToChange);
                foreach (var t in propsUser)
                    t.SetProperty(me.Text, paramsChoose.Color);
            }
        }

        private void BoolPropertyOnCheckedChanged(object sender, EventArgs eventArgs)
        {
            var me = sender as ToolStripMenuItem;
            if (me == null) return;
            var nameOfDrawableToChange = me.OwnerItem.Text;
            var propsUser = _propertyUsers.FindAll(p => p.Name == nameOfDrawableToChange);
            foreach (var t in propsUser)
                t.SetProperty(me.Text, me.Checked);
        }

        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this._menuButton = new System.Windows.Forms.Button();
            this.PropetiesMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.screenshotToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.PropetiesMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // _menuButton
            // 
            this._menuButton.Location = new System.Drawing.Point(0, 0);
            this._menuButton.Name = "_menuButton";
            this._menuButton.Size = new System.Drawing.Size(23, 23);
            this._menuButton.TabIndex = 0;
            this._menuButton.Text = "+";
            this._menuButton.UseVisualStyleBackColor = true;
            // 
            // PropetiesMenuStrip
            // 
            this.PropetiesMenuStrip.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.PropetiesMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.screenshotToolStripMenuItem});
            this.PropetiesMenuStrip.Name = "PropetiesMenuStrip";
            this.PropetiesMenuStrip.Size = new System.Drawing.Size(181, 48);
            // 
            // screenshotToolStripMenuItem
            // 
            this.screenshotToolStripMenuItem.Name = "screenshotToolStripMenuItem";
            this.screenshotToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.screenshotToolStripMenuItem.Text = "Screenshot";
            this.screenshotToolStripMenuItem.Click += new System.EventHandler(this.screenshotToolStripMenuItem_Click);
            // 
            // DrawablesControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.Controls.Add(this._menuButton);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "DrawablesControl";
            this.PropetiesMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        private void MenuButtonClick(object sender, EventArgs e)
        {
            var btnSender = (Button) sender;
            var ptLowerLeft = new Point(0, btnSender.Height);
            ptLowerLeft = btnSender.PointToScreen(ptLowerLeft);
            PropetiesMenuStrip.Show(ptLowerLeft);
        }

        private void screenshotToolStripMenuItem_Click(object sender, EventArgs e)
        {
           Bitmap screenshot = this.GrabScreenshot();
            screenshot.Save("Screenshot" + System.DateTime.Now);
        }
    }
}