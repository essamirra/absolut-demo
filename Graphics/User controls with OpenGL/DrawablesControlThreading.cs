﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Input;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using Properties;
using ViewDemo.Views;
using System.Threading;
using Graphics;
using Timer = System.Threading.Timer;

namespace Graphics
{
    public class DrawablesStateChecker
    {
        private DrawablesControlThreading _control;
        private bool _needRedraw = false;

        public DrawablesStateChecker(DrawablesControlThreading c)
        {
            _control = c;
        }

        public void UpdateTimerOnTick(object state)
        {
            if (!_control.NeedRedraw) return;
            if(_control.IsDisposed) return;
            if (_control.InvokeRequired)
                _control.Invoke(new Action(_control.Invalidate));
            else
                _control.Invalidate();
            _control.NeedRedraw = false;
        }
    }

    public class DrawablesControlThreading : GLControl, IPropertyUser
    {
        private const double RotationSpeed = Math.PI/2;

        private  Camera _camera;

        public Camera Camera
        {
            get { return _camera; }
            set { _camera = value; }
        }

        private IContainer components;

        private bool _isLoaded;
        private readonly Dictionary<PropertyInfo, object> _parameters = new Dictionary<PropertyInfo, object>();

        private readonly Timer _updateTimer;
        private double _lastUpdateTime;
        private bool _needRedraw = false;

        public bool NeedRedraw
        {
            get { return _needRedraw; }
            set { _needRedraw = value; }
        }

        protected readonly PropertyProvider Property = new PropertyProvider();
        protected readonly List<IDrawable> Drawables = new List<IDrawable>();
        protected readonly List<IPropertyUser> PropertyUsers = new List<IPropertyUser>();

        protected Button button1;
        protected ContextMenuStrip contextMenuStrip1;
        private ToolStripMenuItem screenshotToolStripMenuItem;

        public DrawablesControlThreading() : base(new GraphicsMode(32, 24, 8, 4), 3, 0, GraphicsContextFlags.Default)
        {
            InitializeComponent();

            Load += OnLoad;
            Paint += OnPaint;
            Resize += OnResize;
            var checker = new DrawablesStateChecker(this);
            _updateTimer = new Timer(checker.UpdateTimerOnTick, null, 0, 32);
            //_updateTimer.Interval = 32;
            //_updateTimer.Tick += UpdateTimerOnTick;
            //_updateTimer.Start();

            _camera = new Camera(new Vector3d(0, 3, 6));
        }


        private void OnResize(object sender, EventArgs eventArgs)
        {
            if (DesignMode) return;
            if (!_isLoaded) return;
            MakeCurrent();
            var min = Math.Min(Height, Width);
            Camera.Viewport = new Viewport(min,min);
            
            OpenGlHelper.ResizeGLControl(this);
        }

        private void UpdateTimerOnTick()
        {
            double currentTime = Environment.TickCount/1000.0;
            double delta = currentTime - _lastUpdateTime;
            _lastUpdateTime = currentTime;

            if (Keyboard.IsKeyDown(Key.W))
            {
                _camera.RotateAroundX(RotationSpeed*delta);
                _needRedraw = true;
            }
            if (Keyboard.IsKeyDown(Key.A))
            {
                _camera.RotateAroundY(-RotationSpeed*delta);
                _needRedraw = true;
            }
            if (Keyboard.IsKeyDown(Key.S))
            {
                _camera.RotateAroundX(-RotationSpeed*delta);
                _needRedraw = true;
            }
            if (Keyboard.IsKeyDown(Key.D))
            {
                _camera.RotateAroundY(RotationSpeed*delta);
                _needRedraw = true;
            }

            if (_needRedraw)
            {
                Invalidate();
                _needRedraw = false;
            }
        }

        private void OnPaint(object sender, PaintEventArgs paintEventArgs)
        {
            if (DesignMode) return;
            if (!_isLoaded) return;
            MakeCurrent();
            _camera.Setup();
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            foreach (var d in Drawables)
            {
                d.Draw();
            }

            SwapBuffers();
        }

        private void OnLoad(object sender, EventArgs eventArgs)
        {
            if (DesignMode) return;
            _camera.Viewport = new Viewport(Width, Height);
            OpenGlHelper.Init3dWithSimpleLight();
            _isLoaded = true;
        }

        public void AddDrawableObject(IDrawable o)
        {
            Drawables.Add(o);
            var propsUser = o as IPropertyUser;
            if (propsUser == null) return;
            if (PropertyUsers.Find(p => p.Name == propsUser.Name) != null)
            {
                PropertyUsers.Add(propsUser);
                return;
            }
            PropertyUsers.Add(propsUser);
            var added = new ToolStripMenuItem(o.Name);
            contextMenuStrip1.Items.Add(added);
            bool f = false;
            foreach (var p in propsUser.GetPropertiesInfo())
            {
                if ((p is BoolProperty))
                {
                    var boolProperty = new ToolStripMenuItem(p.name)
                    {
                        Checked = (bool) propsUser.GetProperty(p.name),
                        CheckOnClick = true
                    };
                    boolProperty.CheckedChanged += BoolPropertyOnCheckedChanged;

                    added.DropDownItems.Add(boolProperty);
                }
                else if (p is ColorProperty)
                {
                    var colorProperty = new ToolStripMenuItem(p.name);

                    colorProperty.Click += ColorPropertyOnClick;

                    added.DropDownItems.Add(colorProperty);
                }
                else if (p is DoubleProperty)
                {
                    f = true;
                    _parameters.Add(p, (double) propsUser.GetProperty(p.name));
                }
                else if (p is IntProperty)
                {
                    f = true;
                    _parameters.Add(p, (int) propsUser.GetProperty(p.name));
                }
            }
            if (f)
            {
                var paramsItem = new ToolStripMenuItem("Other...");

                paramsItem.Click += OnClick;

                added.DropDownItems.Add(paramsItem);
            }
        }

        private void OnClick(object sender, EventArgs eventArgs)
        {
            using (var k = new ParametersDialog(_parameters))
            {
                DialogResult dr = k.ShowDialog();
                if (dr == DialogResult.OK)
                {
                    var me = sender as ToolStripMenuItem;
                    if (me == null) return;
                    var nameOfDrawableToChange = me.OwnerItem.Text;
                    var propsUser = PropertyUsers.FindAll(p => p.Name == nameOfDrawableToChange);
                    foreach (var t in propsUser)
                    {
                        t.SetProperties(k.changedValues);
                        _parameters.Clear();
                        foreach (var g in t.GetPropertiesInfo())
                        {
                            if (g is DoubleProperty)
                            {
                                _parameters.Add(g, (double) t.GetProperty(g.name));
                            }
                            else if (g is IntProperty)
                            {
                                _parameters.Add(g, (int) t.GetProperty(g.name));
                            }
                        }
                    }
                }
            }
        }

        private void ColorPropertyOnClick(object sender, EventArgs eventArgs)
        {
            using (var paramsChoose = new ColorDialog())
            {
                DialogResult dr = paramsChoose.ShowDialog();
                if (dr == DialogResult.OK)
                {
                    var me = sender as ToolStripMenuItem;
                    if (me == null) return;
                    var nameOfDrawableToChange = me.OwnerItem.Text;
                    var propsUser = PropertyUsers.FindAll(p => p.Name == nameOfDrawableToChange);
                    foreach (var t in propsUser)
                    {
                        t.SetProperty(me.Text, paramsChoose.Color);
                    }
                }
            }
        }

        private void BoolPropertyOnCheckedChanged(object sender, EventArgs eventArgs)
        {
            var me = sender as ToolStripMenuItem;
            if (me == null) return;
            var nameOfDrawableToChange = me.OwnerItem.Text;
            var propsUser = PropertyUsers.FindAll(p => p.Name == nameOfDrawableToChange);
            foreach (var t in propsUser)
            {
                t.SetProperty(me.Text, me.Checked);
            }
        }

        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.button1 = new System.Windows.Forms.Button();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.screenshotToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(0, 0);
            this.button1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(31, 28);
            this.button1.TabIndex = 0;
            this.button1.Text = "+";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // _propetiesMenuStrip
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
                this.screenshotToolStripMenuItem});

            // 
            // screenshotToolStripMenuItem
            // 
            this.screenshotToolStripMenuItem.Name = "screenshotToolStripMenuItem";
            this.screenshotToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.screenshotToolStripMenuItem.Text = "Screenshot";
            this.screenshotToolStripMenuItem.Click += new System.EventHandler(this.screenshotToolStripMenuItem_Click);
            // 
            // DrawablesControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.Controls.Add(this.button1);
            this.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.Name = "DrawablesControl";
            this.ResumeLayout(false);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Button btnSender = (Button) sender;
            Point ptLowerLeft = new Point(0, btnSender.Height);
            ptLowerLeft = btnSender.PointToScreen(ptLowerLeft);
            contextMenuStrip1.Show(ptLowerLeft);
        }

        public void SetProperty(string name, object value)
        {
            Property.SetProperty(name, value);
        }

        public void SetProperties(Dictionary<string, object> propsValues)
        {
            Property.SetProperties(propsValues);
        }

        public object GetProperty(string name)
        {
            return Property.GetProperty(name);
        }

        public List<PropertyInfo> GetPropertiesInfo()
        {
            return Property.GetPropertiesInfo();
        }

        public void RegisterProperty(PropertyInfo p)
        {
            Property.RegisterProperty(p);
        }
        private void screenshotToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Bitmap screenshot = this.GrabScreenshot();
            var filename = "Screenshot" + System.DateTime.Now.ToString("MM/dd/yyyy HH-mm-ss")+ ".jpg";
            screenshot.Save(filename);
        }
    }
}