﻿using OxyPlot;
using OxyPlot.Series;

namespace Graphics.Oxyplot
{
    public interface IOxyPlotDrawable
    {
        Series GetSeries();
    }
}