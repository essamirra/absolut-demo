﻿using System;
using System.Collections.Generic;
using System.Drawing;
using OpenTK;
using OpenTK.Graphics.OpenGL;
namespace Graphics
{
    public interface IPainter
    {
        void DrawPoint(Vector3d a);
        void DrawColorPoint(Vector3d a, Color c);
        void DrawLine(Vector3d a, Vector3d b, Color color, float width=1);
        void DrawTriangled(Vector3d a, Color aColor, Vector3d b, Color bColor, Vector3d c, Color cColor, bool normals = false);
        void DrawLineLoop(Vector3d a, Vector3d b, Vector3d c, Vector3d d, Color color, float lineWidth = 1);
        void DrawRectangle(Vector3d a, Vector3d b, Vector3d c, Vector3d d);

         void DrawTriangle(Vector3 a, Color aColor, Vector3 b, Color bColor, Vector3 c, Color cColor,
            bool normals = false);
        void DrawMaterialTriangle(Vector3d a, Color aColor, Vector3d b, Color bColor, Vector3d c, Color cColor,
            bool normals = false);
        void InvertZAxis();
        void DrawCylinder(Vector3d start, Vector3d end, double radius, double height, Color color);
    }


    public static class IPainterExt
    {
        public static void DrawFilledParallepiped(this IPainter p, Vector3d a1, Vector3d b1, Vector3d c1, Vector3d d1, Vector3d a2, Vector3d b2,
            Vector3d c2, Vector3d d2, Color surfColor)
        {
            //Верхняя грань
         
            p.DrawMaterialTriangle(a1,surfColor,b1, surfColor, c1, surfColor, true);
            p.DrawMaterialTriangle(a1,surfColor,c1,surfColor,d1, surfColor, true);
            //Нижняя грань
            p.DrawMaterialTriangle(a2, surfColor, b2, surfColor, c2, surfColor, true);
            p.DrawMaterialTriangle(a2, surfColor, c2, surfColor, d2, surfColor, true);
            //a2a1d1d2
            p.DrawMaterialTriangle(a2, surfColor, a1, surfColor, d1, surfColor, true);
            p.DrawMaterialTriangle(a2, surfColor, d1, surfColor, d2, surfColor, true);
            //b2b1a1a2
            p.DrawMaterialTriangle(b2, surfColor, b1, surfColor, a1, surfColor, true);
            p.DrawMaterialTriangle(b2, surfColor, a1, surfColor, a2, surfColor, true);
            //b2b1c1c2
            p.DrawMaterialTriangle(b2, surfColor, b1, surfColor, c1, surfColor, true);
            p.DrawMaterialTriangle(b2, surfColor, c1, surfColor, c2, surfColor, true);
            //d2d1c1c2
            p.DrawMaterialTriangle(d2, surfColor, d1, surfColor, c1, surfColor, true);
            p.DrawMaterialTriangle(d2, surfColor, c1, surfColor, c2, surfColor, true);
        }

        public static void DrawWiredParallepiped(this IPainter p, Vector3d a1, Vector3d b1, Vector3d c1, Vector3d d1,
            Vector3d a2, Vector3d b2,
            Vector3d c2, Vector3d d2, Color wireColor)
        {
            p.DrawLineLoop(a1,b1,c1,d1,wireColor);
            p.DrawLineLoop(a2,b2,c2,d2,wireColor);
            p.DrawLine(a1,a2,wireColor);
            p.DrawLine(b1,b2,wireColor);
            p.DrawLine(c1,c2,wireColor);
            p.DrawLine(d1,d2,wireColor);
        }

        

        public static void DrawArrow(this IPainter p, Vector3d start, Vector3d end, Color c)
        {
            p.DrawLine(start,end,c);
            p.DrawLine(end, new Vector3d(end.X - 0.250, end.Y - 0.25, end.Z - 0.50), c );
            p.DrawLine(end, new Vector3d(end.X + 0.25, end.Y + 0.25, end.Z + 0.550), c);


        }

       


    }
}
