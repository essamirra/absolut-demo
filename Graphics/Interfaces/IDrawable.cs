﻿using System.Collections.Generic;

namespace Graphics
{
    public interface IDrawable
    {
        void Draw();
        string Name { get; }
    }
}