﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using Functions;
using OpenTK;

namespace Graphics
{
    public static class Utils
    {
        public static float Epsilon = 0.01f;

        public static double[] FillCoordArrays(IFunction f, int axeIndex, int gridSize)
        {
            var xray = new double[gridSize];
            var step = (f.Right[axeIndex] - f.Left[axeIndex]) / (gridSize - 1);
            for (var i = 0; i < gridSize; i++)
                xray[i] = f.Left[axeIndex] + step * i;
            return xray;
        }
        public static double[] FillCoordArrays(List<double> lower, List<double> upper , int axeIndex, int gridSize)
        {
            var xray = new double[gridSize];
            var step = (upper[axeIndex] - lower[axeIndex]) / (gridSize - 1);
            for (var i = 0; i < gridSize; i++)
                xray[i] = lower[axeIndex] + step * i;
            return xray;
        }
        public static double[,] FillValues(IFunction f, int gridSize)
        {
            var z = new double[gridSize, gridSize];
            FillZMatrix(gridSize, gridSize, z, FillCoordArrays(f, 0, gridSize), FillCoordArrays(f, 1, gridSize), f);
            return z;
        }

        public static void FillCoordArrays(double endX, double startX, int n, double[] xray)
        {
            var stepx = (endX - startX) / (n - 1);
            for (var i = 0; i < n; i++)
                xray[i] = startX + stepx * i;
        }

        public static void GetInfo(double[] x, double[] y, double[,] z, out double[] min, out double[] max,
            out double[] center)
        {
            min = new[] {x.Min(), y.Min(), z.Cast<double>().Min()};
            max = new[] {x.Max(), y.Max(), z.Cast<double>().Max()};
            center = new double[3];
            for (var i = 0; i < 3; i++)
                center[i] = (min[i] + max[i]) / 2;
        }

        public static Vector3d AdjustVectorTo01(Vector3d v, double[] min, double[] max)
        {
            var v1 = new Vector3d();
            double[] scale = {1, 1, 1}; //GetScaleCoef(min, max, 3);
            var center = new double[3];
            for (var i = 0; i < 3; i++)
                center[i] = (min[i] + max[i]) / 2;

            v1.X = v.X - center[0];
            v1.X = v.X / (max[0] - min[0]);
            v1.X = v.X * 2 * scale[0];
            v1.Y = v.Y - center[1];
            v1.Y = v.Y / (max[1] - min[1]);
            v1.Y = v.Y * 2 * scale[1];
            v1.Z = v.Z - center[2];
            v1.Z = v.Z / (max[2] - min[2]);
            v1.Z = v.Z * 2 * scale[2];
            return v1;
        }

        public static void AdjustTo01(double[] x, double[] y, double[,] z, int gridPoints)
        {
            double[] min = {x.Min(), y.Min(), z.Cast<double>().Min()};
            double[] max = {x.Max(), y.Max(), z.Cast<double>().Max()};
            double[] scale = {1, 1, 1}; //GetScaleCoef(min, max, 3);
            var center = new double[3];
            for (var i = 0; i < 3; i++)
                center[i] = (min[i] + max[i]) / 2;
            for (var i = 0; i < gridPoints; i++)
            {
                x[i] -= center[0];
                x[i] /= max[0] - min[0];
                x[i] *= 2 * scale[0];
                y[i] -= center[1];
                y[i] /= max[1] - min[1];
                y[i] *= 2 * scale[1];
                for (var j = 0; j < gridPoints; j++)
                {
                    z[i, j] -= center[2];
                    z[i, j] /= max[2] - min[2];
                    z[i, j] *= 2 * scale[2];
                }
            }
        }

        public static void AdjustTo01(double[] x, double[] y, int gridPoints)
        {
            double[] min = {x.Min(), y.Min()};
            double[] max = {x.Max(), y.Max()};
            double[] scale = {1, 1}; //GetScaleCoef(min, max, 3);
            var center = new double[2];
            for (var i = 0; i < 2; i++)
                center[i] = (min[i] + max[i]) / 2;
            for (var i = 0; i < gridPoints; i++)
            {
                x[i] -= center[0];
                x[i] /= max[0] - min[0];
                x[i] *= 2 * scale[0];
                y[i] -= center[1];
                y[i] /= max[1] - min[1];
                y[i] *= 2 * scale[1];
            }
        }


        public static Color[] FillColorsForLevels(Color mainColor, int levelsCount)
        {
            var levelsColors = new Color[levelsCount];
            for (var i = 0; i < levelsCount; i++)
                levelsColors[i] = Color.FromArgb(i * (mainColor.R / levelsCount),
                    i * (mainColor.G / levelsCount), i * (mainColor.B / levelsCount));
            return levelsColors;
        }

        public static double[] FillZMatrix(int n, int m, double[,] zmat, double[] xray, double[] yray, IFunction f)

        {
            var extremums = new double[2];
            extremums[0] = double.MaxValue;
            extremums[1] = double.MinValue;
            var point = new List<double> {0, 0};
            for (var i = 0; i < n; i++)
            {
                point[0] = xray[i];

                for (var j = 0; j < m; j++)
                {
                    point[1] = yray[j];
                    zmat[i, j] = f.Calc(point);
                    if (zmat[i, j] > extremums[1])
                        extremums[1] = zmat[i, j];
                    if (zmat[i, j] < extremums[0])
                        extremums[0] = zmat[i, j];
                }
            }
            return extremums;
            //AdjustMatrixArray(n,m, zmat, startZ, endZ);
        }

        public static double[] FillZCMatrix(int n, int m, double[][,] zmat, double[] xray, double[] yray, IFunction f, int t)

        {
            var extremums = new double[2];
            extremums[0] = double.MaxValue;
            extremums[1] = double.MinValue;
            var point = new List<double> { 0, 0 };
            for (var i = 0; i < n; i++)
            {
                point[0] = xray[i];

                for (var j = 0; j < m; j++)
                {
                    point[1] = yray[j];
                    if (t > 0)
                    {
                        if (zmat[t - 1][i, j] <= 0)
                        {
                            zmat[t][i, j] = f.Calc(point);
                            if (zmat[t][i, j] > extremums[1])
                                extremums[1] = zmat[t][i, j];
                            if (zmat[t][i, j] < extremums[0])
                                extremums[0] = zmat[t][i, j];
                        }
                        else
                            zmat[t - 1][i, j] = 1;
                    }
                    else
                    {
                        zmat[t][i, j] = f.Calc(point);
                        if (zmat[t][i, j] > extremums[1])
                            extremums[1] = zmat[t][i, j];
                        if (zmat[t][i, j] < extremums[0])
                            extremums[0] = zmat[t][i, j];
                    }
                }
            }
            return extremums;
            //AdjustMatrixArray(n,m, zmat, startZ, endZ);
        }



        public static Color GetColour(double v, double vmin, double vmax)
        {
            double[] c = new double[3];
            c[0] = 1.0;
            c[1] = 1.0;
            c[2] = 1.0;// white
            double dv;

            if (v < vmin)
                v = vmin;
            if (v > vmax)
                v = vmax;
            dv = vmax - vmin;

            if (v < vmin + 0.25 * dv)
            {
                c[0] = 0;
                c[1] = 4 * (v - vmin) / dv;
            }
            else if (v < vmin + 0.5 * dv)
            {
                c[0] = 0;
                c[2] = 1 + 4 * (vmin + 0.25 * dv - v) / dv;
            }
            else if (v < vmin + 0.75 * dv)
            {
                c[0] = 4 * (v - vmin - 0.5 * dv) / dv;
                c[2] = 0;
            }
            else
            {
                c[1] = 1 + 4 * (vmin + 0.75 * dv - v) / dv;
                c[2] = 0;
            }
            Color returnColor;
            try
            {
                returnColor = Color.FromArgb((int) (255 * c[0]), (int) (255 * c[1]), (int) (255 * c[2]));
            }
            catch (Exception e)
            {
                returnColor = Color.Black;
            }


            return returnColor;
        }

        public static void AdjustTo01(List<GridSquare> polygons, double[] min, double[] max)
        {
            float[] scale = {1, 1, 1}; //GetScaleCoef(min, max, 3);
            var center = new float[3];
            for (var i = 0; i < 3; i++)
                center[i] = (float) ((min[i] + max[i]) / 2);

            foreach (var p in polygons)
            {
                var x = new[] {p.LeftBottom.X, p.LeftUpper.X, p.RightUpper.X, p.RightBottom.X};
                var z = new[] {p.LeftBottom.Y, p.LeftUpper.Y, p.RightUpper.Y, p.RightBottom.Y};
                var y = new[] {p.LeftBottom.Z, p.LeftUpper.Z, p.RightUpper.Z, p.RightBottom.Z};
                for (var i = 0; i < 4; i++)
                {
                    x[i] -= center[0];
                    x[i] /=  (float)(max[0] - min[0]);
                    x[i] *= 2 * scale[0];
                    y[i] -= center[1];
                    y[i] /= (float)(max[1] - min[1]);
                    y[i] *= 2 * scale[1];
                    z[i] -= center[2];
                    z[i] /= (float)(max[2] - min[2]);
                    z[i] *= 2 * scale[2];
                }
                p.LeftBottom = new Vector3(x[0], z[0], y[0]);
                p.LeftUpper = new Vector3(x[1], z[1], y[1]);
                p.RightUpper = new Vector3(x[2], z[2], y[2]);
                p.RightBottom = new Vector3(x[3], z[3], y[3]);
            }
        }

        public static bool NeedToSpleetPolygon(GridSquare gridSquare, IFunction function, float level = 1)
        {
            var left = (gridSquare.LeftUpper + gridSquare.LeftBottom) / 2;
            var right = (gridSquare.RightUpper + gridSquare.RightBottom) / 2;
            var bottom = (gridSquare.LeftBottom + gridSquare.RightBottom) / 2;
            var upper = (gridSquare.LeftUpper + gridSquare.RightUpper) / 2;
            var center = (left + right) / 2;
            var i = 1;
            return CalculateGradient(function, left).Length > level ||
                   CalculateGradient(function, right).Length > level ||
                   CalculateGradient(function, bottom).Length > level ||
                   CalculateGradient(function, upper).Length > level ||
                   CalculateGradient(function, center).Length > level;
        }

        public static float GetAverageGrad(List<GridSquare> polygons, IFunction f)
        {
            float sum = 0;
            foreach (var p in polygons)
            {
                sum += CalculateGradient(f, p.LeftBottom).Length;
                sum += CalculateGradient(f, p.LeftUpper).Length;
                sum += CalculateGradient(f, p.RightUpper).Length;
                sum += CalculateGradient(f, p.RightBottom).Length;
            }

            return sum / (polygons.Count * 4);
        }

        public static float CalculateLipshitsConst(Vector3 a, Vector3 b)
        {
            Vector2 x = new Vector2(a.X, a.Z);
            Vector2 y = new Vector2(b.X, b.Z);
            return Math.Abs(a.Y - b.Y) / (x - y).Length;
        }

        public static Vector2 CalculateGradient(IFunction function, Vector3 point)
        {
            var plusStep = new List<double> {point.X + Epsilon, point.Z};
            var minusStep = new List<double> {point.X - Epsilon, point.Z};
            var dx = (float) (function.Calc(plusStep) - function.Calc(minusStep));
            plusStep[0] -= Epsilon;
            plusStep[1] += Epsilon;
            minusStep[0] += Epsilon;
            minusStep[1] -= Epsilon;
            var dy = (float) (function.Calc(plusStep) - function.Calc(minusStep));

            return new Vector2(dx, dy) / (2.0f * Epsilon);
        }
    }
}