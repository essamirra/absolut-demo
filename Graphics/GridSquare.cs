﻿using System.Drawing;
using OpenTK;

namespace Graphics
{
    public class GridSquare
    {
        public  Vector3 LeftBottom;
        public  Vector3 LeftUpper;
        public  Vector3 RightBottom;
        public  Vector3 RightUpper;
        public readonly int X;
        public readonly int Y;

        public GridSquare(double[] x, double[] y, double[,] z, int xIndex, int yIndex)
        {
            X = xIndex;
            Y = yIndex;
            LeftBottom = new Vector3((float) x[xIndex], (float) z[xIndex, yIndex], (float) y[yIndex]);
            RightBottom = new Vector3((float) x[xIndex + 1], (float) z[xIndex + 1, yIndex], (float) y[yIndex]);
            LeftUpper = new Vector3((float) x[xIndex], (float) z[xIndex, yIndex + 1], (float) y[yIndex + 1]);
            RightUpper = new Vector3((float) x[xIndex + 1], (float) z[xIndex + 1, yIndex + 1], (float) y[yIndex + 1]);
        }

        public GridSquare(Vector3 lu, Vector3 lb, Vector3 ru, Vector3 rb, int xIndex, int yIndex)
        {
            X = xIndex;
            Y = yIndex;
            LeftBottom = lb;
            RightBottom = rb;
            LeftUpper = lu;
            RightUpper = ru;
        }
        public Color LeftUpperColor => Utils.GetColour(LeftUpper.Y, -1, 1);
        public Color LeftBottomColor => Utils.GetColour(LeftBottom.Y, -1, 1);
        public Color RightUpperColor => Utils.GetColour(RightUpper.Y, -1, 1);
        public Color RightBottomColor => Utils.GetColour(RightBottom.Y, -1, 1);
    }
}