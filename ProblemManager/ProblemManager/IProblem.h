#pragma once
using namespace System;
public  interface class ProblemInterface
{
    static const int OK = 0;
    static const int UNDEFINED = -1;
    static const int problemERROR = -2;

    int SetConfigPath(String ^ configPath);
    int SetDimension(int dimension);
    int GetDimension();
    int Initialize();
    void GetBounds(array<double> ^ lower, array<double> ^ upper);
    int GetOptimalValue(double& value);
    int GetOptimalValue(double & value, int index);
    int GetOptimumPoint(array<double>^ y);
    int GetNumberOfFunctions();
    int GetNumberOfConstraints();
    int GetNumberOfCriterions();
    double CalculateFunctionals(array<double> ^ y, int fNumber);

};
