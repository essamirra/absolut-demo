#include "stdafx.h"
#include <msclr\marshal_cppstd.h>
#include "ExaminProblem.h"




ExaminProblem::ExaminProblem(IProblem * _p)
{
    p = _p;
}

int ExaminProblem::SetConfigPath(String ^ configPath)
{
  return  p->SetConfigPath(msclr::interop::marshal_as<std::string>(configPath));
}

int ExaminProblem::SetDimension(int dimension)
{
   return p->SetDimension(dimension);
}
int ExaminProblem::GetDimension()
{
   return p->GetDimension();
}

int ExaminProblem::Initialize()
{
   return  p->Initialize();
}

void ExaminProblem::GetBounds(array<double>^ lower, array<double>^ upper)
{
    pin_ptr<double> l = &lower[0];
    pin_ptr<double> u = &upper[0];
    p->GetBounds(l, u);
}

int ExaminProblem::GetOptimalValue(double & value)
{
    return p->GetOptimumValue(value);
}

int ExaminProblem::GetOptimalValue(double & value, int index)
{
    return p->GetOptimumValue(value, index);
}

int ExaminProblem::GetOptimumPoint(array<double>^ y)
{
    pin_ptr<double> l = &y[0];
    return p->GetOptimumPoint(l);
}

int ExaminProblem::GetNumberOfFunctions()
{
    return p->GetNumberOfFunctions();
}

int ExaminProblem::GetNumberOfConstraints()
{
    return p->GetNumberOfConstraints();
}

int ExaminProblem::GetNumberOfCriterions()
{
    return p->GetNumberOfCriterions();
}

double ExaminProblem::CalculateFunctionals(array<double>^ y, int fNumber)
{
    pin_ptr<double> l = &y[0];
    return p->CalculateFunctionals(l, fNumber);
}
