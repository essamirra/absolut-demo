﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Functions;

namespace Properties
{
   

    

   

    public class FuncProperty : PropertyInfo
    {
        public FuncProperty(string name, IFunction defaultValue)
        {
            Type = PropertyType.Func;
            this.name = name;
            this.defaultValue = defaultValue;
        }

        public override bool CheckValue(object value)
        {
            IFunction f = value as IFunction;
            if (f == null)
            {
                return false;
            }
            return true;
        }
    }

    public class FuncListProperty : PropertyInfo
    {
        public FuncListProperty(string name, List<IFunction> defaultValue)
        {
            this.name = name;
            this.defaultValue = defaultValue;
        }

        public override bool CheckValue(object value)
        {
            List<IFunction> f = value as List<IFunction>;
            if (f == null)
            {
                return false;
            }
            return true;
        }
    }

    public class BoolProperty : PropertyInfo
    {
        public BoolProperty(string name, bool defaultValue)
        {
            this.name = name;
            this.defaultValue = defaultValue;
        }
        
        public override bool CheckValue(object value)
        {
            bool val;
            try
            {
                val = (bool)value;
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }
    }

    public class ColorProperty : PropertyInfo
    {
        public ColorProperty(string name, Color defaultValue)
        {
            this.name = name;
            this.defaultValue = defaultValue;
        }

        public override bool CheckValue(object value)
        {
            Color val;
            try
            {
                val = (Color) value;
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }
    }

}