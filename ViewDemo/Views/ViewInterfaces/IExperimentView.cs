﻿using System;
using System.Collections.Generic;
using Properties;

namespace ViewDemo
{
    public interface IExperimentView
    {
        void ShowNewExperiment(Guid expId, string expName);
        void ShowNewMultiDimExperiment(Guid expId, string expName);
        void ShowFunctionParams(string functionName, List<PropertyInfo> propertyInfos);
        
    }
}