﻿using Algorithms;
using Functions;

namespace ViewDemo
{
    public interface IMainFunctionView
    {
        void SetBounds(int dims, double[] min, double[] max, double[] center);
        void SetGrid(int dims, int gridSize, double[] x, double[] y, double[,] z);
        
        void SetFunction(IFunction f);
        void SetProblem(IProblem problem);

    }
}