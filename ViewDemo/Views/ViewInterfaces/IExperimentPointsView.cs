﻿using Algorithms;
using Algorithms.Methods;

namespace ViewDemo
{
    internal interface IExperimentPointsView
    {
        void SetBounds(int dims, double[] min, double[] max, double[] center);
        void SetProblem(IProblem problem);
        void ClearPoints();
        void AddPoint(MethodPoint p);
        void DeleteLastPoint(MethodPoint p);
       
    }
}