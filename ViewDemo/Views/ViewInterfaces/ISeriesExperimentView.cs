﻿using System;
using Absolut_Model;
using Algorithms;

namespace ViewDemo
{
    public interface ISeriesExperimentView
    {
        void DisplaySeriesStarted();
        void DisplaySubExperimentStarted(ISearchAlg alg, Guid id);
        void DisplaySeriesFinished(SeriesExperimentResult res);
        void DisplaySubExperimentFinished(ExperimentResult subId);
    }
}