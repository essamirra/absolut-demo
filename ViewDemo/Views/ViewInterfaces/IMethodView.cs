﻿using System.Collections.Generic;
using Functions;
using Properties;

namespace ViewDemo
{
    public interface IMethodView
    {
        void UpdateMethodProperties();
        void UpdateIterarion(int curIter);
        void CreateCustomFunctionMenu(string functionName, Dictionary<PropertyInfo, object> info);
        void СreateUsualFunctionMenu();
        void ShowAdditionalFunction(IFunction f);
    }
}