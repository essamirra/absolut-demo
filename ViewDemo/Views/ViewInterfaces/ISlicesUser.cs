﻿using System.Collections.Generic;

namespace ViewDemo
{
    public interface ISlicesUser
    {
        void SetFixedVars(Dictionary<int, double> fixedVars);
    }
}