﻿using Algorithms.Methods;

namespace ViewDemo
{
    public interface ISearchInformationView
    {
        void ClearPoints();
        void AddPoint(MethodPoint p);
        void DeleteLastPoint(MethodPoint p);
    }
}