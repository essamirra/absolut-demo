﻿using System.Windows.Forms;
using Algorithms.Methods;

namespace ViewDemo.Views.Labels
{
    public partial class BestPointLabel : UserControl, IBestPointView
    {
        public MethodPresenter Presenter;
        public BestPointLabel()
        {
            InitializeComponent();
        }

        

        public void UpdateBestPoint(MethodPoint bestPoint)
        {
            if (bestPoint == null) bestLabel.Text = "";
            else    bestLabel.Text = bestPoint.ToString();
        }

        public void SetBounds(int dims, double[] min, double[] max, double[] center)
        {
            
        }
    }
}
