﻿using System.Windows.Forms;
using Algorithms;
using Algorithms.Methods;

namespace ViewDemo.Labels
{
    public partial class CurrentIterationLabel : UserControl, IExperimentPointsView
    {

        public CurrentIterationLabel()
        {
            InitializeComponent();
        }

        public void SetBounds(int dims, double[] min, double[] max, double[] center)
        {

        }

        public void SetProblem(IProblem problem)
        {

        }

        public void ClearPoints()
        {

        }

        public void AddPoint(MethodPoint p)
        {
            curIterLabel.Text = p.iteration.ToString();
        }

        public void DeleteLastPoint(MethodPoint p)
        {
            curIterLabel.Text = p.iteration.ToString();
        }
    }
}
