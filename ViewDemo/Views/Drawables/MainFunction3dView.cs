﻿using System;
using Algorithms;
using Functions;
using Graphics;
using Graphics.Drawables;

namespace ViewDemo
{
    public class MainFunction3DView : ProblemaSurfaces, IProblemaView
    {
        private ProblemaPresenter _presenter;


        public MainFunction3DView() : base(new SimplePainter())
        {
            _presenter = new ProblemaPresenter(this);
        }

        void IProblemaView.SetProblem(IProblem problem) => SetProblem(problem);

        public void SetExperimentId(Guid expId)
        {
            _presenter.ExpId = expId;
        }
    

}
}