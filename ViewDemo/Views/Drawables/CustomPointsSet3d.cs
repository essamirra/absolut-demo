﻿using Graphics;
using OpenTK;

namespace ViewDemo.Views.Drawables
{
    public class CustomPointsSet3d : PointsSet3D
    {
        private int lastBin = -1;
        public void AddPoint(Vector3d point, int bin = 0)
        {
            Bins[bin].Add(point);
            lastBin = 0;
        }

        public void RemoveLastPoint()
        {
            if (lastBin != -1)
                Bins[lastBin].RemoveAt(Bins[lastBin].Count - 1);
        }
        

    }
}