﻿using System;
using System.Collections.Generic;
using Algorithms;
using Functions;
using Graphics;
using Graphics.Drawables;

namespace ViewDemo
{
    public class MultidimMainFunction3dView : ProblemaSurfaces, IProblemaView, ISlicesUser
    {
        private ProblemaPresenter _presenter;

        private Dictionary<int, double> _fixedVars;
        private IProblem _originalProblem;

        public MultidimMainFunction3dView() : base(new SimplePainter())
        {
            _presenter = new ProblemaPresenter(this);
        }

        void IProblemaView.SetProblem(IProblem problem)
        {
            _originalProblem = problem;
            SetProblem(ProblemFactory.BuildProblemWithFixedVars(_fixedVars, problem));

        }


        public void SetExperimentId(Guid expId)
        {
            _presenter.ExpId = expId;
        }


        public void SetFixedVars(Dictionary<int, double> fixedVars)
        {
            _fixedVars = fixedVars;
            SetProblem(ProblemFactory.BuildProblemWithFixedVars(_fixedVars, _originalProblem));
        }
    }
}