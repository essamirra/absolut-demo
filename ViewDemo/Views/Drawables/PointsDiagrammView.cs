﻿using System;
using Algorithms;
using Algorithms.Methods;
using Graphics;
using OpenTK;

namespace ViewDemo.Views
{
    public class PointsDiagrammView : Diagramm, IExperimentPointsView
    {
        private double[] _center;
        private bool _isBoundsSet;
        private double[] _max;
        private double[] _min;
        private readonly int grid = 10;


        public PointsDiagrammView() : base(BPainter.Build())
        {
        }

        void IExperimentPointsView.SetBounds(int dims, double[] min, double[] max, double[] center)
        {
            _min = min;
            _max = max;
            _center = center;
            _isBoundsSet = true;
            var x = new double[grid];
            var y = new double[grid];
            Utils.FillCoordArrays(max[0], min[0], grid, x);
            Utils.FillCoordArrays(max[1], min[1], grid, y);
            Utils.AdjustTo01(x, y, grid);
            SetGrid(x, y, grid);
        }


        public void ClearPoints()
        {
            pointsCount = new int[_gridSize, _gridSize];
        }


        public void AddPoint(MethodPoint p)
        {
            var p1 = new Vector3d(2 * (p.x[0] - _center[0]) / (_max[0] - _min[0]),
                (double) (2 * (p.y - _center[2]) / (_max[2] - _min[2])),
                2 * (p.x[1] - _center[1]) / (_max[1] - _min[1]));

            var x = p1.X > 1 ? 1 : p1.X;
            var y = p1.Z > 1 ? 1 : p1.Z;
            int i, j;
            var step = 2.0 / (grid - 1);
            i = (int) Math.Floor((x + 1) / step);
            j = (int) Math.Floor((y + 1) / step);
            if (i == grid - 1) i = grid - 2;
            if (j == grid - 1) j = grid - 2;
            pointsCount[i, j]++;
        }

        public void DeleteLastPoint(MethodPoint p)
        {
            var p1 = new Vector3d(2 * (p.x[0] - _center[0]) / (_max[0] - _min[0]),
                (double) (2 * (p.y - _center[2]) / (_max[2] - _min[2])),
                2 * (p.x[1] - _center[1]) / (_max[1] - _min[1]));

            var x = p1.X > 1 ? 1 : p1.X;
            var y = p1.Z > 1 ? 1 : p1.Z;
            int i, j;
            var step = 2.0 / (grid - 1);
            i = (int) Math.Floor((x + 1) / step);
            j = (int) Math.Floor((y + 1) / step);
            pointsCount[i, j]--;
        }

        public void SetProblem(IProblem problem)
        {
            throw new NotImplementedException();
        }
    }
}