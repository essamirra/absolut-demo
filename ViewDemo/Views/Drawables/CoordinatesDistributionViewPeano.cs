﻿using System;
using Algorithms;
using Algorithms.Methods;
using Graphics;
using OpenTK;

namespace ViewDemo.Views.Drawables
{
    internal class CoordinatesDistributionViewPeano : LinesOnAxisOnScreen, IExperimentPointsView

    {
        private double[] _center;
        private bool _isBoundsSet;
        private double[] _max;
        private double[] _min;

        public void SetBounds(int dims, double[] min, double[] max, double[] center)
        {
            _min = min;
            _max = max;
            _center = center;
            _isBoundsSet = true;
        }

        public void ClearPoints()
        {
            points.Clear();
        }

        public void AddPoint(MethodPoint p)
        {
            if (!_isBoundsSet) throw new ArgumentOutOfRangeException();
            points.Add(new Vector3d(2 * (p.evolventX - 1f/2),
                (double) (2 * (p.y - _center[2]) / (_max[2] - _min[2])),
                2 * (p.x[1] - _center[1]) / (_max[1] - _min[1])));
        }

        public void DeleteLastPoint(MethodPoint p)
        {
            if (points.Count > 0)
                points.RemoveAt(points.Count - 1);
        }

        public void SetProblem(IProblem problem)
        {
            throw new NotImplementedException();
        }
    }
}