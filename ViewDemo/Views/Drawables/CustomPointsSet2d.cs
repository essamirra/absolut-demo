﻿using System;
using System.Collections.Generic;
using Algorithms.Methods;
using Graphics;
using OpenTK;

namespace ViewDemo.Views.Drawables
{
    public class CustomPointsSet2d : PointsSet2D
    {
        private double[] _center;
        private bool _isBoundsSet;
        private double[] _max;
        private double[] _min;
        private int lastBin = -1;


        public void SetBounds(int dims, double[] min, double[] max, double[] center)
        {
            _min = min;
            _max = max;
            _center = center;
            _isBoundsSet = true;
            Bins.Add(0, new List<Vector3d>());
        }

        public void ClearPoints()
        {
            foreach (var b in Bins)
                b.Value.Clear();
            Bins.Clear();
        }

        public void AddPoint(MethodPoint p)
        {
            if (!_isBoundsSet) throw new ArgumentOutOfRangeException();
            if (p.y == null)
            {
                lastBin = 0;
                Bins[0].Add(new Vector3d(2 * (p.x[0] - _center[0]) / (_max[0] - _min[0]), 0,
                    2 * (p.x[1] - _center[1]) / (_max[1] - _min[1])));

            }
            else
            {
                lastBin = p.id_process + 1;
                if (!Bins.ContainsKey(p.id_process + 1)) Bins.Add(p.id_process + 1, new List<Vector3d>());
                Bins[p.id_process + 1].Add(new Vector3d(2 * (p.x[0] - _center[0]) / (_max[0] - _min[0]), 0,
                    2 * (p.x[1] - _center[1]) / (_max[1] - _min[1])));
            }

        }

        public void DeleteLastPoint(MethodPoint p)
        {
            if (lastBin != -1)
                Bins[lastBin].RemoveAt(Bins[lastBin].Count - 1);
        }


    }
}