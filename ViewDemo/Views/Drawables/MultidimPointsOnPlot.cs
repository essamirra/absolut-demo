﻿using System;
using System.Collections.Generic;
using Algorithms;
using Algorithms.Methods;
using Functions;
using Graphics;
using OpenTK;
using Properties;

namespace ViewDemo
{
    public class MultidimPointsOnPlot : PointsSet3D, IProblemaView, ISearchInformationView, IPropertyUser, ISlicesUser
    {
        private double[] _center;
        private bool _isBoundsSet;
        private double[] _max;
        private double[] _min;
        private int lastBin = -1;
        private PropertyProvider _propertyProvider = new PropertyProvider();
        private SearchInformationPresenter _pointsPresenter;
        private ProblemaPresenter _problemaPresenter;

        private Dictionary<int, double> _fixedVars;
        private IProblem _originalProblem;

        public MultidimPointsOnPlot()
        {
            RegisterProperty(new IntProperty("Grid size", 1, 128, 1, 64));
            _pointsPresenter = new SearchInformationPresenter(this);
            _problemaPresenter = new ProblemaPresenter(this);
            Bins.Add(0, new List<Vector3d>() );
        }

        public void SetExperimentId(Guid id)
        {

            _pointsPresenter.ExpId = id;
            _problemaPresenter.ExpId = id;
        }
        public void ClearPoints()
        {
            foreach (var b in Bins)
                b.Value.Clear();
            Bins.Clear();
            Bins.Add(0, new List<Vector3d>());

        }

        public void AddPoint(MethodPoint p)
        {
            if (!_isBoundsSet) throw new ArgumentOutOfRangeException();
            
            if (!Bins.ContainsKey(p.id_process+1)) Bins.Add(p.id_process+1, new List<Vector3d>());
            if (p.y == null)
            {
                
                Bins[0].Add(new Vector3d(2 * (p.x[0] - _center[0]) / (_max[0] - _min[0]),
                    (double)1,
                    2 * (p.x[1] - _center[1]) / (_max[1] - _min[1])));
                lastBin = 0;
            }
            else
            {
                Bins[p.id_process+1].Add(new Vector3d(2 * (p.x[0] - _center[0]) / (_max[0] - _min[0]),
                    (double)(2 * (p.y - _center[2]) / (_max[2] - _min[2])),
                    2 * (p.x[1] - _center[1]) / (_max[1] - _min[1])));
                lastBin = p.id_process + 1;
            }
           
        }

        public void DeleteLastPoint(MethodPoint p)
        {
            if (lastBin != -1)
                Bins[lastBin].RemoveAt(Bins[lastBin].Count - 1);
        }

        public void SetProblem(IProblem problem)
        {

            var realProblem = ProblemFactory.BuildProblemWithFixedVars(_fixedVars, _originalProblem);
            var gridSize = (int) GetProperty("Grid size");
            var newFunction = realProblem.Criterions[0];
            double[] x = Utils.FillCoordArrays(newFunction, 0, gridSize),
                y = Utils.FillCoordArrays(newFunction, 1, gridSize);
            var z = Utils.FillValues(newFunction, gridSize);
            Utils.FillZMatrix(gridSize, gridSize, z, x, y, newFunction);
            double[] min, max, center;
            Utils.GetInfo(x, y, z, out min, out max, out center);
            _min = min;
            _max = max;
            _center = center;
            _isBoundsSet = true;
        }

        public void SetProperty(string name, object value)
        {
            _propertyProvider.SetProperty(name, value);
        }

        public void SetProperties(Dictionary<string, object> propsValues)
        {
            _propertyProvider.SetProperties(propsValues);
        }

        public object GetProperty(string name)
        {
            return _propertyProvider.GetProperty(name);
        }

        public List<PropertyInfo> GetPropertiesInfo()
        {
            return _propertyProvider.GetPropertiesInfo();
        }

        public void RegisterProperty(PropertyInfo p)
        {
            _propertyProvider.RegisterProperty(p);
        }

        public void SetFixedVars(Dictionary<int, double> fixedVars)
        {
            _fixedVars = fixedVars;
            SetProblem(ProblemFactory.BuildProblemWithFixedVars(_fixedVars, _originalProblem));
        }
    }
}