﻿using System;
using System.Collections.Generic;
using Algorithms;
using Algorithms.Methods;
using Graphics;
using OpenTK;

namespace ViewDemo
{
    public class PointsOnPlotPeano : PointsSet2D, IExperimentPointsView
    {
        private double[] _center;
        private bool _isBoundsSet;
        private double[] _max;
        private double[] _min;
        private int lastBin = -1;


        public void SetBounds(int dims, double[] min, double[] max, double[] center)
        {
            _min = min;
            _max = max;
            _center = center;
            _isBoundsSet = true;
        }

        public void ClearPoints()
        {
            foreach (var b in Bins)
                b.Value.Clear();
            Bins.Clear();
        }

        public void AddPoint(MethodPoint p)
        {
            if (!_isBoundsSet) throw new ArgumentOutOfRangeException();
            lastBin = p.id_process;
            if (!Bins.ContainsKey(p.id_process)) Bins.Add(p.id_process, new List<Vector3d>());
            Bins[p.id_process].Add(new Vector3d(2 * (p.evolventX - 1f/2), (double) (2 * (p.y - _center[2]) / (_max[2] - _min[2])), 0));
        }

        public void DeleteLastPoint(MethodPoint p)
        {
            if (lastBin != -1)
                Bins[lastBin].RemoveAt(Bins[lastBin].Count - 1);
        }

        public void SetProblem(IProblem problem)
        {
            throw new NotImplementedException();
        }
    }
}