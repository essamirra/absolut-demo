﻿using Algorithms;
using Functions;
using Graphics;

namespace ViewDemo
{
    public class MainFunctionIsoLineView : ProblemaIsobars, IMainFunctionView
    {
        public void SetBounds(int dims, double[] min, double[] max, double[] center)
        {
        }

        public void SetGrid(int dims, int gridSize, double[] x, double[] y, double[,] z)
        {
            var i = 150;
            var newX = new double[i];
            var newY = new double[i];
            var newZ = new double[i, i];
            Utils.FillCoordArrays(x[gridSize - 1], x[0], i, newX);
            Utils.FillCoordArrays(y[gridSize - 1], y[0], i, newY);
            Utils.FillZMatrix(i, i, newZ, newX, newY, _f);
            Utils.AdjustTo01(newX, newY, newZ, i);
            SetSurface(i, newX, newY, newZ);
        }

        public void SetFunction(IFunction f)
        {
            SetDrawedFunction(f);
        }

        public void SetProblem(IProblem problem)
        {
            SetDrawedFunction(problem.Criterions[0]);
            UpdateProblem(problem);
        }
    }
}