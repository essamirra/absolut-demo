﻿using Functions;
using Graphics;
using Graphics.Drawables;


namespace ViewDemo
{
    public class FunctionView : Surface
    {
        public FunctionView(IFunction f) : base(new SimplePainter())
        {
            SetSurface(f);
        }
        public FunctionView() : base(new SimplePainter())
        {
            
        }

        public void SetFunction(IFunction f)
        {
            SetSurface(f);
        }
        
    }
}