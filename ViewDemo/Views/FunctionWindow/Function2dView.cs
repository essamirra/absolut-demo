﻿using System;
using System.Windows.Forms;
using Functions;
using Graphics;

namespace ViewDemo
{
    public class Function2dView : DrawablesControl
    {
        private readonly int _gridSize = 64;
        private TemperatureMap _function3DView;

        private readonly ToolStripMenuItem isoItem = new ToolStripMenuItem
        {
            Text = "Isoline view",
            Checked = false,
            CheckOnClick = true
        };

        private FunctionIsolines isoLine;

        private readonly ToolStripMenuItem temperatureItem = new ToolStripMenuItem
        {
            Text = "Temperature map view",
            Checked = true,
            CheckOnClick = true
        };

        public void SetFunction(IFunction f)
        {
            _function3DView = new TemperatureMap(new SimplePainter());
            double[] x = Utils.FillCoordArrays(f, 0, _gridSize),
                y = Utils.FillCoordArrays(f, 1, _gridSize);
            var z = Utils.FillValues(f, _gridSize);
            Utils.FillZMatrix(_gridSize, _gridSize, z, x, y, f);
            double[] min, max, center;
            Utils.GetInfo(x, y, z, out min, out max, out center);
            Utils.AdjustTo01(x, y, z, _gridSize);
            _function3DView.SetSurface(_gridSize, x, y, z);

            double[] x1 = Utils.FillCoordArrays(f, 0, 150),
                y1 = Utils.FillCoordArrays(f, 1, 150);
            var z1 = Utils.FillValues(f, 150);
            Utils.FillZMatrix(150, 150, z1, x1, y1, f);
            Utils.AdjustTo01(x1, y1, z1, 150);
            isoLine = new FunctionIsolines(150, x1, y1, z1);
            isoLine.SetDrawedFunction(f);
            AddDrawableObject(_function3DView);
            AddDrawableObject(isoLine);
            Drawables.RemoveAt(1);
            isoItem.CheckedChanged += IsoItemOnCheckedChanged;
            temperatureItem.CheckedChanged += TemperatureItemOnCheckedChanged;
            PropetiesMenuStrip.Items.Add(isoItem);
            PropetiesMenuStrip.Items.Add(temperatureItem);
        }

        private void TemperatureItemOnCheckedChanged(object sender, EventArgs eventArgs)
        {
            if (temperatureItem.Checked)
            {
                Drawables.RemoveAt(0);
                Drawables.Add(_function3DView);
                isoItem.Checked = false;
            }
            if (!isoItem.Checked && !temperatureItem.Checked) temperatureItem.Checked = true;
        }

        private void IsoItemOnCheckedChanged(object sender, EventArgs eventArgs)
        {
            if (isoItem.Checked)
            {
                Drawables.RemoveAt(0);
                Drawables.Add(isoLine);
                temperatureItem.Checked = false;
            }
            if (!isoItem.Checked && !temperatureItem.Checked) isoItem.Checked = true;
        }
    }
}