﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Algorithms;
using Algorithms.Methods;
using Functions;
using Graphics;
using Graphics.Drawables;
using ViewDemo.Views.Drawables;

namespace ViewDemo.Views.User_controls
{
    public partial class CustomProblem3dPlot : DrawablesControlThreading, IProblemaView
    {
        ProblemaSurfaces _problemaSurface = new ProblemaSurfaces(new SimplePainter());
        Surface _functionSurface = new Surface(new SimplePainter());
        ProblemaTemperatureMap _problemaTemperatureMap = new ProblemaTemperatureMap(new SimplePainter());
        TemperatureMap _temperatureMap = new TemperatureMap(new SimplePainter());
        CustomPointsSet3d _pointsSet3 = new CustomPointsSet3d();
        CustomPointsSet2d _pointsSet2 = new CustomPointsSet2d();
        private bool _problemaMode = false;
        private bool _2dMode = false;
        private int _gridSize = 64;

        public bool Mode2d
        {
            get { return _2dMode; }
            set
            {
                _2dMode = value;
                Drawables.Clear();
                SetCurrentDrawable();
            }
        }

        private void SetCurrentDrawable()
        {
            if (ProblemaMode && _2dMode)
            {
                AddDrawableObject(_problemaTemperatureMap);
                AddDrawableObject(_pointsSet2);
            }
            else if (ProblemaMode && !_2dMode)
            {
                AddDrawableObject(_problemaSurface);
                AddDrawableObject(_pointsSet3);
            }
            else if (!ProblemaMode && _2dMode)
            {
                AddDrawableObject(_temperatureMap);
                AddDrawableObject(_pointsSet2);
            }
            else if (!ProblemaMode && !_2dMode)
            {
                AddDrawableObject(_functionSurface);
                AddDrawableObject(_pointsSet3);
            }
        }

        public bool ProblemaMode
        {
            get { return _problemaMode; }
            set
            {
                Drawables.Clear();
                _problemaMode = value;
                SetCurrentDrawable();
            }
        }

        public CustomProblem3dPlot()
        {
            InitializeComponent();
        }


        public void SetProblem(IProblem problem)
        {
            if (ProblemaMode)
            {
                _problemaSurface.SetProblem(problem);
                IFunction function = problem.Criterions[0];
                double[] x = Utils.FillCoordArrays(function, 0, _gridSize),
                    y = Utils.FillCoordArrays(function, 1, _gridSize);
                var z = Utils.FillValues(function, _gridSize);
                Utils.FillZMatrix(_gridSize, _gridSize, z, x, y, function);
                double[] min, max, center;
                Utils.GetInfo(x, y, z, out min, out max, out center);
                Utils.AdjustTo01(x, y, z, _gridSize);
                _problemaTemperatureMap.SetSurface(_gridSize, x, y,z);
                _problemaTemperatureMap.SetNewProblem(problem);
            }
        }

        public void SetFunction(IFunction function)
        {
            if (!ProblemaMode)
            {
                _functionSurface.SetSurface(function);
                double[] x = Utils.FillCoordArrays(function, 0, _gridSize),
                    y = Utils.FillCoordArrays(function, 1, _gridSize);
                var z = Utils.FillValues(function, _gridSize);
                Utils.FillZMatrix(_gridSize, _gridSize, z, x, y, function);
                double[] min, max, center;
                Utils.GetInfo(x, y, z, out min, out max, out center);
                Utils.AdjustTo01(x, y, z, _gridSize);
                _temperatureMap.SetSurface(_gridSize, x,y,z);
            }
        }

        public void DeleteLastPoint(MethodPoint methodPoint)
        {
            _pointsSet2.DeleteLastPoint(methodPoint);
            _pointsSet3.RemoveLastPoint();

        }
    }
}