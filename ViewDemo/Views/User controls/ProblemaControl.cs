﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Algorithms;
using Functions;
using Graphics;
using Graphics.Drawables;
using Properties;

namespace ViewDemo.Views.User_controls
{
    public partial class ProblemaControl : DrawablesControlThreading, IProblemaView
    {
        private Surface _surface = new FunctionView();
        private Tiles _tiles;
        private ProblemaPresenter _problemaPresenter;
        private IProblem _problem;
        private bool isConstraints = false;
        private int funcNum = 0;
        List<string> comboBox1DataSource = new List<String>() { "Criteria", "Constraints" };
        List<string> comboBox1DataSource1 = new List<String>() { "Criteria" };
        public ProblemaControl()
        {
            InitializeComponent();
            comboBox1.DataSource = comboBox1DataSource;
            _tiles = new Tiles(_surface);
            AddDrawableObject(_tiles);
            _problemaPresenter = new ProblemaPresenter(this);
            numericUpDown1.Increment = 1;
            comboBox1.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        public void SetProblem(IProblem problem)
        {
            _problem = problem;
            comboBox1.DataSource = problem.Constraints.Count == 0 ? comboBox1DataSource1 : comboBox1DataSource;
            _surface.SetSurface(problem.Criterions[0]);
            NeedRedraw = true;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(_problem == null) return;
            var comboBox = sender as ComboBox;
            isConstraints = comboBox.SelectedItem.ToString() == "Constraints";
            int min = 1;
            int max = isConstraints ? _problem.NumberOfConstratins : _problem.NumberOfCriterions;
            ResetNumericUpDown(min,max);
            NeedRedraw = true;
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            funcNum = Convert.ToInt32(numericUpDown1.Value)-1;
            var displayedFunction = DisplayedFunction();
            _surface.SetSurface(displayedFunction);
            NeedRedraw = true;
        }

        private IFunction DisplayedFunction()
        {
            IFunction displayedFunction = isConstraints
                ? _problem.Constraints[funcNum]
                : _problem.Criterions[funcNum];
            return displayedFunction;
        }

        private void ResetNumericUpDown(int min, int max)
        {
            numericUpDown1.Maximum = max;
            numericUpDown1.Minimum = min;
            numericUpDown1.Value = 1;
            funcNum =0;
            IFunction displayedFunction = DisplayedFunction();
            _surface.SetSurface(displayedFunction);
            NeedRedraw = true;
        }

        public void SetExperimentId(Guid id)
        {
            _problemaPresenter.ExpId = id;
        }
    }

    
}
