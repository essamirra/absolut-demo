﻿namespace ViewDemo
{
    partial class SliceEditorControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.varLabel = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.var1CoefTextBox = new System.Windows.Forms.TextBox();
            this.var1 = new System.Windows.Forms.Label();
            this.var2CoefTextBox = new System.Windows.Forms.TextBox();
            this.var2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.constTextBox = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.AutoSize = true;
            this.tableLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 268F));
            this.tableLayoutPanel1.Controls.Add(this.varLabel, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label2, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 2, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(319, 106);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // varLabel
            // 
            this.varLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.varLabel.AutoSize = true;
            this.varLabel.Location = new System.Drawing.Point(3, 46);
            this.varLabel.Name = "varLabel";
            this.varLabel.Size = new System.Drawing.Size(22, 13);
            this.varLabel.TabIndex = 0;
            this.varLabel.Text = "var";
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(31, 46);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(13, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "=";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 7;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.12214F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 7.251908F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.8855F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.50382F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.870229F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30.15267F));
            this.tableLayoutPanel2.Controls.Add(this.var1CoefTextBox, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.var1, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.var2CoefTextBox, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.var2, 4, 0);
            this.tableLayoutPanel2.Controls.Add(this.label3, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.label5, 5, 0);
            this.tableLayoutPanel2.Controls.Add(this.constTextBox, 6, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(54, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(262, 100);
            this.tableLayoutPanel2.TabIndex = 2;
            // 
            // var1CoefTextBox
            // 
            this.var1CoefTextBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.var1CoefTextBox.Location = new System.Drawing.Point(3, 40);
            this.var1CoefTextBox.Name = "var1CoefTextBox";
            this.var1CoefTextBox.Size = new System.Drawing.Size(30, 20);
            this.var1CoefTextBox.TabIndex = 0;
            // 
            // var1
            // 
            this.var1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.var1.AutoSize = true;
            this.var1.Location = new System.Drawing.Point(39, 37);
            this.var1.Name = "var1";
            this.var1.Size = new System.Drawing.Size(25, 26);
            this.var1.TabIndex = 1;
            this.var1.Text = "free1";
            // 
            // var2CoefTextBox
            // 
            this.var2CoefTextBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.var2CoefTextBox.Location = new System.Drawing.Point(93, 40);
            this.var2CoefTextBox.Name = "var2CoefTextBox";
            this.var2CoefTextBox.Size = new System.Drawing.Size(31, 20);
            this.var2CoefTextBox.TabIndex = 3;
            // 
            // var2
            // 
            this.var2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.var2.AutoSize = true;
            this.var2.Location = new System.Drawing.Point(131, 43);
            this.var2.Name = "var2";
            this.var2.Size = new System.Drawing.Size(31, 13);
            this.var2.TabIndex = 4;
            this.var2.Text = "free2";
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(75, 43);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(12, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "+";
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(168, 43);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(11, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "+";
            // 
            // constTextBox
            // 
            this.constTextBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.constTextBox.Location = new System.Drawing.Point(185, 40);
            this.constTextBox.Name = "constTextBox";
            this.constTextBox.Size = new System.Drawing.Size(34, 20);
            this.constTextBox.TabIndex = 6;
            // 
            // SliceEditorControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "SliceEditorControl";
            this.Size = new System.Drawing.Size(319, 106);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label varLabel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TextBox var1CoefTextBox;
        private System.Windows.Forms.Label var1;
        private System.Windows.Forms.TextBox var2CoefTextBox;
        private System.Windows.Forms.Label var2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox constTextBox;
    }
}
