﻿namespace ViewDemo.Views.User_controls
{
    partial class MultidimMainFunction
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grid = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.firstFreeVarComboBox = new System.Windows.Forms.ComboBox();
            this.secondFreeVarComboBox = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.fixedVarTable = new System.Windows.Forms.TableLayoutPanel();
            this.multidimFunction3dPlot1 = new ViewDemo.Views.MultidimFunction3dPlot();
            this.functionalName = new System.Windows.Forms.Label();
            this.grid.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // grid
            // 
            this.grid.ColumnCount = 2;
            this.grid.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.71042F));
            this.grid.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 74.28958F));
            this.grid.Controls.Add(this.groupBox2, 0, 2);
            this.grid.Controls.Add(this.groupBox3, 0, 1);
            this.grid.Controls.Add(this.multidimFunction3dPlot1, 1, 1);
            this.grid.Controls.Add(this.functionalName, 0, 0);
            this.grid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grid.Location = new System.Drawing.Point(0, 0);
            this.grid.Name = "grid";
            this.grid.RowCount = 3;
            this.grid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.grid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 86.18182F));
            this.grid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 13.81818F));
            this.grid.Size = new System.Drawing.Size(739, 550);
            this.grid.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.tableLayoutPanel2);
            this.groupBox2.Location = new System.Drawing.Point(3, 479);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(183, 68);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Free Variables";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 41F));
            this.tableLayoutPanel2.Controls.Add(this.firstFreeVarComboBox, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.secondFreeVarComboBox, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.button1, 2, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(177, 49);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // firstFreeVarComboBox
            // 
            this.firstFreeVarComboBox.FormattingEnabled = true;
            this.firstFreeVarComboBox.Location = new System.Drawing.Point(3, 3);
            this.firstFreeVarComboBox.Name = "firstFreeVarComboBox";
            this.firstFreeVarComboBox.Size = new System.Drawing.Size(62, 21);
            this.firstFreeVarComboBox.TabIndex = 0;
            // 
            // secondFreeVarComboBox
            // 
            this.secondFreeVarComboBox.Dock = System.Windows.Forms.DockStyle.Left;
            this.secondFreeVarComboBox.FormattingEnabled = true;
            this.secondFreeVarComboBox.Location = new System.Drawing.Point(71, 3);
            this.secondFreeVarComboBox.Name = "secondFreeVarComboBox";
            this.secondFreeVarComboBox.Size = new System.Drawing.Size(62, 21);
            this.secondFreeVarComboBox.TabIndex = 1;
            // 
            // button1
            // 
            this.button1.Cursor = System.Windows.Forms.Cursors.Default;
            this.button1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button1.Location = new System.Drawing.Point(139, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(35, 43);
            this.button1.TabIndex = 2;
            this.button1.Text = "OK";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.fixedVarTable);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Location = new System.Drawing.Point(3, 23);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(184, 450);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Fixed Variables";
            // 
            // fixedVarTable
            // 
            this.fixedVarTable.ColumnCount = 1;
            this.fixedVarTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.fixedVarTable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fixedVarTable.Location = new System.Drawing.Point(3, 16);
            this.fixedVarTable.Name = "fixedVarTable";
            this.fixedVarTable.RowCount = 1;
            this.fixedVarTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.fixedVarTable.Size = new System.Drawing.Size(178, 431);
            this.fixedVarTable.TabIndex = 0;
            // 
            // multidimFunction3dPlot1
            // 
            this.multidimFunction3dPlot1.BackColor = System.Drawing.Color.Black;
            this.multidimFunction3dPlot1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.multidimFunction3dPlot1.ExpId = new System.Guid("00000000-0000-0000-0000-000000000000");
            this.multidimFunction3dPlot1.Location = new System.Drawing.Point(195, 25);
            this.multidimFunction3dPlot1.Margin = new System.Windows.Forms.Padding(5);
            this.multidimFunction3dPlot1.Name = "multidimFunction3dPlot1";
            this.multidimFunction3dPlot1.NeedRedraw = false;
            this.grid.SetRowSpan(this.multidimFunction3dPlot1, 2);
            this.multidimFunction3dPlot1.Size = new System.Drawing.Size(539, 520);
            this.multidimFunction3dPlot1.TabIndex = 4;
            this.multidimFunction3dPlot1.VSync = false;
            // 
            // functionalName
            // 
            this.functionalName.AutoSize = true;
            this.grid.SetColumnSpan(this.functionalName, 2);
            this.functionalName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.functionalName.Location = new System.Drawing.Point(3, 0);
            this.functionalName.Name = "functionalName";
            this.functionalName.Size = new System.Drawing.Size(733, 20);
            this.functionalName.TabIndex = 5;
            this.functionalName.Text = "label1";
            // 
            // MultidimMainFunction
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.grid);
            this.Name = "MultidimMainFunction";
            this.Size = new System.Drawing.Size(739, 550);
            this.grid.ResumeLayout(false);
            this.grid.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel grid;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.ComboBox firstFreeVarComboBox;
        private System.Windows.Forms.ComboBox secondFreeVarComboBox;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TableLayoutPanel fixedVarTable;
        private MultidimFunction3dPlot multidimFunction3dPlot1;
        private System.Windows.Forms.Label functionalName;
    }
}
