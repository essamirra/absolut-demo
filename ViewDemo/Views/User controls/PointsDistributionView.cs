﻿using System;
using Graphics;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using ViewDemo.Views.Drawables;

namespace ViewDemo.Views
{
    internal class PointsDistributionView : DrawablesControlThreading
    {
        private readonly int _gridSize = 64;
        private readonly PointsDiagrammView diagramm = new PointsDiagrammView();
        private readonly CoordinatesDistributionView x = new CoordinatesDistributionView();
        private readonly CoordinatesDistributionView y = new CoordinatesDistributionView();
        private ExperimentPointsPresenter _experimentPointsPresenter;
        private Guid _expId;

        public PointsDistributionView()
        {
            AddDrawableObject(diagramm);
            AddDrawableObject(x);
            AddDrawableObject(y);
            x.SetAxis(Vector3d.UnitX);
            y.SetAxis(Vector3d.UnitZ);
            Load += OnLoad;
        }

        public Guid ExpId
        {
            get { return _expId; }
            set
            {
                if (value == Guid.Empty) return;
                if (_experimentPointsPresenter != null)
                    throw new InvalidOperationException();
                _expId = value;
                _experimentPointsPresenter = new ExperimentPointsPresenter(_expId, _gridSize);
                _experimentPointsPresenter.AddView(diagramm);
                _experimentPointsPresenter.AddView(x);
                _experimentPointsPresenter.AddView(y);
            }
        }

        private void OnLoad(object sender, EventArgs eventArgs)
        {
            if (!DesignMode)
                GL.Enable(EnableCap.Lighting);
        }
    }
}