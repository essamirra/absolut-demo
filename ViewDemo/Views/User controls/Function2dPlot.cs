﻿using System;
using Graphics;
using OpenTK;
using ViewDemo.Views.Drawables;

namespace ViewDemo.Views
{
    internal class Function2dPlot : DrawablesControlThreading
    {
        private readonly MainFunctionPeanoView _function3DView = new MainFunctionPeanoView();
        private readonly PointsOnPlotPeano _points = new PointsOnPlotPeano();
        private readonly CoordinatesDistributionViewPeano _x = new CoordinatesDistributionViewPeano();
        private readonly  CoordinatesDistributionViewPeano _y = new CoordinatesDistributionViewPeano();
        private ExperimentPointsPresenter _experimentPointsPresenter;
        private Guid _expId;
        private readonly int _gridSize = 64;
        private Labels.CurrentPointLabel currentPointLabel1;
        private MainFunctionPresenter _mainFunctionPresenter;

        public Function2dPlot()
        {
            Camera =  new Camera(new Vector3d(0, 0, 6.5));
            AddDrawableObject(_points);
            AddDrawableObject(_function3DView);
            AddDrawableObject(_x);
            AddDrawableObject(_y);
            _x.SetAxis(Vector3d.UnitX);
            _y.SetAxis(Vector3d.UnitY);
        }

        public Guid ExpId
        {
            get { return _expId; }
            set
            {
                if(value == Guid.Empty) return;
                if (_experimentPointsPresenter != null || _mainFunctionPresenter != null)
                    throw new InvalidOperationException();
                _expId = value;
                _experimentPointsPresenter = new ExperimentPointsPresenter(_expId, _gridSize);
                _mainFunctionPresenter = new MainFunctionPresenter(_expId, _gridSize);
               _experimentPointsPresenter.AddView(_points);
                _experimentPointsPresenter.AddView(_x);
                _experimentPointsPresenter.AddView(_y);
                _mainFunctionPresenter.AddView(_function3DView);

            }
        }

        private void InitializeComponent()
        {
            this.currentPointLabel1 = new ViewDemo.Views.Labels.CurrentPointLabel();
            this.SuspendLayout();
            // 
            // currentPointLabel1
            // 
            this.currentPointLabel1.AutoSize = true;
            this.currentPointLabel1.Location = new System.Drawing.Point(0, 0);
            this.currentPointLabel1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.currentPointLabel1.Name = "currentPointLabel1";
            this.currentPointLabel1.Size = new System.Drawing.Size(73, 0);
            this.currentPointLabel1.TabIndex = 2;
            // 
            // Function2dPlot
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.Controls.Add(this.currentPointLabel1);
            this.Name = "Function2dPlot";
            this.Controls.SetChildIndex(this.button1, 0);
            this.Controls.SetChildIndex(this.currentPointLabel1, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }
    }
}