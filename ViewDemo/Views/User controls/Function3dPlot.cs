﻿using System;
using Graphics;

namespace ViewDemo.Views
{
    internal class Function3dPlot : DrawablesControlThreading
    {
        private readonly MainFunction3DView _function3DView = new MainFunction3DView();
        private readonly PointsOnPlot _points = new PointsOnPlot();
        private Guid _expId;


        public Function3dPlot()
        {
            AddDrawableObject(_points);
            AddDrawableObject(_function3DView);
        }

        public Guid ExpId
        {
            get { return _expId; }
            set
            {
                if (value == Guid.Empty) return;
                _expId = value;
                _function3DView.SetExperimentId(_expId);
                _points.SetExperimentId(_expId);
            }
        }
    }
}