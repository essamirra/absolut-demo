﻿using System;
using System.Windows.Forms;
using Graphics;
using ViewDemo.Properties;

namespace ViewDemo.Views
{
    internal class MainFunctionn2dView : DrawablesControlThreading
    {
        private readonly MainFunctionTemperatureMapView _function3DView = new MainFunctionTemperatureMapView();
        private readonly int _gridSize = 64;

        private readonly ToolStripMenuItem _isoItem = new ToolStripMenuItem
        {
            Text = Resources.MainFunctionn2dView__isoItem_Isoline_view,
            Checked = false,
            CheckOnClick = true
        };

        private readonly MainFunctionIsoLineView _isoLine = new MainFunctionIsoLineView();

        private readonly PointsOnPlot2D _points = new PointsOnPlot2D();

        private readonly ToolStripMenuItem _temperatureItem = new ToolStripMenuItem
        {
            Text = Resources.MainFunctionn2dView__temperatureItem_Temperature_map_view,
            Checked = true,
            CheckOnClick = true
        };

        private ExperimentPointsPresenter _experimentPointsPresenter;
        private Guid _expId;
        private MainFunctionPresenter _mainFunctionPresenter;

        public MainFunctionn2dView()
        {
            AddDrawableObject(_points);
            AddDrawableObject(_function3DView);
            AddDrawableObject(_isoLine);
            Drawables.RemoveAt(2);
            _isoItem.CheckedChanged += IsoItemOnCheckedChanged;
            _temperatureItem.CheckedChanged += TemperatureItemOnCheckedChanged;
            contextMenuStrip1.Items.Add(_isoItem);
            contextMenuStrip1.Items.Add(_temperatureItem);
        }

        public Guid ExpId
        {
            get { return _expId; }
            set
            {
                if (value == Guid.Empty) return;
                if (_experimentPointsPresenter != null || _mainFunctionPresenter != null)
                    throw new InvalidOperationException();
                _expId = value;
                _experimentPointsPresenter = new ExperimentPointsPresenter(_expId, _gridSize);
                _mainFunctionPresenter = new MainFunctionPresenter(_expId, _gridSize);
                _experimentPointsPresenter.AddView(_points);
                _mainFunctionPresenter.AddView(_function3DView);
                _mainFunctionPresenter.AddView(_isoLine);
            }
        }

        private void TemperatureItemOnCheckedChanged(object sender, EventArgs eventArgs)
        {
            if (_temperatureItem.Checked)
            {
                Drawables.RemoveAt(1);
                Drawables.Add(_function3DView);
                _isoItem.Checked = false;
            }
            if (!_isoItem.Checked && !_temperatureItem.Checked) _temperatureItem.Checked = true;
        }

        private void IsoItemOnCheckedChanged(object sender, EventArgs eventArgs)
        {
            if (_isoItem.Checked)
            {
                Drawables.RemoveAt(1);
                Drawables.Add(_isoLine);
                _temperatureItem.Checked = false;
            }
            if (!_isoItem.Checked && !_temperatureItem.Checked) _isoItem.Checked = true;
        }
    }
}