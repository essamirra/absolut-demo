﻿using System;
using System.Runtime.InteropServices;
using Absolut_Model;
using Absolut_Model.CreationStrategies;
using Algorithms;

namespace ViewDemo
{
    public class SeriesExperimentOperatorPresenter : ISeriesEventsListener
    {
        private readonly ISeriesExperimentsModel _experimentsModel = SeriesExperimentModelFactory.Build();
        private readonly ISeriesExperimentsView _view;

        public SeriesExperimentOperatorPresenter(ISeriesExperimentsView view)
        {
            _experimentsModel.Subscribe(this);
            _view = view;
        }

        public void OnNewExperimentAdded(Guid id, string name, int count)
        {
            _view.ShowNewSeriesExperiment(id, name, count);
        }

        public void OnExperimentDeleted(Guid id)
        {
            _view.HideDeletedSeriesExperiment(id);
        }

        public void OnSubExperimentFinished(Guid id, ExperimentResult subId)
        {
        }

        public void OnSeriesStarted(Guid id)
        {
        }

        public void OnSeriesFinished(Guid id)
        {
        }

        public void OnShowSubExperiment(ISearchAlg alg)
        {
            _view.AddNewExperiment(alg);
        }

        public void AddNewExperiment(string experimentName)
        {
            _experimentsModel.CreateNewSeries(experimentName);
        }

        public void OnSubExperimentStarted(Guid id, ISearchAlg subId, Guid subExpId)
        {
        }

        public void AddNewExperiment(string nameExperimentName, ISeriesCreator nameCreator)
        {
            _experimentsModel.CreateNewSeries(nameExperimentName, nameCreator);
        }
    }
}