﻿using System;
using System.Collections.Generic;
using Absolut_Model;
using Algorithms;
using Functions;

namespace ViewDemo
{
    public class ExperimentPresenter : IEventListener
    {
        private Guid _experimentId;
        private readonly IModel _model;
        private readonly List<IExperimentView> _views;

        public ExperimentPresenter()
        {
            _model = ModelFactory.Build();
            _model.Subscribe(this);
            _views = new List<IExperimentView>();
        }

        public void OnPointsReset()
        {
        }

        public void OnMainFunctionChanged()
        {
        }

        public void OnMethodChanged()
        {
        }

        public void OnNextStep()
        {
        }

        public void OnPrevStep()
        {
        }

        public void OnExperimentAdded()
        {
        }

        public void OnExperimentDeleted()
        {
        }

        public void OnSlicesChanged()
        {
        }

        public void AddView(IExperimentView v)
        {
            _views.Add(v);
        }

        public void AddNewExperiment(string nameExperimentName)
        {
            _experimentId = _model.AddExperiment(nameExperimentName, AlgFactory.Build("Agp"));

            foreach (var v in _views)
                v.ShowNewExperiment(_experimentId, _model.GetExperimentsNames()[_experimentId]);
            _model.SetProblem(ProblemFactory.BuildNotConstrainedProblem("Ackley2"), _experimentId);
       
        }

        public void DeleteExperiment(Guid expId)
        {
            _model.DeleteExperiment(expId);
        }

        public void OnProblemChanged()
        {
           
        }

        public void AddNewExperimentWithAlg(string subExperiment, ISearchAlg alg)
        {
            _experimentId = _model.AddExperiment(subExperiment, alg);

            foreach (var v in _views)
                v.ShowNewExperiment(_experimentId, _model.GetExperimentsNames()[_experimentId]);

            _model.SetProblem(alg.Problem, _experimentId);

        }

        public void SaveAlg(Guid childExpId, string path = null)
        {
            if(path == null)
                AlgFactory.SaveToXML(_model.GetAlg(childExpId), "Absolut Experiment " + System.DateTime.Now.ToString("MM/dd/yyyy HH-mm-ss") + ".xml");
            else
            {
                AlgFactory.SaveToXML(_model.GetAlg(childExpId), path);
            }
        }
    }
}