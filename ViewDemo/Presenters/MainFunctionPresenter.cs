﻿using System;
using System.Collections.Generic;
using Absolut_Model;
using Functions;
using Graphics;

namespace ViewDemo
{
    public class MainFunctionPresenter : IEventListener
    {
        private readonly Guid _expId;
        private readonly int _gridSize;
        private readonly IModel _m;
        private readonly List<IMainFunctionView> _views;

        public MainFunctionPresenter(Guid expId, int gridSize)
        {
            _views = new List<IMainFunctionView>();
            _m = ModelFactory.Build();
            _expId = expId;
            _m.SubscribeExperiment(this, expId);
            _gridSize = gridSize;
        }

        public void OnPointsReset()
        {
        }


        public void OnMainFunctionChanged()
        {
        }

        public void OnMethodChanged()
        {
        }

        public void OnNextStep()
        {
        }

        public void OnPrevStep()
        {
        }

        public void OnExperimentAdded()
        {
        }

        public void OnExperimentDeleted()
        {
        }

        public void OnSlicesChanged()
        {
            var newFunction = _m.GetProblem(_expId).Criterions[0];
            if (newFunction.Left.Count > 2)
                newFunction = new FixedByFormulaFunction(newFunction, _m.GetSlicesDictionary(_expId));
            double[] x = Utils.FillCoordArrays(newFunction, 0, _gridSize),
                y = Utils.FillCoordArrays(newFunction, 1, _gridSize);
            var z = Utils.FillValues(newFunction, _gridSize);
            Utils.FillZMatrix(_gridSize, _gridSize, z, x, y, newFunction);
            double[] min, max, center;
            Utils.GetInfo(x, y, z, out min, out max, out center);
            foreach (var v in _views)
            {
                //v.SetFunction(newFunction);
                v.SetProblem(_m.GetProblem(_expId));
                v.SetBounds(3, min, max, center);
            }
            //  Utils.AdjustTo01(x, y, z, _gridSize);
            foreach (var v in _views)
                v.SetGrid(3, _gridSize, x, y, z);
        }

        public void AddView(IMainFunctionView v)
        {
            _views.Add(v);
        }

        public void SetSlicesDictionary(Dictionary<int, IFunction> slices)
        {
            _m.SetSlicesDictionary(slices, _expId);
        }

        public void OnProblemChanged()
        {
            var newFunction = _m.GetProblem(_expId).Criterions[0];
            if (newFunction.Left.Count > 2)
                newFunction = new FixedByFormulaFunction(newFunction, _m.GetSlicesDictionary(_expId));
            double[] x = Utils.FillCoordArrays(newFunction, 0, _gridSize),
                y = Utils.FillCoordArrays(newFunction, 1, _gridSize);
            var z = Utils.FillValues(newFunction, _gridSize);
            Utils.FillZMatrix(_gridSize, _gridSize, z, x, y, newFunction);
            double[] min, max, center;
            Utils.GetInfo(x, y, z, out min, out max, out center);
            foreach (var v in _views)
            {
               // v.SetFunction(newFunction);
                v.SetBounds(3, min, max, center);
                v.SetProblem(_m.GetProblem(_expId));
            }
            //  Utils.AdjustTo01(x, y, z, _gridSize);
            foreach (var v in _views)
                v.SetGrid(3, _gridSize, x, y, z);
        }
    }
}