﻿using System;
using System.Collections.Generic;
using Absolut_Model;
using Algorithms.Methods;
using Functions;
using Graphics;

namespace ViewDemo
{
    internal class SearchInformationPresenter : IEventListener
    {
        private Guid _expId;

        public Guid ExpId
        {
            get { return _expId; }
            set
            {
                _expId = value;
                _m.SubscribeExperiment(this, _expId);
            }
        }

        private readonly IModel _m;
        private readonly ISearchInformationView _view;

        public SearchInformationPresenter(ISearchInformationView v)
        {
            _view = v;
            _m = ModelFactory.Build();
        }

        public void OnPointsReset()
        {
            _view.ClearPoints();
        }

        public void OnMainFunctionChanged()
        {
        }

        public void OnMethodChanged()
        {
            _view.ClearPoints();
        }

        public void OnNextStep()
        {
            var lastPoint = _m.GetLastPoint(_expId);
            _view.AddPoint(lastPoint);
        }

        public void OnPrevStep()
        {
            _view.DeleteLastPoint(_m.GetLastPoint(_expId));
        }

        public void OnExperimentAdded()
        {
        }

        public void OnExperimentDeleted()
        {
        }

        public void OnSlicesChanged()
        {
        }

        public void Next()
        {
            if (_m.HasNext(_expId)) _m.NextStep(_expId);
        }

        public void Prev()
        {
            _m.PrevStep(_expId);
        }

        public void OnProblemChanged()
        {
        }
    }
}