﻿using System;
using System.Collections.Generic;
using Absolut_Model;
using Algorithms.Methods;
using Functions;
using Graphics;

namespace ViewDemo
{
    internal class ExperimentPointsPresenter : IEventListener
    {
        private readonly Guid _expId;
        private readonly int _gridSize;
        private readonly IModel _m;
        private readonly List<IExperimentPointsView> _views;

        public ExperimentPointsPresenter(Guid expId, int gridSize)
        {
            _views = new List<IExperimentPointsView>();
            _m = ModelFactory.Build();
            _expId = expId;
            _m.SubscribeExperiment(this, expId);
            _gridSize = gridSize;
        }

        public void OnPointsReset()
        {
            foreach (var v in _views)
                v.ClearPoints();
        }

        public void OnMainFunctionChanged()
        {
            var newFunction = _m.GetProblem(_expId).Criterions[0];
            if (newFunction.Left.Count > 2)
                newFunction = new FixedByFormulaFunction(newFunction, _m.GetSlicesDictionary(_expId));
            double[] x = Utils.FillCoordArrays(newFunction, 0, _gridSize),
                y = Utils.FillCoordArrays(newFunction, 1, _gridSize);
            var z = Utils.FillValues(newFunction, _gridSize);
            Utils.FillZMatrix(_gridSize, _gridSize, z, x, y, newFunction);
            double[] min, max, center;
            Utils.GetInfo(x, y, z, out min, out max, out center);
            foreach (var v in _views)
            {
                v.ClearPoints();
                v.SetBounds(3, min, max, center);
            }
        }

        public void OnMethodChanged()
        {
            foreach (var v in _views)
                v.ClearPoints();
        }

        public void OnNextStep()
        {
            var lastPoint = _m.GetLastPoint(_expId);
            var slice = _m.GetSlicesDictionary(_expId);
            if (slice.Count == 0)
            {
                foreach (var v in _views)
                    v.AddPoint(lastPoint);
                return;
            }
            var args = new List<double>();
            var dimension = _m.GetProblem(_expId).Criterions[0].Left.Count;
            for (var i = 0; i < dimension; i++)
                if (!slice.ContainsKey(i))
                    args.Add(lastPoint.x[i]);
            var newFunction = new FixedByFormulaFunction(_m.GetProblem(_expId).Criterions[0],
                _m.GetSlicesDictionary(_expId));

            foreach (var v in _views)
            {
                var projectedPoint = new MethodPoint();
                projectedPoint.x = args;
                projectedPoint.evolventX = lastPoint.evolventX;

                projectedPoint.y = newFunction.Calc(args);


                v.AddPoint(projectedPoint);
            }
        }

        public void OnPrevStep()
        {
            foreach (var v in _views)
                v.DeleteLastPoint(_m.GetLastPoint(_expId));
        }

        public void OnExperimentAdded()
        {
        }

        public void OnExperimentDeleted()
        {
        }

        public void OnSlicesChanged()
        {
        }

        public void AddView(IExperimentPointsView v)
        {
            _views.Add(v);
        }

        public void Next()
        {
            if (_m.HasNext(_expId)) _m.NextStep(_expId);
        }

        public void Prev()
        {
            _m.PrevStep(_expId);
        }

        public void OnProblemChanged()
        {
            var newFunction = _m.GetProblem(_expId).Criterions[0];
            if (newFunction.Left.Count > 2)
                newFunction = new FixedByFormulaFunction(newFunction, _m.GetSlicesDictionary(_expId));
            double[] x = Utils.FillCoordArrays(newFunction, 0, _gridSize),
                y = Utils.FillCoordArrays(newFunction, 1, _gridSize);
            var z = Utils.FillValues(newFunction, _gridSize);
            Utils.FillZMatrix(_gridSize, _gridSize, z, x, y, newFunction);
            double[] min, max, center;
            Utils.GetInfo(x, y, z, out min, out max, out center);
            foreach (var v in _views)
            {
                v.ClearPoints();
                v.SetBounds(3, min, max, center);
            }
        }
    }

    internal class BestPointsPresenter : IEventListener
    {
        private readonly Guid _expId;
        private readonly IModel _m;
        private readonly List<IBestPointView> _views;

        public BestPointsPresenter(Guid expId)
        {
            _views = new List<IBestPointView>();
            _m = ModelFactory.Build();
            _expId = expId;
            _m.SubscribeExperiment(this, expId);
        }

        public void OnPointsReset()
        {
            UpdateBestPoint();
        }

        public void OnMainFunctionChanged()
        {
            UpdateBestPoint();
        }

        public void OnMethodChanged()
        {
            UpdateBestPoint();
        }

        public void OnNextStep()
        {
            UpdateBestPoint();
        }

        public void OnPrevStep()
        {
            UpdateBestPoint();
        }

        public void OnExperimentAdded()
        {
        }

        public void OnExperimentDeleted()
        {
        }

        public void OnSlicesChanged()
        {
        }

        public void AddView(IBestPointView v)
        {
            _views.Add(v);
        }

        private void UpdateBestPoint()
        {
            foreach (var v in _views)
                v.UpdateBestPoint(_m.GetBestPoint(_expId));
        }

        public void OnProblemChanged()
        {
            UpdateBestPoint();
        }
    }
}