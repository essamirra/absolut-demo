﻿using System;
using System.Collections.Generic;
using System.Linq;
using Absolut_Model;
using Algorithms;
using Algorithms.Methods;
using Functions;
using Properties;

namespace ViewDemo
{
    public class MethodPresenter : IEventListener
    {
        private readonly Guid _expId;
        private readonly IModel _m;
        private readonly List<IMethodView> _views;

        public MethodPresenter(Guid expId)
        {
            _views = new List<IMethodView>();
            _m = ModelFactory.Build();
            _expId = expId;
            _m.SubscribeExperiment(this, expId);
        }

        public void OnPointsReset()
        {
            UpdateIterationViews();
        }

        public void OnMainFunctionChanged()
        {
           
        }

        public void OnMethodChanged()
        {
            UpdatePropertyViews();
        }

        public void OnNextStep()
        {
            UpdateIterationViews();
        }

        public void OnPrevStep()
        {
            UpdateIterationViews();
        }

        public void OnExperimentAdded()
        {
        }

        public void OnExperimentDeleted()
        {
        }

        public MethodPoint BestPoint()
        {
            return _m.GetBestPoint(_expId);
        }

        public List<PropertyInfo> GetParamsInfo()
        {
            return _m.GetPropsNames(_expId);
        }

        public void SetProperties(Dictionary<string, object> propsValues)
        {
            if (propsValues.Count > 0)
                _m.SetProperties(_expId, propsValues);
        }

        public void SetCustomFunctionProperties(Dictionary<string, object> propsValues)
        {
            if (propsValues.Count > 0)
            {
                var f = (CustomFunctionDLL)_m.GetProblem(_expId).Criterions[0];
                var clone = (CustomFunctionDLL) f.Clone();
                clone.SetProperties(propsValues);
                foreach (var v in _views)
                    v.ShowAdditionalFunction(clone);
            }
        }

        public void SetExaminMethod(string dllPath, string configPath, string examinPath)
        {
            _m.SetAlg(_expId, AlgFactory.BuildExaminAlg(dllPath, configPath, examinPath));
            _m.SetProblem(ProblemFactory.BuildProblemFromDll(dllPath, configPath), _expId);
        }

        public void ShowFunction()
        {
           foreach (var v in _views)
                v.ShowAdditionalFunction(_m.GetProblem(_expId).Criterions[0]);
        }

        public void SetMainFunction(string name)
        {
            _m.SetProblem(ProblemFactory.BuildNotConstrainedProblem(name), _expId);
        }

        public void SetMainFunction(List<double> left, List<double> right, string formula)
        {
            _m.SetProblem(ProblemFactory.BuildFromTextInput(left, right, formula), _expId);
        }

        public void SetMainFunction(string dllpath, string xmlpath)
        {
            _m.SetProblem(ProblemFactory.BuildProblemFromDll(dllpath, xmlpath), _expId);
        }

        public void SetMethodWithOldFunction(string methodName)
        {

            var oldProblem = _m.GetProblem(_expId);
            _m.SetAlg(_expId, AlgFactory.Build(methodName));
            _m.SetProblem(oldProblem,_expId);
        }

        public void SetProperty(string propertyName, object newValue)
        {
            _m.SetProperty(propertyName, newValue, _expId);
        }

        public object GetProperty(string propertyName)
        {
            return _m.GetProperty(propertyName, _expId);
        }

        public void SetMethodType(string methodName)
        {
            _m.SetAlg(_expId, AlgFactory.Build(methodName));
        }

        private void UpdatePropertyViews()
        {
            foreach (var v in _views)
                v.UpdateMethodProperties();
        }

        private void CreateCustomFunctionWindow()
        {
            string name;

            var f = (CustomFunctionDLL) _m.GetProblem(_expId).Criterions[0];
            name = f.Name;
            var values = new Dictionary<PropertyInfo, object>();
            foreach (var i in f.GetPropertiesInfo())
                values.Add(i, f.GetProperty(i.name));
            foreach (var v in _views)
                v.CreateCustomFunctionMenu(name, values);
        }

        private void CreateFunctionWindow()
        {
            foreach (var v in _views)
                v.СreateUsualFunctionMenu();
        }

        private void UpdateIterationViews()
        {
            foreach (var v in _views)
                v.UpdateIterarion(_m.GetCurrentInter(_expId));
        }

        public void SetDirectMethod(string dllPath, string configPath, string directPath)
        {
            _m.SetAlg(_expId, AlgFactory.BuildDiRectAlg(dllPath, configPath, directPath));
            _m.SetProblem(ProblemFactory.BuildProblemFromDll(dllPath, configPath), _expId);
        }

        public void OnSlicesChanged()
        {
        }

        public void OnProblemChanged()
        {
            UpdatePropertyViews();
            var f = _m.GetProblem(_expId).Criterions[0] as CustomFunctionDLL;
            if (f == null)
                CreateFunctionWindow();
            else
                CreateCustomFunctionWindow();
        }

        public void SetConstrainedProblem(string methodChooseChoosedOption)
        {
         _m.SetAlg(_expId, AlgFactory.BuildConstrainedAlg(AlgFactory.ConstrainedOptions.FirstOrDefault()));
            _m.SetProblem(ProblemFactory.BuildStandartConstrainedProblem(methodChooseChoosedOption), _expId);
        }
    }
}