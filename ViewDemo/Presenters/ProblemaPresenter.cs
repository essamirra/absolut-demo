﻿using System;
using System.Collections.Generic;
using Absolut_Model;
using Functions;
using Graphics;

namespace ViewDemo
{
    public class ProblemaPresenter : IEventListener
    {
        private Guid _expId;

        public Guid ExpId
        {
            get { return _expId; }
            set
            {
                _expId = value;
                _m.SubscribeExperiment(this, _expId);
            }
        }

        private readonly IModel _m;
        private readonly IProblemaView _view;

        public ProblemaPresenter(IProblemaView v)
        {
            _view = v;
            _m = ModelFactory.Build();
        }

        public void OnPointsReset()
        {
        }


        public void OnMainFunctionChanged()
        {
        }

        public void OnMethodChanged()
        {
        }

        public void OnNextStep()
        {
        }

        public void OnPrevStep()
        {
        }

        public void OnExperimentAdded()
        {
        }

        public void OnExperimentDeleted()
        {
        }

        public void OnSlicesChanged()
        {
        }

        public void OnProblemChanged()
        {
            if (_expId == null) throw new ArgumentException();
            _view.SetProblem(_m.GetProblem(_expId));
        }
    }
}