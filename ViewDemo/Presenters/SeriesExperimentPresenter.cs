﻿using System;
using System.Collections.Generic;
using Absolut_Model;
using Algorithms;
using Properties;

namespace ViewDemo
{
    public class SeriesExperimentPresenter : ISeriesEventsListener
    {
        private ISeriesExperimentsModel _experimentsModel = SeriesExperimentModelFactory.Build();
        private ISeriesExperimentView _view;
        private Guid _id;

        public SeriesExperimentPresenter(ISeriesExperimentView view, Guid id)
        {
            _view = view;
            _id = id;
            _experimentsModel.Subscribe(id, this);
        }

        public void OnNewExperimentAdded(Guid id, string name, int count)
        {
        }

        public void OnExperimentDeleted(Guid id)
        {
        }

        public void OnSubExperimentStarted(Guid id, ISearchAlg subId, Guid subExpId)
        {
            _view.DisplaySubExperimentStarted(subId, subExpId);
        }

        public void OnSubExperimentFinished(Guid id, ExperimentResult subId)
        {
            _view.DisplaySubExperimentFinished(subId);
        }

        public void OnSeriesStarted(Guid id)
        {
            _view.DisplaySeriesStarted();
        }

        public void OnSeriesFinished(Guid id)
        {
            _view.DisplaySeriesFinished(_experimentsModel.GetSeriesExperimentResult(id));
        }

        public void OnShowSubExperiment(ISearchAlg alg)
        {
           
        }

        public void StartSeries()
        {
            _experimentsModel.StartSeries(_id);
        }

        public void StartNext()
        {
            _experimentsModel.StartNextInSeries(_id);
        }

        public void ShowDetailsOfSubExperiment(Guid subId)
        {
            _experimentsModel.ShowSubExperiment(_id, subId);
        }

        public void DeleteSeries()
        {
            _experimentsModel.DeleteSeries(_id);
        }

        public List<PropertyInfo> GetParamsInfo()
        {
            return _experimentsModel.GetProperties(_id);
        }

        public void SetProperties(Dictionary<string, object> propsValues)
        {
            if (propsValues.Count > 0)
                _experimentsModel.SetProperties(_id, propsValues);
        }

        public object GetProperty(string propertyName)
        {
            return _experimentsModel.GetProperty(propertyName, _id);
        }
    }
}