﻿using System;
using System.Windows.Forms;

namespace ViewDemo
{
    public partial class ExaminSeriesSettings : Form
    {
        public string dllPath;
        public string configPath;
        public string examinPath;

        public ExaminSeriesSettings()
        {
            InitializeComponent();
           
        }

        private void splitContainer1_Panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (dllPath == "" || configPath == "" || examinPath == "") return;
            dllPath = textBox1.Text;
            configPath = textBox2.Text;
            examinPath = textBox3.Text;
            DialogResult = DialogResult.OK;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog f = new OpenFileDialog())
            {
                DialogResult dr = f.ShowDialog();
                if (dr == DialogResult.OK)
                {
                    textBox1.Text = f.FileName;
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            using (FolderBrowserDialog f = new FolderBrowserDialog())
            {
                DialogResult dr = f.ShowDialog();
                if (dr == DialogResult.OK)
                {
                    textBox2.Text = f.SelectedPath;
                }
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog f = new OpenFileDialog())
            {
                DialogResult dr = f.ShowDialog();
                if (dr == DialogResult.OK)
                {
                    textBox3.Text = f.FileName;
                }
            }
        }
    }
}
