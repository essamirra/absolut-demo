﻿using System;
using System.Windows.Forms;
using Absolut_Model.CreationStrategies;
using Algorithms;

namespace ViewDemo.Dialogs
{
    public partial class SeriesExperimentCreationForm : Form
    {
        public string ExperimentName;
        public bool IsConstrained = false;

        public int ExperimentsCount => (int) numericUpDown1.Value;
        public ISeriesCreator Creator = new AbsolutMethodsSeriesCreator();
        public bool isAbsolut;
        public bool isExamin;

        public SeriesExperimentCreationForm()
        {
            InitializeComponent();
            textBox1.Text = "Series " + DateTime.Now;
        }


        protected void button1_Click(object sender, EventArgs e)
        {
            if (absolutMethodsRadioButton.Checked)
            {
                isAbsolut = true;
                Creator = new AbsolutMethodsSeriesCreator();
            }
               
            if (examinMethod.Checked)
            {
                isExamin = true;
                Creator = new ExaminMethodSeriesCreator();
            }

            DialogResult = DialogResult.OK;
            ExperimentName = textBox1.Text;
            Creator.SetCount((int) numericUpDown1.Value);
        }

        protected void button2_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }
    }
}