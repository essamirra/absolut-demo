﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Properties;


namespace ViewDemo.Views
{
    public partial class ParametersDialog : Form
    {
        public Dictionary<string, object> changedValues = new Dictionary<string, object>();
        public ParametersDialog(Dictionary<PropertyInfo,object> info )
        {
            InitializeComponent();
            foreach (var i in info)
            {
                if (i.Key.Type == PropertyType.Double)
                {
                    var input = new DoubleInput((DoubleProperty) i.Key, (double) i.Value) {Dock = DockStyle.Fill};
                    input.OnValueChanged += Input_OnValueChanged;
                    optionsTable.Controls.Add(input);
                }


                if (i.Key.Type == PropertyType.Integer)
                {
                    var input = new IntInput((IntProperty) i.Key, (int) i.Value) {Dock = DockStyle.Fill};
                    input.OnValueChanged += Input_OnValueChanged1;
                    optionsTable.Controls.Add(input);
                }
                if (i.Key.Type == PropertyType.String)
                {
                    var input = new StringInput((StringProperty) i.Key, (String) i.Value) {Dock = DockStyle.Fill};
                    optionsTable.Controls.Add(input);
                }


            }
        }

      

        private void Input_OnValueChanged1(object sender, System.EventArgs e)
        {
            var t = sender as IntInput;
            if (t != null && !changedValues.ContainsKey(t.PropertyName))
                changedValues.Add(t.PropertyName, t.GetValue());
            else if (t != null)
                changedValues[t.PropertyName] = t.GetValue();
        }

        private void Input_OnValueChanged(object sender, System.EventArgs e)
        {
            var t = sender as DoubleInput;
            if(t != null && !changedValues.ContainsKey(t.PropertyName))
                changedValues.Add(t.PropertyName,t.GetValue());
            else if (t != null)
                changedValues[t.PropertyName] = t.GetValue();
        }
        private void OKButton_Click(object sender, System.EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }

        private void CancelButton_Click(object sender, System.EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }
    }
}
