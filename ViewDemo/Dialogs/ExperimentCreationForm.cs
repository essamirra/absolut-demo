﻿using System;
using System.Windows.Forms;

namespace ViewDemo
{
    public partial class ExperimentCreationForm : Form
    {
        public string experimentName;
        public bool isConstrained = false;
        public ExperimentCreationForm()
        {
            InitializeComponent();
        }

        protected void button1_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            experimentName = textBox1.Text;

        }

        protected void button2_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }
    }
}
