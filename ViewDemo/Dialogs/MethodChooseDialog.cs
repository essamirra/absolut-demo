﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace ViewDemo
{
    public partial class MethodChooseDialog : Form
    {
        public string ChoosedOption;
        private string _standartOption;
        public MethodChooseDialog(List<string> options)
        {
            InitializeComponent();
            _standartOption = options.First();
            ChoosedOption = _standartOption;
            foreach (var option in options)
            {
                var t = new RadioButton() {Text =  option};
                if (option == _standartOption)
                    t.Checked = true;
                t.CheckedChanged += (sender, args) =>
                {
                    var r = sender as RadioButton;
                    if (r != null && r.Checked)
                        ChoosedOption = option;
                };
                optionsTable.Controls.Add(t);
            }
        }

        private void OKButton_Click(object sender, System.EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }

        private void CancelButton_Click(object sender, System.EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }
    }
}
