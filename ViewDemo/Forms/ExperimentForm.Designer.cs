﻿using ViewDemo.Views;

namespace ViewDemo
{
    partial class ExperimentForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this._keyboardChecker = new System.Windows.Forms.Timer(this.components);
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.bestPointLabel2 = new ViewDemo.Views.Labels.BestPointLabel();
            this.currentPointLabel1 = new ViewDemo.Views.Labels.CurrentPointLabel();
            this.bestPointLabel1 = new ViewDemo.Views.Labels.BestPointLabel();
            this.currentIterationLabel1 = new ViewDemo.Labels.CurrentIterationLabel();
            this.iter = new System.Windows.Forms.Label();
            this.Navigation = new System.Windows.Forms.GroupBox();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.button1 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.problemaControl1 = new ViewDemo.Views.User_controls.ProblemaControl();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this._mainFunctionn2DView1 = new ViewDemo.Views.MainFunctionn2dView();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.function3dPlot1 = new ViewDemo.Views.Function3dPlot();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.functionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1.SuspendLayout();
            this.Navigation.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // timer2
            // 
            this.timer2.Interval = 25;
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // _keyboardChecker
            // 
            this._keyboardChecker.Interval = 32;
            // 
            // groupBox1
            // 
            this.groupBox1.AutoSize = true;
            this.groupBox1.Controls.Add(this.bestPointLabel2);
            this.groupBox1.Controls.Add(this.currentPointLabel1);
            this.groupBox1.Controls.Add(this.bestPointLabel1);
            this.groupBox1.Controls.Add(this.currentIterationLabel1);
            this.groupBox1.Controls.Add(this.iter);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(451, 440);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(443, 112);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Problem info";
            // 
            // bestPointLabel2
            // 
            this.bestPointLabel2.AutoSize = true;
            this.bestPointLabel2.Location = new System.Drawing.Point(8, 69);
            this.bestPointLabel2.Name = "bestPointLabel2";
            this.bestPointLabel2.Size = new System.Drawing.Size(76, 17);
            this.bestPointLabel2.TabIndex = 9;
            // 
            // currentPointLabel1
            // 
            this.currentPointLabel1.AutoSize = true;
            this.currentPointLabel1.Location = new System.Drawing.Point(8, 45);
            this.currentPointLabel1.Margin = new System.Windows.Forms.Padding(2);
            this.currentPointLabel1.Name = "currentPointLabel1";
            this.currentPointLabel1.Size = new System.Drawing.Size(88, 19);
            this.currentPointLabel1.TabIndex = 8;
            // 
            // bestPointLabel1
            // 
            this.bestPointLabel1.AutoSize = true;
            this.bestPointLabel1.Location = new System.Drawing.Point(7, 38);
            this.bestPointLabel1.Margin = new System.Windows.Forms.Padding(4);
            this.bestPointLabel1.Name = "bestPointLabel1";
            this.bestPointLabel1.Size = new System.Drawing.Size(79, 0);
            this.bestPointLabel1.TabIndex = 6;
            // 
            // currentIterationLabel1
            // 
            this.currentIterationLabel1.AutoSize = true;
            this.currentIterationLabel1.Location = new System.Drawing.Point(7, 20);
            this.currentIterationLabel1.Margin = new System.Windows.Forms.Padding(4);
            this.currentIterationLabel1.Name = "currentIterationLabel1";
            this.currentIterationLabel1.Size = new System.Drawing.Size(118, 21);
            this.currentIterationLabel1.TabIndex = 5;
            // 
            // iter
            // 
            this.iter.AutoSize = true;
            this.iter.Location = new System.Drawing.Point(67, 18);
            this.iter.Name = "iter";
            this.iter.Size = new System.Drawing.Size(0, 13);
            this.iter.TabIndex = 4;
            // 
            // Navigation
            // 
            this.Navigation.Controls.Add(this.flowLayoutPanel1);
            this.Navigation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Navigation.Location = new System.Drawing.Point(451, 558);
            this.Navigation.Name = "Navigation";
            this.Navigation.Size = new System.Drawing.Size(443, 164);
            this.Navigation.TabIndex = 9;
            this.Navigation.TabStop = false;
            this.Navigation.Text = "Navigation";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.button1);
            this.flowLayoutPanel1.Controls.Add(this.button3);
            this.flowLayoutPanel1.Controls.Add(this.button4);
            this.flowLayoutPanel1.Controls.Add(this.button2);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(3, 16);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(437, 145);
            this.flowLayoutPanel1.TabIndex = 5;
            // 
            // button1
            // 
            this.button1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.button1.Location = new System.Drawing.Point(3, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "<<";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button3
            // 
            this.button3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.button3.Location = new System.Drawing.Point(84, 3);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 3;
            this.button3.Text = "start";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.button4.Location = new System.Drawing.Point(165, 3);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 4;
            this.button4.Text = "pause";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button2
            // 
            this.button2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.button2.Location = new System.Drawing.Point(246, 3);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 2;
            this.button2.Text = ">>";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.SystemColors.Control;
            this.groupBox4.Controls.Add(this.problemaControl1);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox4.Location = new System.Drawing.Point(451, 51);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(443, 383);
            this.groupBox4.TabIndex = 10;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Problema";
            // 
            // problemaControl1
            // 
            this.problemaControl1.BackColor = System.Drawing.Color.Black;
            this.problemaControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.problemaControl1.Location = new System.Drawing.Point(3, 16);
            this.problemaControl1.Margin = new System.Windows.Forms.Padding(5);
            this.problemaControl1.Name = "problemaControl1";
            this.problemaControl1.NeedRedraw = false;
            this.problemaControl1.Size = new System.Drawing.Size(437, 364);
            this.problemaControl1.TabIndex = 0;
            this.problemaControl1.VSync = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this._mainFunctionn2DView1);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Location = new System.Drawing.Point(3, 440);
            this.groupBox3.Name = "groupBox3";
            this.tableLayoutPanel1.SetRowSpan(this.groupBox3, 2);
            this.groupBox3.Size = new System.Drawing.Size(442, 282);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Criteria 2d projection";
            // 
            // _mainFunctionn2DView1
            // 
            this._mainFunctionn2DView1.BackColor = System.Drawing.Color.Black;
            this._mainFunctionn2DView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this._mainFunctionn2DView1.ExpId = new System.Guid("00000000-0000-0000-0000-000000000000");
            this._mainFunctionn2DView1.Location = new System.Drawing.Point(3, 16);
            this._mainFunctionn2DView1.Margin = new System.Windows.Forms.Padding(5);
            this._mainFunctionn2DView1.Name = "_mainFunctionn2DView1";
            this._mainFunctionn2DView1.NeedRedraw = false;
            this._mainFunctionn2DView1.Size = new System.Drawing.Size(436, 263);
            this._mainFunctionn2DView1.TabIndex = 2;
            this._mainFunctionn2DView1.VSync = false;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.groupBox3, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.groupBox2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.groupBox4, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.Navigation, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.menuStrip1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.groupBox1, 1, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.667073F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 53.65517F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.27586F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 23.33323F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(897, 725);
            this.tableLayoutPanel1.TabIndex = 11;
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.SystemColors.Control;
            this.groupBox2.Controls.Add(this.function3dPlot1);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(3, 51);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(442, 383);
            this.groupBox2.TabIndex = 12;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Criteria surface";
            // 
            // function3dPlot1
            // 
            this.function3dPlot1.BackColor = System.Drawing.Color.Black;
            this.function3dPlot1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.function3dPlot1.ExpId = new System.Guid("00000000-0000-0000-0000-000000000000");
            this.function3dPlot1.Location = new System.Drawing.Point(3, 16);
            this.function3dPlot1.Margin = new System.Windows.Forms.Padding(5);
            this.function3dPlot1.Name = "function3dPlot1";
            this.function3dPlot1.NeedRedraw = false;
            this.function3dPlot1.Size = new System.Drawing.Size(436, 364);
            this.function3dPlot1.TabIndex = 11;
            this.function3dPlot1.VSync = false;
            // 
            // menuStrip1
            // 
            this.menuStrip1.AllowMerge = false;
            this.tableLayoutPanel1.SetColumnSpan(this.menuStrip1, 2);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.functionToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(897, 24);
            this.menuStrip1.TabIndex = 9;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // functionToolStripMenuItem
            // 
            this.functionToolStripMenuItem.Name = "functionToolStripMenuItem";
            this.functionToolStripMenuItem.Size = new System.Drawing.Size(66, 20);
            this.functionToolStripMenuItem.Text = "Function";
            this.functionToolStripMenuItem.Click += new System.EventHandler(this.functionToolStripMenuItem_Click);
            // 
            // ExperimentForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(897, 725);
            this.Controls.Add(this.tableLayoutPanel1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "ExperimentForm";
            this.Text = "DemoVersion";
            this.Deactivate += new System.EventHandler(this.DemoVersion_Deactivate);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.DemoVersion_FormClosing);
            this.Leave += new System.EventHandler(this.DemoVersion_Leave);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.Navigation.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.Timer _keyboardChecker;
        private System.Windows.Forms.GroupBox groupBox1;
        private Views.Labels.BestPointLabel bestPointLabel2;
        private Views.Labels.CurrentPointLabel currentPointLabel1;
        private Views.Labels.BestPointLabel bestPointLabel1;
        private Labels.CurrentIterationLabel currentIterationLabel1;
        private System.Windows.Forms.Label iter;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.GroupBox groupBox3;
        private MainFunctionn2dView _mainFunctionn2DView1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox Navigation;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.GroupBox groupBox2;
        private Function3dPlot function3dPlot1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem functionToolStripMenuItem;
        private Views.User_controls.ProblemaControl problemaControl1;
        // private Views.Drawables.MethodPointsTable methodPointsTable1;
    }
}