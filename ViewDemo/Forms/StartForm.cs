﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Absolut_Model.CreationStrategies;
using Algorithms;
using Functions;
using Properties;
using ViewDemo.Dialogs;
using ViewDemo.Series;
using ViewDemo.Views;

namespace ViewDemo
{
    public partial class StartForm : Form, IExperimentView, ISeriesExperimentsView
    {
        private readonly ExperimentPresenter _experimentPresenter;
        private readonly SeriesExperimentOperatorPresenter _seriesExperimentOperatorPresenter;

        public StartForm()
        {
            InitializeComponent();
            _experimentPresenter = new ExperimentPresenter();
            _experimentPresenter.AddView(this);
            _seriesExperimentOperatorPresenter = new SeriesExperimentOperatorPresenter(this);
        }

        public void ShowNewExperiment(Guid expId, string expName)
        {
            var child = new ExperimentForm(expId) {MdiParent = this, Text = expName};
            child.Show();
        }

        public void ShowFunctionParams(string functionName, List<PropertyInfo> propertyInfos)
        {
            throw new NotImplementedException();
        }

        public void ShowNewMultiDimExperiment(Guid expId, string expName)
        {
            throw new NotImplementedException();
        }


        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            using (var name = new ExperimentCreationForm())
            {
                var dr = name.ShowDialog();
                if (dr != DialogResult.OK) return;
                _experimentPresenter.AddNewExperiment(name.experimentName);
                menuStrip2.Visible = true;
            }
        }

        private void standartMethodToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (var c in MdiChildren)
            {
                var form = c as ExperimentForm;
                form?.Stop();
            }
            using (var methodChoose = new MethodChooseDialog(AlgFactory.Options))
            {
                var dr = methodChoose.ShowDialog();
                if (dr != DialogResult.OK) return;
                var child = ActiveMdiChild as ExperimentForm;
                if (child == null) return;
                var methodPresenter = new MethodPresenter(child.ExpId);
                methodPresenter.SetMethodWithOldFunction(methodChoose.ChoosedOption);
            }
        }


        private void standartToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (var c in MdiChildren)
            {
                var form = c as ExperimentForm;
                form?.Stop();
            }
            using (var methodChoose = new MethodChooseDialog(ProblemFactory.NotConstrainedOptionsList))
            {
                var dr = methodChoose.ShowDialog();
                if (dr != DialogResult.OK) return;
                var child = ActiveMdiChild as ExperimentForm;
                if (child == null) return;
                var methodPresenter = child.Presenter;
                methodPresenter.SetMainFunction(methodChoose.ChoosedOption);
            }
        }

        private void parametersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var child = ActiveMdiChild as ExperimentForm;
            foreach (var c in MdiChildren)
            {
                var form = c as ExperimentForm;
                form?.Stop();
            }
            if (child == null) return;
            var methodPresenter = child.Presenter;
            var values = methodPresenter.GetParamsInfo().ToDictionary(i => i, i => methodPresenter.GetProperty(i.name));
            using (var paramsChoose = new ParametersDialog(values))
            {
                var dr = paramsChoose.ShowDialog();
                if (dr == DialogResult.OK)
                    methodPresenter.SetProperties(paramsChoose.changedValues);
            }
        }

        private void newProblemToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (var c in MdiChildren)
            {
                var form = c as ExperimentForm;
                form?.Stop();
            }
            using (var methodChoose = new FormulaInputDialog())
            {
                var dr = methodChoose.ShowDialog();
                if (dr != DialogResult.OK) return;
                var child = ActiveMdiChild as ExperimentForm;
                if (child == null) return;
                var methodPresenter = child.Presenter;
                methodPresenter.SetMainFunction(methodChoose.LeftBoundary, methodChoose.RightBoundary,
                    methodChoose.Formula);
            }
        }

        private void dLLToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (var c in MdiChildren)
            {
                var form = c as ExperimentForm;
                form?.Stop();
            }
            using (var methodChoose = new DLLSettings())
            {
                var dr = methodChoose.ShowDialog();
                if (dr != DialogResult.OK) return;
                using (var xml = new OpenFileDialog() {Title = "Open .dll file"})
                {
                    var child = ActiveMdiChild as ExperimentForm;
                    if (child == null) return;
                    var methodPresenter = child.Presenter;
                    try
                    {
                        methodPresenter.SetMainFunction(methodChoose.dllPath, methodChoose.configPath);
                    }
                    catch (Exception exception)
                    {
                        Console.Write(exception);
                        MessageBox.Show("Cant open dll");
                    }
                }
            }
        }

        private void examinToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (var c in MdiChildren)
            {
                var form = c as ExperimentForm;
                form?.Stop();
            }
            using (var examin = new ExaminSettings())
            {
                var dr = examin.ShowDialog();
                if (dr != DialogResult.OK) return;
                var child = ActiveMdiChild as ExperimentForm;
                if (child == null) return;
                var methodPresenter = child.Presenter;
                methodPresenter.SetExaminMethod(examin.dllPath, examin.configPath, examin.examinPath);
            }
        }

        private void cascadeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.Cascade);
        }

        private void mosaicToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileHorizontal);
        }

        private void horizontalMosaicToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileVertical);
        }

        private void directToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (var c in MdiChildren)
            {
                var form = c as ExperimentForm;
                form?.Stop();
            }
            using (var examin = new ExaminSettings())
            {
                var dr = examin.ShowDialog();
                if (dr != DialogResult.OK) return;
                var child = ActiveMdiChild as ExperimentForm;
                if (child == null) return;
                var methodPresenter = child.Presenter;
                methodPresenter.SetDirectMethod(examin.dllPath, examin.configPath, examin.examinPath);
            }
        }

        private void constrainedToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (var c in MdiChildren)
            {
                var form = c as ExperimentForm;
                form?.Stop();
            }
            using (var methodChoose = new MethodChooseDialog(ProblemFactory.ConstrainedOptions))
            {
                var dr = methodChoose.ShowDialog();
                if (dr != DialogResult.OK) return;
                var child = ActiveMdiChild as ExperimentForm;
                if (child == null) return;
                var methodPresenter = child.Presenter;
                methodPresenter.SetConstrainedProblem(methodChoose.ChoosedOption);
            }
        }

        private void newSeriesExperimentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (var name = new SeriesExperimentCreationForm())
            {
                var dr = name.ShowDialog();
                if (dr != DialogResult.OK) return;
                if (name.isAbsolut)
                {
                    using (var methodChoose = new MethodChooseDialog(AlgFactory.Options))
                    {
                        var methodName = methodChoose.ShowDialog();
                        if (methodName != DialogResult.OK) return;
                        name.Creator.UseMethod(methodChoose.ChoosedOption);
                    }
                }
                else if (name.isExamin)
                {
                    using (var examin = new ExaminSeriesSettings())
                    {
                        var examinRes = examin.ShowDialog();
                        if (examinRes != DialogResult.OK) return;
                        name.Creator.UseExamin(examin.examinPath, examin.dllPath, examin.configPath);

                    }
                }

                _seriesExperimentOperatorPresenter.AddNewExperiment(name.ExperimentName, name.Creator);
                // menuStrip2.Visible = true;
            }
        }

        public void ShowNewSeriesExperiment(Guid id, String expName, int count)
        {
            var child = new ExperimentsDemo(id, count) {MdiParent = this, Text = expName};
            child.Show();
        }

        public void HideDeletedSeriesExperiment(Guid id)
        {
        }

        public void AddNewExperiment(ISearchAlg alg)
        {
            _experimentPresenter.AddNewExperimentWithAlg("Sub experiment", alg);
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (var c in MdiChildren)
            {
                var form = c as ExperimentForm;
                form?.Stop();
            }

            using (var openDialog = new OpenFileDialog())
            {
                openDialog.Multiselect = false;
                openDialog.CheckFileExists = true;
                var dr = openDialog.ShowDialog();
                if (dr != DialogResult.OK) return;
                try
                {
                    _experimentPresenter.AddNewExperimentWithAlg(openDialog.FileName, AlgFactory.OpenFromXML(openDialog.FileName));
                    menuStrip2.Visible = true;
                }
                catch (Exception exception)
                {
                    Console.WriteLine(exception);
                    MessageBox.Show("Can not read XML file - please check");
                }
                
            }
            
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (var c in MdiChildren)
            {
                var form = c as ExperimentForm;
                form?.Stop();
            }
            var child = ActiveMdiChild as ExperimentForm;
            if (child == null) return;
            try
            {
                _experimentPresenter.SaveAlg(child.ExpId);
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
                MessageBox.Show("Can not save XML file - please check your disk space");
            }

            

        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach(var c in MdiChildren)
            {
                var form = c as ExperimentForm;
                form?.Stop();
            }

            using (var saveFile = new SaveFileDialog())
            {
                saveFile.CreatePrompt = true;
                saveFile.OverwritePrompt = true;
                saveFile.DefaultExt = ".xml";
                saveFile.AddExtension = true;
                var dr = saveFile.ShowDialog();
                if (dr != DialogResult.OK) return;
                var child = ActiveMdiChild as ExperimentForm;
                if (child == null) return;
                try
                {
                    _experimentPresenter.SaveAlg(child.ExpId, saveFile.FileName);
                }
                catch (Exception exception)
                {
                    Console.WriteLine(exception);
                    MessageBox.Show("Can not save XML file - please check your disk space");
                }
            }
                
        }
    }
}