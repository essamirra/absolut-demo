﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Windows.Input;
using Algorithms;
using Functions;
using Graphics;
using Properties;
using ViewDemo.Views;

namespace ViewDemo
{
    public partial class ExperimentForm : Form, IMethodView, IMainFunctionView
    {
        private readonly List<DrawablesControlThreading> _d = new List<DrawablesControlThreading>();
        private readonly ExperimentPointsPresenter _experimentPointsPresenter;
        private readonly ExperimentPresenter _experimentPresenter;
        private readonly MainFunctionPresenter _mainFunctionPresenter;
        private Dictionary<PropertyInfo, object> _customFunctionParameters = new Dictionary<PropertyInfo, object>();
        private double _lastUpdateTime;

        public Guid ExpId;

        public ExperimentForm(Guid expId)
        {
            InitializeComponent();
            var gridSize = 128;
            ExpId = expId;
            _mainFunctionPresenter = new MainFunctionPresenter(expId, gridSize);
            _mainFunctionPresenter.AddView(this);
            _experimentPointsPresenter = new ExperimentPointsPresenter(expId, gridSize);
            Presenter = new MethodPresenter(expId);

            _experimentPresenter = new ExperimentPresenter();
            var bestPointPresenter = new BestPointsPresenter(expId);
            function3dPlot1.ExpId = expId;
            _mainFunctionn2DView1.ExpId = expId;
            problemaControl1.SetExperimentId(expId);
            //pointsDistributionView1.BindView(_experimentPointsPresenter);
            _experimentPointsPresenter.AddView(currentPointLabel1);
            _experimentPointsPresenter.AddView(currentIterationLabel1);
            bestPointPresenter.AddView(bestPointLabel2);

            _keyboardChecker.Interval = 32;
            _keyboardChecker.Tick += KeyboardCheckerOnTick;
            _keyboardChecker.Start();
            _d.Add(function3dPlot1);
            _d.Add(_mainFunctionn2DView1);
            _d.Add(problemaControl1);
            //_d.Add(pointsDistributionView1);
        }

        public MethodPresenter Presenter { get; }

        public void SetBounds(int dims, double[] min, double[] max, double[] center)
        {
        }

        public void SetGrid(int dims, int gridSize, double[] x, double[] y, double[,] z)
        {
        }

        public void SetFunction(IFunction f)
        {
           // pointsDistributionView1.NeedRedraw = true;
            foreach (var control in _d)
                control.NeedRedraw = true;
        }

        public void SetProblem(IProblem problem)
        {
        }

        public void UpdateMethodProperties()
        {
        }

        public void UpdateIterarion(int curIter)
        {
        }


        public void CreateCustomFunctionMenu(string functionName, Dictionary<PropertyInfo, object> info)
        {
            _customFunctionParameters = info;
            functionToolStripMenuItem.Click -= FunctionToolStripMenuItemOnClick;
            functionToolStripMenuItem.Click -= MenuOptionOnClick;
            functionToolStripMenuItem.Click += MenuOptionOnClick;
        }

        public void СreateUsualFunctionMenu()
        {
            functionToolStripMenuItem.Click -= MenuOptionOnClick;
            functionToolStripMenuItem.Click -= FunctionToolStripMenuItemOnClick;
            functionToolStripMenuItem.Click += FunctionToolStripMenuItemOnClick;
        }

        public void ShowAdditionalFunction(IFunction f)
        {
            if (f.Left.Count <= 2)
            {
                var window = new FunctionWindow(f);
                window.Show();
            }
            else
            {
                var window = new SliceEditorForm(f);
                var dr = window.ShowDialog();
                if (dr == DialogResult.OK && window.SlicePlanes != null)
                    _mainFunctionPresenter.SetSlicesDictionary(window.SlicePlanes);
            }
        }

        private void KeyboardCheckerOnTick(object sender, EventArgs eventArgs)
        {
            var currentTime = Environment.TickCount / 1000.0;
            var delta = currentTime - _lastUpdateTime;
            _lastUpdateTime = currentTime;
            const double RotationSpeed = Math.PI / 2;
            if (Keyboard.IsKeyDown(Key.W))
                foreach (var control in _d)
                {
                    control.Camera.RotateAroundX(RotationSpeed * delta);
                    control.NeedRedraw = true;
                }
            if (Keyboard.IsKeyDown(Key.A))
                foreach (var control in _d)
                {
                    control.Camera.RotateAroundY(-RotationSpeed * delta);
                    control.NeedRedraw = true;
                }
            if (Keyboard.IsKeyDown(Key.S))
                foreach (var control in _d)
                {
                    control.Camera.RotateAroundX(-RotationSpeed * delta);
                    control.NeedRedraw = true;
                }
            if (Keyboard.IsKeyDown(Key.D))
                foreach (var control in _d)
                {
                    control.Camera.RotateAroundY(RotationSpeed * delta);
                    control.NeedRedraw = true;
                }
        }

        public void Stop()
        {
            timer2.Stop();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            timer2.Stop();
            _experimentPointsPresenter.Prev();
            //pointsDistributionView1.NeedRedraw = true;
            foreach (var control in _d)
                control.NeedRedraw = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            timer2.Stop();
            _experimentPointsPresenter.Next();
           // pointsDistributionView1.NeedRedraw = true;
            foreach (var control in _d)
                control.NeedRedraw = true;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            timer2.Start();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            timer2.Stop();
        }

        private void DemoVersion_Deactivate(object sender, EventArgs e)
        {
            Invalidate();
        }

        private void DemoVersion_FormClosing(object sender, FormClosingEventArgs e)
        {
            _experimentPresenter.DeleteExperiment(ExpId);
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            _experimentPointsPresenter.Next();
            foreach (var control in _d)
                control.NeedRedraw = true;
        }

        private void DemoVersion_Leave(object sender, EventArgs e)
        {
            timer2.Stop();
        }

        private void FunctionToolStripMenuItemOnClick(object sender, EventArgs eventArgs)
        {
            Presenter.ShowFunction();
        }

        private void MenuOptionOnClick(object sender, EventArgs eventArgs)
        {
            var paramsDialog = new ParametersDialog(_customFunctionParameters);
            var dr = paramsDialog.ShowDialog();
            if (dr == DialogResult.OK)
                Presenter.SetCustomFunctionProperties(paramsDialog.changedValues);
        }

        private void functionToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }
    }
}