﻿namespace ViewDemo.Forms
{
    partial class DashboardForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this._keyboardCheckerTimer = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // _keyboardCheckerTimer
            // 
            this._keyboardCheckerTimer.Tick += new System.EventHandler(this.KeyboardCheckerOnTick);
            // 
            // DashboardForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(632, 497);
            this.Name = "DashboardForm";
            this.Text = "DashboardForm";
            this.Activated += new System.EventHandler(this.DashboardForm_Activated);
            this.Deactivate += new System.EventHandler(this.DashboardForm_Deactivate);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Timer _keyboardCheckerTimer;
    }
}