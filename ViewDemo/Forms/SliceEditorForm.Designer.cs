﻿using ViewDemo.Views;
namespace ViewDemo
{
    partial class SliceEditorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this._mainGrid = new System.Windows.Forms.TableLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.yLabel = new System.Windows.Forms.Label();
            this.xLabel = new System.Windows.Forms.Label();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.firstFreeVarComboBox = new System.Windows.Forms.ComboBox();
            this.secondFreeVarComboBox = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.fixedVars = new System.Windows.Forms.GroupBox();
            this.fixedVarsTable = new System.Windows.Forms.TableLayoutPanel();
            this.createSliceButton = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.customProblem3dPlot1 = new ViewDemo.Views.User_controls.CustomProblem3dPlot();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this._mainGrid.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.fixedVars.SuspendLayout();
            this.SuspendLayout();
            // 
            // _mainGrid
            // 
            this._mainGrid.ColumnCount = 2;
            this._mainGrid.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 39.54704F));
            this._mainGrid.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60.45296F));
            this._mainGrid.Controls.Add(this.panel1, 1, 0);
            this._mainGrid.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this._mainGrid.Controls.Add(this.createSliceButton, 0, 1);
            this._mainGrid.Controls.Add(this.button2, 0, 2);
            this._mainGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._mainGrid.Location = new System.Drawing.Point(0, 0);
            this._mainGrid.Name = "_mainGrid";
            this._mainGrid.RowCount = 3;
            this._mainGrid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 92.65734F));
            this._mainGrid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.342658F));
            this._mainGrid.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this._mainGrid.Size = new System.Drawing.Size(831, 602);
            this._mainGrid.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.tableLayoutPanel1);
            this.panel1.Controls.Add(this.customProblem3dPlot1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(331, 3);
            this.panel1.Name = "panel1";
            this._mainGrid.SetRowSpan(this.panel1, 3);
            this.panel1.Size = new System.Drawing.Size(497, 596);
            this.panel1.TabIndex = 0;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.label1, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.yLabel, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.xLabel, 1, 2);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(391, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(109, 66);
            this.tableLayoutPanel1.TabIndex = 1;
            this.tableLayoutPanel1.CellPaint += new System.Windows.Forms.TableLayoutCellPaintEventHandler(this.tableLayoutPanel1_CellPaint);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Location = new System.Drawing.Point(57, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 22);
            this.label1.TabIndex = 0;
            this.label1.Text = "Z";
            // 
            // yLabel
            // 
            this.yLabel.AutoSize = true;
            this.yLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.yLabel.Location = new System.Drawing.Point(57, 22);
            this.yLabel.Name = "yLabel";
            this.yLabel.Size = new System.Drawing.Size(49, 22);
            this.yLabel.TabIndex = 1;
            this.yLabel.Text = "X";
            // 
            // xLabel
            // 
            this.xLabel.AutoSize = true;
            this.xLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xLabel.Location = new System.Drawing.Point(57, 44);
            this.xLabel.Name = "xLabel";
            this.xLabel.Size = new System.Drawing.Size(49, 22);
            this.xLabel.TabIndex = 2;
            this.xLabel.Text = "Y";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.groupBox1, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.fixedVars, 0, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 350F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(322, 521);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tableLayoutPanel3);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(316, 165);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Free variables";
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 152F));
            this.tableLayoutPanel3.Controls.Add(this.firstFreeVarComboBox, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.secondFreeVarComboBox, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.button1, 0, 1);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 2;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(310, 146);
            this.tableLayoutPanel3.TabIndex = 0;
            // 
            // firstFreeVarComboBox
            // 
            this.firstFreeVarComboBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.firstFreeVarComboBox.FormattingEnabled = true;
            this.firstFreeVarComboBox.Location = new System.Drawing.Point(3, 26);
            this.firstFreeVarComboBox.Name = "firstFreeVarComboBox";
            this.firstFreeVarComboBox.Size = new System.Drawing.Size(94, 21);
            this.firstFreeVarComboBox.TabIndex = 0;
            // 
            // secondFreeVarComboBox
            // 
            this.secondFreeVarComboBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.secondFreeVarComboBox.FormattingEnabled = true;
            this.secondFreeVarComboBox.Location = new System.Drawing.Point(161, 26);
            this.secondFreeVarComboBox.Name = "secondFreeVarComboBox";
            this.secondFreeVarComboBox.Size = new System.Drawing.Size(103, 21);
            this.secondFreeVarComboBox.TabIndex = 1;
            // 
            // button1
            // 
            this.tableLayoutPanel3.SetColumnSpan(this.button1, 2);
            this.button1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button1.Location = new System.Drawing.Point(3, 76);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(304, 67);
            this.button1.TabIndex = 2;
            this.button1.Text = "Confirm";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.FreeVarsButtonClick);
            // 
            // fixedVars
            // 
            this.fixedVars.Controls.Add(this.fixedVarsTable);
            this.fixedVars.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fixedVars.Location = new System.Drawing.Point(3, 174);
            this.fixedVars.Name = "fixedVars";
            this.fixedVars.Size = new System.Drawing.Size(316, 344);
            this.fixedVars.TabIndex = 1;
            this.fixedVars.TabStop = false;
            this.fixedVars.Text = "Slice settings";
            // 
            // fixedVarsTable
            // 
            this.fixedVarsTable.ColumnCount = 1;
            this.fixedVarsTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.fixedVarsTable.Dock = System.Windows.Forms.DockStyle.Top;
            this.fixedVarsTable.Location = new System.Drawing.Point(3, 16);
            this.fixedVarsTable.Name = "fixedVarsTable";
            this.fixedVarsTable.RowCount = 1;
            this.fixedVarsTable.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.fixedVarsTable.Size = new System.Drawing.Size(310, 325);
            this.fixedVarsTable.TabIndex = 0;
            // 
            // createSliceButton
            // 
            this.createSliceButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.createSliceButton.Location = new System.Drawing.Point(3, 530);
            this.createSliceButton.Name = "createSliceButton";
            this.createSliceButton.Size = new System.Drawing.Size(322, 35);
            this.createSliceButton.TabIndex = 2;
            this.createSliceButton.Text = "Confirm";
            this.createSliceButton.UseVisualStyleBackColor = true;
            this.createSliceButton.Click += new System.EventHandler(this.createSliceButton_Click);
            // 
            // button2
            // 
            this.button2.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.button2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button2.Location = new System.Drawing.Point(3, 571);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(322, 28);
            this.button2.TabIndex = 3;
            this.button2.Text = "Set as main slice";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // customProblem3dPlot1
            // 
            this.customProblem3dPlot1.BackColor = System.Drawing.Color.Black;
            this.customProblem3dPlot1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.customProblem3dPlot1.Location = new System.Drawing.Point(0, 0);
            this.customProblem3dPlot1.Margin = new System.Windows.Forms.Padding(5);
            this.customProblem3dPlot1.Name = "customProblem3dPlot1";
            this.customProblem3dPlot1.NeedRedraw = false;
            this.customProblem3dPlot1.ProblemaMode = false;
            this.customProblem3dPlot1.Size = new System.Drawing.Size(497, 596);
            this.customProblem3dPlot1.TabIndex = 2;
            this.customProblem3dPlot1.VSync = false;
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // SliceEditorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(831, 602);
            this.Controls.Add(this._mainGrid);
            this.Name = "SliceEditorForm";
            this.Text = "SlicesWindow";
            this._mainGrid.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.fixedVars.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel _mainGrid;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.ComboBox firstFreeVarComboBox;
        private System.Windows.Forms.ComboBox secondFreeVarComboBox;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox fixedVars;
        private System.Windows.Forms.TableLayoutPanel fixedVarsTable;
        private System.Windows.Forms.Button createSliceButton;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label yLabel;
        private System.Windows.Forms.Label xLabel;
        private Views.User_controls.CustomProblem3dPlot customProblem3dPlot1;
        private System.Windows.Forms.Timer timer1;
    }
}