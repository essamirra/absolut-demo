﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.Windows.Input;
using Algorithms;
using Algorithms.Methods;
using Functions;
using Functions.Functions;
using Graphics;
using Properties;

namespace ViewDemo
{
    public partial class SliceEditorForm : Form
    {
        private readonly Dictionary<string, int> variablesNames = new Dictionary<string, int>();
        private int _freeVar1;
        private int _freeVar2;
        private IFunction _functionToShow;
        private IFunction _functionToSlice;
        private Dictionary<int, double> _fixedVars = new Dictionary<int, double>();
        private bool isFreeVariablesInputedCorrectly;
        public Dictionary<int, IFunction> SlicePlanes;
        private IProblem _problemToSlice;
        private IProblem _problemToShow;
        private double _lastUpdateTime;
        private List<DrawablesControlThreading> _d;

        public SliceEditorForm()
        {
            InitializeComponent();
            FunctionToSlice = new Squares();
            _d = new List<DrawablesControlThreading>() {customProblem3dPlot1};
            timer1.Start();
        }

        public bool Mode2d
        {
            get => customProblem3dPlot1.Mode2d;
            set
            {
                customProblem3dPlot1.Mode2d = value;
                customProblem3dPlot1.NeedRedraw = true;
            }
        }

        public SliceEditorForm(IFunction functionToSlice)
        {
            InitializeComponent();
            FunctionToSlice = functionToSlice;
            _d = new List<DrawablesControlThreading>() { customProblem3dPlot1 };
            timer1.Start();
        }

        public SliceEditorForm(IProblem problemToSlice)
        {
            InitializeComponent();
            ProblemToSlice = problemToSlice;
            _d = new List<DrawablesControlThreading>() { customProblem3dPlot1 };
            timer1.Start();
        }

        public IFunction FunctionToSlice
        {
            get { return _functionToSlice; }
            set
            {
                _functionToSlice = value;
                if (value.Left.Count == 2)
                {
                    HideVarsControls();
                    customProblem3dPlot1.ProblemaMode = false;
                    customProblem3dPlot1.SetFunction(_functionToSlice);
                    Refresh();

                }
                   
                else
                {
                    UpdateFreeVarsControls();
                }

                
                
            }
        }

        private void KeyboardCheckerOnTick(object sender, EventArgs eventArgs)
        {
            var currentTime = Environment.TickCount / 1000.0;
            var delta = currentTime - _lastUpdateTime;
            _lastUpdateTime = currentTime;
            const double RotationSpeed = Math.PI / 2;
            
            if (Keyboard.IsKeyDown(Key.W))
                foreach (var control in _d)
                {
                    control.Camera.RotateAroundX(RotationSpeed * delta);
                    control.NeedRedraw = true;
                }
            if (Keyboard.IsKeyDown(Key.A))
                foreach (var control in _d)
                {
                    control.Camera.RotateAroundY(-RotationSpeed * delta);
                    control.NeedRedraw = true;
                }
            if (Keyboard.IsKeyDown(Key.S))
                foreach (var control in _d)
                {
                    control.Camera.RotateAroundX(-RotationSpeed * delta);
                    control.NeedRedraw = true;
                }
            if (Keyboard.IsKeyDown(Key.D))
                foreach (var control in _d)
                {
                    control.Camera.RotateAroundY(RotationSpeed * delta);
                    control.NeedRedraw = true;
                }
        }

        public IProblem ProblemToSlice
        {
            get { return _problemToSlice; }
            set
            {
                _problemToSlice = value;
                customProblem3dPlot1.ProblemaMode = true;
                if (value.UpperBound.Count == 2)
                {
                    HideVarsControls();
                    
                    customProblem3dPlot1.SetProblem(_problemToSlice);
                    Refresh();

                }

                else
                {
                    UpdateFreeVarsControls();
                }



            }
        }

        private void HideVarsControls()
        {
            _mainGrid.ColumnStyles[0].Width = 0;
            Refresh();
        }

        private void UpdateFreeVarsControls()
        {
            variablesNames.Clear();
            for (var i = 0; i < _functionToSlice.Left.Count; i++)
                variablesNames.Add("x" + i, i);

            firstFreeVarComboBox.DataSource = variablesNames.Keys.ToList();
            secondFreeVarComboBox.DataSource = variablesNames.Keys.ToList();
        }

        private void FreeVarsButtonClick(object sender, EventArgs e)
        {
            var selectedFreeVar1 = variablesNames[firstFreeVarComboBox.Text];
            var selectedFreeVar2 = variablesNames[secondFreeVarComboBox.Text];
            xLabel.Text = firstFreeVarComboBox.Text;
            yLabel.Text = secondFreeVarComboBox.Text;
            if (selectedFreeVar2 != selectedFreeVar1)
            {
                if (selectedFreeVar1 < selectedFreeVar2)
                {
                    _freeVar1 = selectedFreeVar1;
                    _freeVar2 = selectedFreeVar2;
                }
                else
                {
                    _freeVar1 = selectedFreeVar2;
                    _freeVar2 = selectedFreeVar1;
                }
                isFreeVariablesInputedCorrectly = true;
                var button = sender as Button;
                button.BackColor = Color.Green;
                fixedVarsTable.Controls.Clear();
                UpdateFixedVarsControls();
                createSliceButton_Click(sender,e);
            }
            else
            {
                isFreeVariablesInputedCorrectly = false;
                var button = sender as Button;
                button.BackColor = Color.Red;
                fixedVarsTable.Controls.Clear();
                SlicePlanes = null;
            }
        }

        private void UpdateFixedVarsControls()
        {
            _fixedVars.Clear();
            for (var i = 0; i < _functionToSlice.Left.Count; i++)
            {
                if (i == _freeVar1 || i == _freeVar2)
                    continue;
                var propertyInfo = new DoubleProperty("x "+ i, FunctionToSlice.Left[i], FunctionToSlice.Right[i], 10, DoubleDeltaType.Step, (FunctionToSlice.Right[i] + FunctionToSlice.Left[i])/2);
                var control = new DoubleInput(propertyInfo, (FunctionToSlice.Right[i] + FunctionToSlice.Left[i]) / 2) {Dock = DockStyle.Fill};
                _fixedVars.Add(i, (FunctionToSlice.Right[i] + FunctionToSlice.Left[i]) / 2);
                control.OnValueChanged += (sender, args) =>
                {
                    var contr = sender as DoubleInput;
                    _fixedVars[Convert.ToInt32(contr.PropertyName.Substring(1))] = contr.GetValue();
                   // _fixedVars.Add(Convert.ToInt32(contr.PropertyName.Substring(1)), contr.GetValue());
                    createSliceButton_Click(sender,args);
                };
                fixedVarsTable.Controls.Add(control);
            }
            Refresh();
        }

        private void createSliceButton_Click(object sender, EventArgs e)
        {
            if (customProblem3dPlot1.ProblemaMode)
            {
                _problemToShow = new FixedByValuesProblem(ProblemToSlice, _fixedVars);
            }
            _functionToShow = new FixedByValuesFunction(FunctionToSlice, _fixedVars);
            //functionViewControl1.SetFunction(_functionToShow);
            Refresh();
        }

        private void tableLayoutPanel1_CellPaint(object sender, TableLayoutCellPaintEventArgs e)
        {
            if (e.Column == 0)
            {
                switch (e.Row)
                {
                    case 0:
                        e.Graphics.FillRectangle(Brushes.Blue, e.CellBounds);
                        break;
                    case 1:
                        e.Graphics.FillRectangle(Brushes.Green, e.CellBounds);
                        break;
                    case 2:
                        e.Graphics.FillRectangle(Brushes.Red, e.CellBounds);
                        break;

                }
            }
                
        }

        internal void SetBounds(int dims, double[] min, double[] max, double[] center)
        {
            throw new NotImplementedException();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            KeyboardCheckerOnTick(sender,e);
        }

        public void DeleteLastPoint(MethodPoint methodPoint)
        {
            customProblem3dPlot1.DeleteLastPoint(methodPoint);
            Refresh();
        }

        public void AddPoint(MethodPoint methodPoint)
        {
            throw new NotImplementedException();
        }
    }

    
}