﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Absolut_Model;
using Algorithms;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using ViewDemo.Views;

namespace ViewDemo.Series
{
    public partial class ExperimentsDemo : Form, ISeriesExperimentView
    {
        private Guid _id;
        private SeriesExperimentPresenter _presenter;
        private int _last;
        private LineSeries _lineSeries;
        private int i = 1;
        private bool _isAutostart = false;
        private List<Guid> _algs = new List<Guid>();
        private LineSeries _timeSeries;

        private void DemoGraph()
        {
            var myModel = new PlotModel {Title = "Iterations"};

            _lineSeries = new LineSeries();
            myModel.Series.Add(_lineSeries);
            plot1.Model = myModel;
            myModel.Axes.Add(new LinearAxis {Position = AxisPosition.Bottom, Minimum = 0, Maximum = 15});
            myModel.Axes.Add(new LinearAxis {Position = AxisPosition.Left, Minimum = 0, Maximum = 1000});
            plotView2.Model = new PlotModel()
            {
                Title = "Operational Characteristic"
            };

            plotView1.Model = new PlotModel()
            {
                Title = "Time"
            };
            _timeSeries = new LineSeries();
            plotView1.Model.Series.Add(_timeSeries);
        }

        public ExperimentsDemo(Guid id, int count)
        {
            _id = id;
            _presenter = new SeriesExperimentPresenter(this, id);
            InitializeComponent();
            DemoGraph();
        }

        private void plot1_Click(object sender, EventArgs e)
        {
        }

        public void DisplaySeriesStarted()
        {
            toolStripStatusLabel1.Text = "Series Started";
            //startToolStripMenuItem.Enabled = false;
            methodParametersToolStripMenuItem.Enabled = false;
            toolStripProgressBar1.Visible = true;
            toolStripProgressBar1.Value = 0;
            Refresh();
        }

        public void DisplaySubExperimentStarted(ISearchAlg alg, Guid id)
        {
            toolStripStatusLabel1.Text = "SubExperiment started " + alg.Name;
            _last = dataGridView1.Rows.Add(1,alg.Name,1 , "");
            _algs.Add(id);
            toolStripProgressBar1.Value++;
        }

        public void DisplaySeriesFinished(SeriesExperimentResult res)
        {
            toolStripStatusLabel1.Text = "Series finished";
           // nextToolStripMenuItem.Enabled = false;
            methodParametersToolStripMenuItem.Enabled = true;
            _isAutostart = false;

            var lineSeries = new LineSeries();

            foreach (var point in res.OperationalChars)
            {
                lineSeries.Points.Add(new DataPoint(point.X, point.Y));
            }
            plotView2.Model = new PlotModel()
            {
                Title = "Operational Characteristic"
            };

            plotView2.Model.Series.Add(lineSeries);
            plotView2.Model.Axes.Add(new LinearAxis {Position = AxisPosition.Left, Minimum = 0, Maximum = 100});
            toolStripProgressBar1.Visible = false;
            plotView2.Refresh();

        }

        public void DisplaySubExperimentFinished(ExperimentResult subId)
        {
            toolStripStatusLabel1.Text = "SubExperiment finished ";
            dataGridView1[0, _last].Value = _last+1;
            dataGridView1[2, _last].Value = subId.Iterations;
            dataGridView1[3, _last].Value = subId.WasProblemSolved;
            _lineSeries.Points.Add(new DataPoint(i, subId.Iterations));
            _timeSeries.Points.Add(new DataPoint(i, subId.Time));
            i++;
            plot1.Refresh();
            plotView1.Refresh();
            if (_isAutostart)
                _presenter.StartNext();
        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {
        }

        private void autoStartToolStripMenuItem_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();
            Refresh();
            _presenter.StartSeries();
            _isAutostart = true;
            _presenter.StartNext();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 1)
                _presenter.ShowDetailsOfSubExperiment(_algs[e.RowIndex]);
        }

        private void ExperimentsDemo_FormClosed(object sender, FormClosedEventArgs e)
        {
            _presenter.DeleteSeries();
        }

        private void methodParametersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var existingParams = _presenter.GetParamsInfo().ToDictionary(i => i, i => _presenter.GetProperty(i.name));
            using (var paramsChoose = new ParametersDialog(existingParams))
            {
                var dr = paramsChoose.ShowDialog();
                if (dr == DialogResult.OK)
                    _presenter.SetProperties(paramsChoose.changedValues);
            }
        }
    }
}