﻿using System.Collections.Generic;
using Algorithms.Methods;
using Functions;
using Properties;

namespace Algorithms
{
    public interface ISearchAlg
    {
        string Name { get; }
        int IterationsPerformed { get; }
        IProblem Problem { get; set; }
        void NextStep();
        void PrevStep();
        bool HasNext();
        int CurrentIter();
        MethodPoint LastPoint();
        MethodPoint ArgminTask();

        void RecalcPoints();
        IEnumerable<MethodPoint> CalculatedPoints(int start = 0);
        ISearchAlg Clone();
        void SetProperty(string name, object value);
        void SetProperties(Dictionary<string, object> propsValues);
        object GetProperty(string name);
        List<PropertyInfo> GetPropertiesInfo();
        int IterOverMin();
        bool IsIterOverMin();
        void SetPropertyWithoutRecalc(string name, object value);
        void SetPropertiesWithoutRecalc(Dictionary<string, object> propsValues);
    }
}