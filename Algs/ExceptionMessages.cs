﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Algorithms
{
    internal static class ExceptionMessages
    {
        public const string ProblemIsNull = "Problem is not set";
        public const string ProblemWithoutCriterions = "Problem does not have any criterions";
    }
}
