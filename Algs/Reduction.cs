﻿/*using System; 
using System.Linq;
using Functions;


using System.Collections.Generic;
using Properties;

namespace Algorithms
{
    public class Reduction : GenericAlg
    {
        protected IFunction f;

        public Reduction(string alg_name)
        {
            RegisterProperty(new DoubleProperty("Precision", 0.000001, 0.1, 6, DoubleDeltaType.Log, 0.001));
            RegisterProperty(new FuncProperty("Function", null));
            RegisterProperty(new IntProperty("Dimension", 2, 2, 0, 2));
            RegisterProperty(new StringProperty("Alg_Name", ""));//!!!!!!!
        }

        public override void SetProperty(string name, object value)
        {
            if (name == "Function")
            {
                f = (IFunction)value;
            }
            base.SetProperty(name, value);
        }

        public override void RecalcPoints()
        {
            if (f == null) return;
            Restart();
            ExperimentPoints.Clear();
            ExperimentPointsReal.Clear();
            Init();

        }


        private double reduction(List<double> fixed_x)
        {
            double y = 0.0;
            List<double> fixed_x_down = new List<double>(fixed_x);

            if (fixed_x.count < (int)GetProperty("Dimension") - 1)
            {
                ISearchAlg alg_level = new ISearchAlg();
                //установка свойств, выбор алгоритма alg_name
                alg_level.InitReduction();//инициализация границ
                while (!isEnd())
                {
                    alg_level.Step();

                    fixed_x_down.Add(new_x);
                    double y = reduction(fixed_x_down);
                    alg_level.AddPoint(fixed_x_down, y);
                    fixed_x_down.RemoveAt(fixed_x_down.Count);

                }
                return alg_level.ArgminTask().y;
            }

            else
            {
                ISearchAlg alg_level = new ISearchAlg();
                //установка свойств, выбор алгоритма  по alg_name
                alg_level.InitReduction();//инициализация границ
                while (!isEnd())
                {
                    alg_level.Step();

                    fixed_x_down.Add(new_x);
                    double y = f.Calc(fixed_x_down);
                    alg_level.AddPoint(fixed_x_down, y);
                    fixed_x_down.RemoveAt(fixed_x_down.Count);
                }
                return alg_level.ArgminTask().y;
            }

        }


        public override bool IsEnded()
        {
            
            return false;
        }

        private void Init()
        {
            ExperimentPoints.Add(new MethodPoint()
            {
                IdFun = 1,
                iteration = 0,
                isEnd = AlgEnd.no,
                x = f.Left,
                y = f.Calc(f.Left),
                evolventX = 0
            });
            ExperimentPoints.Add(new MethodPoint()
            {
                IdFun = 1,
                iteration = 1,
                isEnd = AlgEnd.no,
                x = f.Right,
                y = f.Calc(f.Right),
                evolventX = 1
            });
            ExperimentPointsReal.Add(new MethodPoint()
            {
                IdFun = 1,
                iteration = 0,
                isEnd = AlgEnd.no,
                x = f.Left,
                y = f.Calc(f.Left),
                evolventX = 0
            });
            ExperimentPointsReal.Add(new MethodPoint()
            {
                IdFun = 1,
                iteration = 1,
                isEnd = AlgEnd.no,
                x = f.Right,
                y = f.Calc(f.Right),
                evolventX = 1
            });
            realIteration = 1;
        }

    }
}*/