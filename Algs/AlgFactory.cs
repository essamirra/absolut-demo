﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using Algorithms.Methods;
using com.sun.org.apache.bcel.@internal.generic;
using Functions;

namespace Algorithms
{
    public struct MethodsNames
    {
        public const string Agp = "Agp";
        public const string Scanning = "Scanning alg";
        public const string AgpParal = "Pseudoparallel AGP ";
        public const string ScanningParal = "Pseudoparallel Scanning algorithm";
        public const string GeneticSimple = "GeneticSimpleAlg";
        public const string AgpReduction = "Reduction_AGP";
        public const string Piyavsky = "Piyavskiy";
        public const string DiffEvolution = "DE";
        public const string Bleic = "Bleic";


    }
    public static class AlgFactory
    {
        public static List<string> Options = new List<string>
        {"Agp", "Scanning alg", "AGP_paral", "Scanning_alg_paral", "GeneticSimpleAlg", "Piyavskiy", 
        "Reduction_AGP", "DE", MethodsNames.Bleic};

       public static List<string> ConstrainedOptions = new List<string> { "Alglib AUL"};
        public static ISearchAlg Build(string algName)
        {
            switch (algName)
            {
                case "Agp":
                    return new Agp(){Name = algName};
                case "Scanning alg":
                    return new ScanningAlg() { Name = algName };
                case "AGP_paral":
                    return new AGP_2() { Name = algName };
                case "Scanning_alg_paral":
                    return new ScanningAlgParal() { Name = algName };
                case "GeneticSimpleAlg":
                    return new GeneticSimpleAlg() { Name = algName };
                case "Piyavskiy":
                    return new Piyavskiy() { Name = algName };
                case MethodsNames.Bleic:
                    return new AlglibBleic() { Name = algName };
                case "DE":
                    return new DE();
            }

            throw new ArgumentException(algName);
        }

        public static ISearchAlg BuildConstrainedAlg(string algName)
        {
            switch (algName)
            {
                case "Alglib AUL":
                    return new AlglibAul();
              
            }
            throw new ArgumentException(algName);
        }

        public static ISearchAlg BuildExaminAlg(string dll, string conf, string examin)
        {
            ISearchAlg a = new ExaminAlg();
            a.SetProperty("Examin_conf", conf);
            a.SetProperty("Examin_dll", dll);
            a.SetProperty("Examin_exe", examin);
            return a;
        }

        public static ISearchAlg BuildDiRectAlg(string dll, string conf, string examin)// string outfile)
        {
            ISearchAlg a = new DiRect();
            a.SetProperty("DiRect_conf", conf);
            a.SetProperty("DiRect_dll", dll);
            a.SetProperty("DiRect_exe", examin);
            a.SetProperty("outDiRect", "outDirect.txt");
            return a;
        }

        public static void SaveToXML(ISearchAlg a, string path)
        {
            XmlWriter writer = null;
            XmlWriterSettings settings = new XmlWriterSettings
            {
                Indent = true,
                IndentChars = ("\t"),
                OmitXmlDeclaration = true
            };

            // Create the XmlWriter object and write some content.
            writer = XmlWriter.Create(path, settings);
            const string startElement = "Method";
            writer.WriteStartElement(startElement);
            writer.WriteAttributeString("name", a.Name);
            writer.WriteStartElement("Problem");
            writer.WriteAttributeString("name", a.Problem.Name);
            if(a.Problem.ConfigPath != null)
                writer.WriteAttributeString("config", a.Problem.ConfigPath);
            if(a.Problem.DllPath != null)
                writer.WriteAttributeString("dll", a.Problem.DllPath);
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.Flush();
            writer.Close();
        }

        public static ISearchAlg OpenFromXML(string path)
        {
            XmlReader reader = XmlReader.Create(new FileStream(path, FileMode.Open));

            reader.ReadToFollowing("Method");
            reader.MoveToFirstAttribute();
            ISearchAlg a = Build(reader.Value);
            reader.ReadToFollowing("Problem");
            reader.MoveToFirstAttribute();
            if (reader.Value == "DLL")
            {
                reader.MoveToAttribute("dll");
                var dllPath = reader.Value;
                var hasConfig = reader.MoveToAttribute("config");
                var config = hasConfig ? reader.Value : null;
                a.Problem = ProblemFactory.BuildProblemFromDll(dllPath, config);

            }
            else
            a.Problem = ProblemFactory.BuildNotConstrainedProblem(reader.Value);
            return a;
        }
    }
}