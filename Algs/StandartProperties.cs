﻿namespace Algorithms
{
    public struct StandartProperties
    {
        public const string MaxIter = "Maximum Iteration";
        public const string MainFunction = "Function";
        public const string Precision = "Precision";
        public const string ProccessCount = "Process";


    }
}