﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Algorithms.Methods
{
    public enum AlgEnd
    {
        no,
        presicion,
        iter
    }

    public class MethodPoint
    {
        public int IdFun;
        public AlgEnd isEnd;
        public List<double> x;
        public double evolventX;
        public double? y;
        public int iteration;
        public int id_process;
        public List<double> criterions;
        public List<double> constraints;
        public MethodPoint()
        {
            x = new List<double>();
            id_process = 0;
        }

        public override string ToString()
        {
            var stringX = x.Select(f => f.ToString("N2"));
            return "( " + String.Join(", ", stringX) + ", " + y + ")";
        }

        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.

            // If parameter cannot be cast to Point return false.
            MethodPoint p = obj as MethodPoint;
            if ((System.Object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (x == p.x) && (Math.Abs((double) (y - p.y)) < 0.01);
        }

        public bool Equals(MethodPoint p)
        {
            // If parameter is null return false:
            if ((object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (x == p.x) && (Math.Abs((double) (y - p.y)) < 0.01);
        }


    }
}