﻿using System;
using System.Collections.Generic;
using Functions;
using Properties;

namespace Algorithms.Methods
{
    public class CharacteristicAlg : GenericAlg
    {
        protected int MaxCharIndex = 1;
        private PeanoHelper _peano;

        public override IProblem Problem
        {
            get { return base.Problem; }
            set
            {
                base.Problem = value;
                _peano = new PeanoHelper(Problem.LowerBound.GetRange(0,value.Functionals[0].Dimension), Problem.UpperBound.GetRange(0, value.Functionals[0].Dimension));
            }
        }

        protected readonly List<Characteristic> CharacteristicList;

        protected CharacteristicAlg()
        {
            CharacteristicList = new List<Methods.Characteristic>();
            RegisterProperty(new DoubleProperty("Precision", 0.000001, 0.1, 6, DoubleDeltaType.Log, 0.001));
        }

        protected virtual double CalcCharacteristic(int curIntervalIndex)
        {
            return 0;
        }

        protected virtual double CalcNewPoint()
        {
            return 0;
        }

        public override void RecalcPoints()
        {
            if (Problem == null) return;
            MaxCharIndex = 1;
            Restart();
            ExperimentPoints.Clear();
            ExperimentPointsReal.Clear();
            Init();
            while (CurrentIteration <= (int) GetProperty("Maximum Iteration") && !IsEnded())
            {
                ExperimentPoints.Sort((p1, p2) => p1.evolventX.CompareTo(p2.evolventX));
                MaxCharIndex = 1;
                double maxR = Double.MinValue;
                CharacteristicList.Clear();
                for (int i = 0; i < CurrentIteration; i++)
                {
                    var R = CalcCharacteristic(i);
                    if (R > maxR)
                    {
                        MaxCharIndex = i + 1;
                        maxR = R;
                    }
                }
                if (Problem.OptimalPoint != null && IsEndedOverMin()) IterMin = true;
                if (IsEnded()) return;
                double newPoint = CalcNewPoint();
                if (!IsIterOverMin())
                {
                    IterationOverMin++;
                }

                var newPointImage = _peano.GetImage(newPoint);
                var methodPoint = new MethodPoint()
                {
                    evolventX = newPoint,
                    IdFun = 0,
                    iteration = CurrentIteration + 1,
                    x = newPointImage,
                    y = Problem.CalculateFunctionals(newPointImage, 0)
                };
                ExperimentPointsReal.Add(methodPoint);
                ExperimentPoints.Add(methodPoint);
                CurrentIteration++;
            }

        }

        public override bool IsEnded()
        {
            var b = Math.Abs(ExperimentPoints[MaxCharIndex].evolventX - ExperimentPoints[MaxCharIndex - 1].evolventX)
                    < (double) GetProperty(StandartProperties.Precision);
            WasSolved = b;
            var isEnded = b || CurrentIteration >=(int) GetProperty(StandartProperties.MaxIter);

            return isEnded;
        }

        public bool IsEndedOverMin()
        {
            if ((!IterMin) &&
                (Math.Abs(ExperimentPoints[MaxCharIndex].evolventX - _peano.GetPrototype(Problem.OptimalPoint)) <
                 (double) GetProperty(StandartProperties.Precision)))
            {
                IterMin = true;
                return true;
            }
            return false;
        }

        private void Init()
        {
            var methodPointLeft = new MethodPoint()
            {
                IdFun = 1,
                iteration = 0,
                isEnd = AlgEnd.no,
                x = Problem.LowerBound,
                y = Problem.CalculateFunctionals(Problem.LowerBound, 0),
                evolventX = 0
            };
            ExperimentPoints.Add(methodPointLeft);
            var methodPointRight = new MethodPoint()
            {
                IdFun = 1,
                iteration = 1,
                isEnd = AlgEnd.no,
                x = Problem.UpperBound,
                y = Problem.CalculateFunctionals(Problem.UpperBound, 0),
                evolventX = 1
            };
            ExperimentPoints.Add(methodPointRight);
            ExperimentPointsReal.Add(methodPointLeft);
            ExperimentPointsReal.Add(methodPointRight);
            CurrentIteration = 1;
        }
    }
}