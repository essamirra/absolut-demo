﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using Properties;

namespace Algorithms.Methods
{
    [Serializable]
    public class GenericAlg : ISearchAlg, IPropertyUser
    {
        [NonSerialized]
        protected List<MethodPoint> ExperimentPoints;
        [NonSerialized]
        protected List<MethodPoint> ExperimentPointsReal;

        protected int IteratorPosition;
        protected int IterationOverMin;
        protected bool IterMin;
        private readonly PropertyProvider _p = new PropertyProvider();
        protected int CurrentIteration = 0;
        private IProblem _problem = null;


        public GenericAlg()
        {
            ExperimentPointsReal = new List<MethodPoint>();
            ExperimentPoints = new List<MethodPoint>();
            RegisterProperty(new IntProperty(StandartProperties.MaxIter, 0, 10000, 10, 300));
        }

        private int MaxIterations => (int) _p.GetProperty(StandartProperties.MaxIter);


        public void RegisterProperty(PropertyInfo info)
        {
            _p.RegisterProperty(info);
        }

        public string Name { get; set; }

        public void Restart()
        {
            IterMin = false;
            IterationOverMin = 0;
            IteratorPosition = 0;
        }

        public void NextStep()
        {
            if (IteratorPosition + 1 <= CurrentIteration)
                IteratorPosition++;
        }

        public void PrevStep()
        {
            if (IteratorPosition > 0 && IteratorPosition <= CurrentIteration)
                IteratorPosition--;
        }

        public bool HasNext()
        {
            return IteratorPosition + 1 <= CurrentIteration;
        }

        public int CurrentIter()
        {
            return IteratorPosition;
        }

        public int IterOverMin()
        {
            return IterMin ? IterationOverMin : CurrentIteration;
        }

        public bool IsIterOverMin()
        {
            return IterMin;
        }

        public MethodPoint Argmin()
        {
            if (ExperimentPoints.Count == 0) throw new AccessViolationException("Experiment points is empty");
            var min = ExperimentPoints.Min(point => point.y);
            return ExperimentPoints.Find(point => Math.Abs((double) (point.y - min)) < double.Epsilon);
        }

        public MethodPoint ArgminTask()
        {
            if (IteratorPosition == 0) return null;
            if (ExperimentPointsReal.Count == 0) return null;
            var iter = 1;
            var y = double.MaxValue;
            for (var i = 0; i < MaxIterations && i < IteratorPosition; i++)
                if (ExperimentPointsReal[i].y < y)
                {
                    y = (double) ExperimentPointsReal[i].y;
                    iter = i;
                }
            return ExperimentPointsReal[iter];
        }

        public virtual bool IsEnded()
        {
            return true;
        }

        public virtual void RecalcPoints()
        {
        }

        public IEnumerable<MethodPoint> CalculatedPoints(int count = 0)
        {
            return ExperimentPointsReal.AsReadOnly();
        }

        public ISearchAlg Clone()
        {
            return (ISearchAlg) MemberwiseClone();
        }

        public MethodPoint LastPoint()
        {
            return ExperimentPointsReal[IteratorPosition];
        }

        public virtual void SetProperty(string name, object value)
        {
            SetPropertyWithoutRecalc(name, value);
            RecalcPoints();
        }

        public void SetProperties(Dictionary<string, object> propsValues)
        {
            foreach (var k in propsValues)
                SetPropertyWithoutRecalc(k.Key, k.Value);
            RecalcPoints();
        }

        public void SetPropertiesWithoutRecalc(Dictionary<string, object> propsValues)
        {
            foreach (var k in propsValues)
                SetPropertyWithoutRecalc(k.Key, k.Value);
            
        }

        public object GetProperty(string name)
        {
            return _p.GetProperty(name);
        }


        public List<PropertyInfo> GetPropertiesInfo()
        {
            return _p.GetPropertiesInfo();
        }

        public int PointsCount { get; }

        public bool WasSolved { get; set; }


        public virtual IProblem Problem
        {
            get { return _problem; }
            set
            {
                _problem = value;


            }
        }

        public int IterationsPerformed => CurrentIteration;

        public void SetPropertyWithoutRecalc(string name, object value)
        {
            _p.SetProperty(name, value);
        }

        public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            
        }

        
    }
}