﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using Functions;
using Properties;

namespace Algorithms.Methods
{
    class CharacteristicAlgParal : GenericAlg
    {
        protected int maxCharIndex;
        protected readonly List<Characteristic> CharacteristicList;

        protected CharacteristicAlgParal()
        {
            CharacteristicList = new List<Characteristic>();
            RegisterProperty(new DoubleProperty(StandartProperties.Precision, 0.000001, 0.1, 5, DoubleDeltaType.Log, 0.00001));
            RegisterProperty(new IntProperty(StandartProperties.ProccessCount, 1, 4, 1, 1));
        }
        protected virtual double CalcCharacteristic(int curIntervalIndex)
        {
            return 0;
        }

        protected virtual double CalcNewPoint()
        {
            return 0;
        }
        public override void RecalcPoints()
        {
            if (Problem == null) throw new InvalidOperationException(ExceptionMessages.ProblemIsNull);
            var f = Problem.Criterions.FirstOrDefault();
            if (f == null) throw new InvalidOperationException(ExceptionMessages.ProblemWithoutCriterions);
            Restart();
            ExperimentPoints.Clear();
            Init();

            while (CurrentIteration <= (int)GetProperty(StandartProperties.MaxIter))
            {
                ExperimentPoints.Sort((p1, p2) => p1.evolventX.CompareTo(p2.evolventX));
                maxCharIndex = 0;
                
                CharacteristicList.Clear();
                for (int i = 0; i < CurrentIteration; i++)
                {
                    var R = CalcCharacteristic(i);

                }
                CharacteristicList.Sort((p1, p2) => p2.value_characteristic.CompareTo(p1.value_characteristic));

                int index = 0;
                if ((int)GetProperty(StandartProperties.ProccessCount) > CharacteristicList.Count) index = CharacteristicList.Count;
                else index = (int)GetProperty(StandartProperties.ProccessCount);


                for (int i = 0; i < index; i++){


                    double newPoint = CalcNewPoint();
                    if (IsEnded()) return;
                    maxCharIndex = i + 1;
                    ExperimentPointsReal.Add(new MethodPoint
                    {
                        evolventX = newPoint,
                        IdFun = 0,
                        iteration = CurrentIteration + 1,
                        y = f.Calc(newPoint),
                        x = f.GetImage(newPoint),
                        id_process = i
                    });

                    ExperimentPoints.Add(new MethodPoint
                    {
                        evolventX = newPoint,
                        IdFun = 0,
                        iteration = CurrentIteration + 1,
                        y = f.Calc(newPoint),
                        x = f.GetImage(newPoint),
                        id_process = i
                    });

                }
                CurrentIteration++;
            }
        }

        public override bool IsEnded()
        {
            return Math.Abs(CharacteristicList[maxCharIndex].right_x.evolventX - CharacteristicList[maxCharIndex].left_x.evolventX)
                   < (double)GetProperty(StandartProperties.Precision);
        }

        private void Init()
        {
            if (Problem == null) throw new InvalidOperationException(ExceptionMessages.ProblemIsNull);
            var f = Problem.Criterions.FirstOrDefault();
            if (f == null) throw new InvalidOperationException(ExceptionMessages.ProblemWithoutCriterions);
            ExperimentPoints.Add(new MethodPoint
            {
                IdFun = 1,
                iteration = 0,
                isEnd = AlgEnd.no,
                x = Problem.LowerBound,
                y = f.Calc(Problem.LowerBound),
                evolventX = 0
            });
            ExperimentPoints.Add(new MethodPoint
            {
                IdFun = 1,
                iteration = 1,
                isEnd = AlgEnd.no,
                x = Problem.UpperBound,
                y = f.Calc(Problem.UpperBound),
                evolventX = 1
            });
            ExperimentPointsReal.Add(new MethodPoint
            {
                IdFun = 1,
                iteration = 0,
                isEnd = AlgEnd.no,
                x = f.Left,
                y = f.Calc(f.Left),
                evolventX = 0
            });
            ExperimentPointsReal.Add(new MethodPoint
            {
                IdFun = 1,
                iteration = 1,
                isEnd = AlgEnd.no,
                x = f.Right,
                y = f.Calc(f.Right),
                evolventX = 1
            });
            CurrentIteration = 1;
        }
    }
}
