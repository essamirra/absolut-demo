﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using org.moeaframework;
using org.moeaframework.core;
using org.moeaframework.core.variable;
using org.moeaframework.problem;


namespace Algorithms.Methods
{
    public class NSGA2 : GenericAlg
    {
        public NSGA2()
        {
            ikvm.runtime.Startup.addBootClassPathAssemby(Assembly.Load("MOEAFramework-2.12"));
        }

        public static void test()
        {
            NondominatedPopulation result =
                new Executor().withProblem("UF1").withAlgorithm("NSGAII").withMaxEvaluations(10000).run();
            foreach (Solution res in result)
            {
                Console.Write(res.getObjective(0));
                Console.Write(res.getObjective(1));
            }
        }

        public override bool IsEnded()
        {
            return base.IsEnded();
        }

        public override void RecalcPoints()
        {
            if (Problem == null) throw new InvalidOperationException(ExceptionMessages.ProblemIsNull);
            MoeaProblem._problem = Problem;
            NondominatedPopulation result = new Executor().withProblemClass(typeof(MoeaProblem)).withAlgorithm("NSGAII")
                .withMaxEvaluations((int)GetProperty(StandartProperties.MaxIter))
                .run();
            ExperimentPoints = MoeaProblem._points;
            ExperimentPointsReal = MoeaProblem._points;
            CurrentIteration = MoeaProblem.Iterations;
        }


        public class MoeaProblem : AbstractProblem
        {
            public static IProblem _problem;
            public static List<MethodPoint> _points = new List<MethodPoint>();
            public static int Iterations = 0;

            public MoeaProblem()
                : base(_problem.LowerBound.Count, _problem.NumberOfCriterions, _problem.NumberOfCriterions)
            {
            }

            public override Solution newSolution()
            {
                Solution solution = new Solution(getNumberOfVariables(),
                    getNumberOfObjectives());

                for (int i = 0; i < getNumberOfVariables(); i++)
                {
                    solution.setVariable(i, new RealVariable(_problem.LowerBound[i], _problem.UpperBound[i]));
                }

                return solution;
            }

            public override void evaluate(Solution solution)
            {
                double[] x = EncodingUtils.getReal(solution);
                double[] f = _problem.CalculateCriterions(x.ToList()).ToArray();
                double[] constr = _problem.CalculateConstraints(x.ToList()).ToArray();
                _points.Add(new MethodPoint()
                {
                    evolventX = 0,
                    id_process = 0,
                    IdFun = 0,
                    isEnd = AlgEnd.no,
                    iteration = Iterations,
                    x = x.ToList(),
                    y = f[0],
                    constraints = _problem.CalculateConstraints(x.ToList()),
                    criterions = _problem.CalculateCriterions(x.ToList())
                });
                solution.setObjectives(f);
                solution.setConstraints(constr);
                Iterations++;
            }
        }
    }
}