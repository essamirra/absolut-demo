﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Properties;

namespace Algorithms.Methods
{
    class AGP_2 : CharacteristicAlgParal
    {
        private double m;
        public AGP_2()
        {
            Name = "AGP_2";
            RegisterProperty(new DoubleProperty("r", 1.000001, 100, 100, DoubleDeltaType.Step, 2));
        }

        protected override double CalcNewPoint()
        {
            //return 0.5 * (ExperimentPoints[maxCharIndex].evolventX +
            //                       ExperimentPoints[maxCharIndex - 1].evolventX) -
            //                      (ExperimentPoints[maxCharIndex].y - ExperimentPoints[maxCharIndex - 1].y) / (2 * m);
            return (double) (0.5 * (CharacteristicList[maxCharIndex].right_x.evolventX + CharacteristicList[maxCharIndex].left_x.evolventX) -
                             (CharacteristicList[maxCharIndex].right_x.y - CharacteristicList[maxCharIndex].left_x.y) / (2 * m));
        }

        protected override double CalcCharacteristic(int curIntervalIndex)
        {
            if (curIntervalIndex == 0)
            {
                var d = new double[CurrentIteration];

                for (int i = 0; i < CurrentIteration; i++)
                {
                    var right = ExperimentPoints[i + 1];
                    var left = ExperimentPoints[i];
                    d[i] = Math.Abs((double) ((right.y - left.y) / (right.evolventX - left.evolventX)));

                }

                var M = d.Max();
                m = M > 0 ? M * (double)GetProperty("r") : 1;
            }

            var point = ExperimentPoints[curIntervalIndex + 1];
            var methodPoint = ExperimentPoints[curIntervalIndex];
            CharacteristicList.Add(new Methods.Characteristic()
            {
                evolventX = point.evolventX,
                right_x = point,
                sort_x_interval = curIntervalIndex,
                left_x = methodPoint,
                value_characteristic = (double) (m * (point.evolventX - methodPoint.evolventX) +
                                                 ((point.y - methodPoint.y) * (point.y - methodPoint.y)) / (m * (point.evolventX - methodPoint.evolventX)) -
                                                 2 * (point.y + methodPoint.y))
            });
            return (double) (m * (point.evolventX - methodPoint.evolventX) +
                             ((point.y - methodPoint.y) * (point.y - methodPoint.y)) / (m * (point.evolventX - methodPoint.evolventX)) -
                             2 * (point.y + methodPoint.y));
        }
    }
}

