﻿using Functions;
using Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Algorithms.Methods;
using DifferentialEvolution_namespace;

// Inputs to the optimizer

//        int I_NP; // Number of population members
//        double F_weight; // DE-stepsize F_weight from interval [0, 2]
//        double F_CR; // Crossover probability constant from interval [0, 1].
//        int I_D; // Number of parameters of the objective function.
//        //*** note: these are not bound constraints!! ***//
//        double[] FVr_minbound; // Vector of lower bounds FVr_minbound(1) ... FVr_minbound(I_D) of initial population.
//        double[] FVr_maxbound; // Vector of upper bounds FVr_maxbound(1) ... FVr_maxbound(I_D) of initial population.
//        bool I_bnd_constr; // true: use bounds as bound constraints, false: no bound constraints
//        int I_itermax; // maximum number of iterations (generations)
//        double F_VTR; // "Value To Reach" (stop when ofunc < F_VTR)
//        int I_strategy;
//        //                1 --> DE/rand/1:
//        //                      the classical version of DE.
//        //                2 --> DE/local-to-best/1:
//        //                      a version which has been used by quite a number
//        //                      of scientists. Attempts a balance between robustness
//        //                      and fast convergence.
//        //                3 --> DE/best/1 with jitter:
//        //                      taylored for small population sizes and fast convergence.
//        //                      Dimensionality should not be too high.
//        //                4 --> DE/rand/1 with per-vector-dither:
//        //                      Classical DE with dither to become even more robust.
//        //                5 --> DE/rand/1 with per-generation-dither:
//        //                      Classical DE with dither to become even more robust.
//        //                      Choosing F_weight = 0.3 is a good start here.
//        //                6 --> DE/rand/1 either-or-algorithm:
//        //                      Alternates between differential mutation and three-point-
//        //                      recombination
//        int I_refresh; // intermediate output will be produced after "I_refresh" iterations. No intermediate output will be produced if I_refresh is < 1
//        // public bool I_plotting; // we will not use this option.
//        double[,] FM_pop;
//        double[] FVr_bestmem; // Solution

namespace Algorithms
{
    class DE : GenericAlg
    {
        private readonly int _populationSize;
        private readonly int _numberOfIterations;
        private readonly int _numberOfParameters;
        private readonly int _strategy;
        public static InputStructure SInfun1;

        public DE()
        {

            RegisterProperty(new IntProperty("NumberOfParameters", 2, 2, 1, 2));
            RegisterProperty(new IntProperty("Strategy", 1, 5, 1, 1));
            RegisterProperty(new IntProperty("populationSize", 2, 300, 10, 100));
            RegisterProperty(new IntProperty("NumberOfIterations", 1, 5000, 100, 20));
            _populationSize = (int)GetProperty("populationSize");
            _numberOfIterations = (int)GetProperty("NumberOfIterations");
            _numberOfParameters = (int)GetProperty("NumberOfParameters");
            _strategy = (int)GetProperty("Strategy");
        }


        public override void RecalcPoints()
        {
            CurrentIteration = 0;
            IteratorPosition = 0;

            if (Problem == null) throw new InvalidOperationException(ExceptionMessages.ProblemIsNull);
            Restart();
            ExperimentPoints.Clear();
            ExperimentPointsReal.Clear();

            // Configuring Differential evolution optimizer
            SInfun1 = new InputStructure
            {
                F_VTR = -100000,
                I_D = _numberOfParameters
            };
            // Lower bound on the objective function

            // number of parameters to optimize

            SInfun1.FVr_minbound = new double[SInfun1.I_D]; // lower limit
            SInfun1.FVr_maxbound = new double[SInfun1.I_D];

            var f = Problem.Criterions.FirstOrDefault();
            if (f == null) throw new InvalidOperationException(ExceptionMessages.ProblemWithoutCriterions);
            SInfun1.FVr_minbound[0] = Problem.LowerBound[0];
            SInfun1.FVr_minbound[1] = Problem.LowerBound[0];
            SInfun1.FVr_maxbound[0] = Problem.UpperBound[0];
            SInfun1.FVr_maxbound[1] = Problem.UpperBound[1];

            SInfun1.I_bnd_constr = true; // bound by lower and upper limits
            SInfun1.I_NP = _populationSize;
            SInfun1.I_itermax = _numberOfIterations;
            SInfun1.F_weight = 0.85;
            SInfun1.F_CR = 1;
            SInfun1.I_strategy = _strategy;
            SInfun1.I_refresh = 1;

            SInfun1.FVr_bestmem = new double[SInfun1.I_D];
            SInfun1.FM_pop = new double[SInfun1.I_NP, SInfun1.I_D];


            var deOptimizer = new DifferentialEvolution(ObjectiveFunction);

            deOptimizer.Optimizer(SInfun1);

            for (int i = 0; i < deOptimizer.point.Count; i++)
            {
                ExperimentPointsReal.Add(new MethodPoint()
                {
                    IdFun = 1,
                    id_process = 0,
                    iteration = i,
                    isEnd = AlgEnd.no,
                    x = new List<double>(deOptimizer.point[i]),
                    y = f.Calc(deOptimizer.point[i]),
                    evolventX = 0
                });
            }
            ExperimentPoints = ExperimentPointsReal;
            CurrentIteration = deOptimizer.point.Count - 1;
        }

        private OutputFunction ObjectiveFunction(double[] parameters, InputStructure S_In)
        {
            if (Problem == null) throw new InvalidOperationException(ExceptionMessages.ProblemIsNull);
            var f = Problem.Criterions.FirstOrDefault();
            if (f == null) throw new InvalidOperationException(ExceptionMessages.ProblemWithoutCriterions);
            var result = new OutputFunction
            {
                I_nc = 0,
                FVr_ca = new double[] {0},
                I_no = 1
            };
            // any constrains??
            // number of outputs

            double x = parameters[0];
            double y = parameters[1];
            List<double> point = new List<double>();
            point.Add(x);
            point.Add(y);
            double z = f.Calc(point);
            result.FVr_oa = new[] { z };// -z }; // it is a minimizer
            return result;
        }

        public override bool IsEnded()
        {
            return false;
        }
    }
}
