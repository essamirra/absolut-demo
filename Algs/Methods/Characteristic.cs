﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Algorithms.Methods
{
    public class Characteristic
    {
        public double evolventX;
        public MethodPoint left_x;
        public MethodPoint right_x;
        public double value_characteristic;
        public int id_process;
        public int sort_x_interval;

        public Characteristic()
        {
            left_x = new MethodPoint();
            right_x = new MethodPoint();
        }

        public override string ToString()
        {

            return String.Join("( ", value_characteristic) + ") ";
        }

    }
}
