﻿using System;
using System.Collections.Generic;
using System.Linq;
using Functions;
using Properties;

namespace Algorithms.Methods
{
    public class AlglibBleic: GenericAlg
    {
        private alglib.minbleicreport _minbleicreport;
        private const string StartingPoint = "Starting Point";

        public AlglibBleic()
        {
            RegisterProperty(new PointProperty(StartingPoint, new List<double>()));
            
        }

        private void IFunctionAdapter(double[] x, ref double func, object obj)
        {
          if (Problem != null) func = Problem.CalculateFunctionals(x.ToList(), 0);
        }

        public  void IterationCallback(double[] x, double func, object obj)
        {
            var methodPoint = new MethodPoint()
            {
                evolventX = 0,
                IdFun = 0,
                iteration = CurrentIteration + 1,
                y = func,
                x = x.ToList()
            };
            ExperimentPointsReal.Add(methodPoint);

            ExperimentPoints.Add(methodPoint);
            CurrentIteration++;
        }

        public override void RecalcPoints()
        {
            if(Problem == null) throw new NotSupportedException();
            Restart();
            ExperimentPoints.Clear();
            ExperimentPointsReal.Clear();
            Init();

            double epsg = 0.000001;
            double epsf = 0;
            double epsx = 0;
            int maxits = 0;

            //
            // This variable contains differentiation step
            //
            double diffstep = 1.0e-6;

            //
            // Now we are ready to actually optimize something:
            // * first we create optimizer
            // * we add boundary constraints
            // * we tune stopping conditions
            // * and, finally, optimize and obtain results...
            //
            List<double> start = GetProperty(StartingPoint) as List<double>;
            alglib.minbleicstate minbleicstate;
            alglib.minbleiccreatef(start.ToArray(), diffstep, out minbleicstate);
            alglib.minbleicsetbc(minbleicstate,Problem.LowerBound.ToArray() , Problem.UpperBound.ToArray());
            alglib.minbleicsetcond(minbleicstate, epsg, epsf, epsx, maxits);
            alglib.minbleicsetxrep(minbleicstate, true);
            alglib.minbleicoptimize(minbleicstate, IFunctionAdapter, IterationCallback, null);
            double[] result;
            alglib.minbleicresults(minbleicstate, out result, out _minbleicreport);

        }

        private void Init()
        {
           // Problem = ProblemFactory.BuildProblem((IFunction)GetProperty(StandartProperties.MainFunction));
            List<double> startPoint = new List<double>();

            for (int i = 0; i < Problem.LowerBound.Count; i++)
            {
               startPoint.Add(Problem.LowerBound[i] + Problem.UpperBound[i]/2);
            }
            SetPropertyWithoutRecalc(StartingPoint,startPoint);
            ExperimentPoints.Add(new MethodPoint()
            {
                IdFun = 1,
                iteration = 0,
                isEnd = AlgEnd.no,
                x = startPoint,
                y = Problem.CalculateFunctionals(startPoint, 0),
                evolventX = 0
            });
            ExperimentPointsReal.Add(new MethodPoint()
            {
                IdFun = 1,
                iteration = 0,
                isEnd = AlgEnd.no,
                x = startPoint,
                y = Problem.CalculateFunctionals(startPoint, 0),
                evolventX = 0
            });
            CurrentIteration = 0;
        }
    }
}