﻿using System;
using System.Collections.Generic;
using System.Linq;
using Functions;
using Properties;

namespace Algorithms.Methods
{
    class GeneticSimpleAlg : GenericAlg
    {
        GA ga;
        public GeneticSimpleAlg()
        {

            RegisterProperty(new DoubleProperty("crossoverRate", 0.0, 100.0, 1, DoubleDeltaType.Log, 0.8));
            RegisterProperty(new DoubleProperty("mutationRate", 0.0, 100.0, 1, DoubleDeltaType.Log, 0.05));
            RegisterProperty(new IntProperty("populationSize", 2, 300, 10, 100));
            RegisterProperty(new IntProperty("generationSize", 1, 5000, 100, 20));
        }

        private double theActualFunction(params double[] values)
        {
            if(Problem == null) throw new InvalidOperationException(ExceptionMessages.ProblemIsNull);
            var f = Problem.Criterions.FirstOrDefault();
            if (f == null) throw new InvalidOperationException(ExceptionMessages.ProblemWithoutCriterions);
           
            if (values.Length != 2)
                throw new ArgumentOutOfRangeException("should only have 2 args");

            double x = values[0];
            double y = values[1];

            List<double> values_list = new List<double>();
            values_list.Add(x);
            values_list.Add(y);
            return (-1.0) * f.Calc(values_list);

        }

        public override void RecalcPoints()
        {
            CurrentIteration = 0;
            ga = new GA((double)GetProperty("crossoverRate"), (double)GetProperty("mutationRate"),
            (int)GetProperty("populationSize"), (int)GetProperty("generationSize"), 2);
            var f = Problem.Criterions.FirstOrDefault();
            if (f == null) throw new InvalidOperationException(ExceptionMessages.ProblemWithoutCriterions);
            Restart();
            ExperimentPoints.Clear();
            ExperimentPointsReal.Clear();
            ga.FitnessFunction = theActualFunction;
            ga.Elitism = true;
            ga.Go(ref ExperimentPoints, ref CurrentIteration);
            ExperimentPointsReal = ExperimentPoints;
            CurrentIteration--;
        }

        public override bool IsEnded()
        {

            return false;
        }
    }
}

