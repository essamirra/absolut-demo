﻿using System;
using System.Linq;
using Properties;

namespace Algorithms.Methods
{
    [Serializable]
    public class Agp:CharacteristicAlg
    {
        
        private double m;
        public Agp()
        {
            Name = MethodsNames.Agp;
            RegisterProperty(new DoubleProperty("r", 1.000001, 100,100,DoubleDeltaType.Step, 2));
        }

        protected override double CalcNewPoint()
        {
            return (double) (0.5 * (ExperimentPoints[MaxCharIndex].evolventX +
                                    ExperimentPoints[MaxCharIndex - 1].evolventX) -
                             (ExperimentPoints[MaxCharIndex].y - ExperimentPoints[MaxCharIndex - 1].y) / (2 * m));
        }

        protected override double CalcCharacteristic(int curIntervalIndex)
        {
            if (curIntervalIndex == 0)
            {
                var d = new double[CurrentIteration];
                for (int i = 0; i < CurrentIteration; i++)
                {
                    var right = ExperimentPoints[i + 1];
                    var left = ExperimentPoints[i];
                    d[i] = Math.Abs((double) ((right.y - left.y) / (right.evolventX - left.evolventX)));
                }
                var M = d.Max();
                 m = M > 0 ? M * (double)GetProperty("r") : 1;
            }

            var point = ExperimentPoints[curIntervalIndex + 1];
            var methodPoint = ExperimentPoints[curIntervalIndex];
            return (double) (m * (point.evolventX - methodPoint.evolventX) +
                             ((point.y - methodPoint.y) * (point.y - methodPoint.y)) / (m * (point.evolventX - methodPoint.evolventX)) -
                             2 * (point.y + methodPoint.y));
        }
    }
}