﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Algorithms.Methods
{
    internal class ScanningAlgParal:CharacteristicAlgParal
    {
            protected override double CalcNewPoint()
            {
                return 0.5 * (CharacteristicList[maxCharIndex].right_x.evolventX +
                            CharacteristicList[maxCharIndex].left_x.evolventX);
            }

            protected override double CalcCharacteristic(int curIntervalIndex)
            {

                var right = ExperimentPoints[curIntervalIndex + 1];
                var left = ExperimentPoints[curIntervalIndex];
                CharacteristicList.Add(new Characteristic()
                {
                    evolventX = right.evolventX,
                    right_x = right,
                    sort_x_interval = curIntervalIndex,
                    left_x = left,
                    value_characteristic = right.evolventX - left.evolventX
                });
                return right.evolventX - left.evolventX;
            }
    }

}
