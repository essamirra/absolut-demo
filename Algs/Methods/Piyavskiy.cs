﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Properties;

namespace Algorithms.Methods
{
    class Piyavskiy : CharacteristicAlg
    {
        private double m;
        public Piyavskiy()
        {
            RegisterProperty(new DoubleProperty("r", 1.000001, 100, 100, DoubleDeltaType.Step, 2));
        }

        protected override double CalcNewPoint()
        {
            return (double) (0.5 * (ExperimentPoints[MaxCharIndex].evolventX +
                                    ExperimentPoints[MaxCharIndex - 1].evolventX) -
                             (ExperimentPoints[MaxCharIndex].y - ExperimentPoints[MaxCharIndex - 1].y) / (2 * m));
        }

        protected override double CalcCharacteristic(int curIntervalIndex)
        {
            if (curIntervalIndex == 0)
            {
                var d = new double[CurrentIteration];

                for (int i = 0; i < CurrentIteration; i++)
                {
                    var right = ExperimentPoints[i + 1];
                    var left = ExperimentPoints[i];
                    d[i] = Math.Abs((double) ((right.y - left.y) / (right.evolventX - left.evolventX)));

                }

                var M = d.Max();
                m = M > 0 ? M * (double)GetProperty("r") : 1;
            }
            var point = ExperimentPoints[curIntervalIndex + 1];
            var methodPoint = ExperimentPoints[curIntervalIndex];
            CharacteristicList.Add(new Methods.Characteristic()
            {
                evolventX = point.evolventX,
                right_x = point,
                sort_x_interval = curIntervalIndex,
                left_x = methodPoint,
                value_characteristic = (double) (0.5 * m * (point.evolventX - methodPoint.evolventX) -
                                                 (point.y + methodPoint.y) / 2.0)
            });
            return (double) (0.5 * m * (point.evolventX - methodPoint.evolventX) -
                             (point.y + methodPoint.y) / 2.0);
        }
    }
}
