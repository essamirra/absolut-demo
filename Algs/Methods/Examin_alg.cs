﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using Accord.Math;
using Functions;
using Properties;

namespace Algorithms.Methods
{
    class ExaminAlg : GenericAlg
    {
        protected IFunction f;
        protected int maxCharIndex;
        private string _examinConf;
        private string _examinDll = "Examin_dll";
        private string _examinExe = "Examin_exe";


        public ExaminAlg()
        {
            Name = "Examin";
            RegisterProperty(new DoubleProperty("Precision", 0.000001, 0.1, 5, DoubleDeltaType.Log, 0.00001));
            RegisterProperty(new StringProperty(_examinDll, ""));
            _examinConf = "Examin_conf";
            RegisterProperty(new StringProperty(_examinConf, ""));
            RegisterProperty(new StringProperty(_examinExe, ""));
            RegisterProperty(new IntProperty("Dimension", 2, 2, 0, 2));
            RegisterProperty(new IntProperty("Evolvent", 8, 12, 1, 10));
            RegisterProperty(new DoubleProperty("r", 1.000001, 100, 100, DoubleDeltaType.Step, 2));}

        public override void SetProperty(string name, object value)
        {
            if(name == _examinDll || name == _examinExe || name == _examinConf)
                SetPropertyWithoutRecalc(name, value);
            else
            {
                base.SetProperty(name,value);
            }
        }

        public override void RecalcPoints()
        {
            if ((string) GetProperty(_examinDll) == "" ||
                (string) GetProperty(_examinExe) == "") return;
            Restart();
            ExperimentPointsReal.Clear();
            Init();

            string path = @"out_point.txt";
            if (File.Exists(path))

                // Открываем файл для чтения и читаем из него все строки, построчно
                using (StreamReader sr = File.OpenText(path))
                {
                    int iter = 0;
                    string firstLine = sr.ReadLine();
                    string[] info = firstLine.Split(' ');
                    CurrentIteration = Convert.ToInt32(info[0]);

                    while (!sr.EndOfStream)
                    {
                        var s = sr.ReadLine();
                        string[] parts = s.Split('|');
                        if(parts.Length == 1) continue;
                        string[] point = parts[0].TrimEnd(' ').Split(' ');
                        string[] functionalsValues = parts[1].TrimStart(' ').TrimEnd(' ').Split(' ');
                        MethodPoint mp;
                        mp = new MethodPoint
                        {
                            iteration = iter,
                            constraints =  new List<double>(),
                            criterions =   new List<double>()
                        };
                        for (int i = 0; i < Problem.NumberOfConstratins && i < functionalsValues.Length; i++)
                        {
                            mp.constraints.Add(Convert.ToDouble(functionalsValues[i], CultureInfo.InvariantCulture));
                        }
                        mp.y = functionalsValues.Length == 1 + Problem.NumberOfConstratins
                            ?  (double?)Convert.ToDouble(functionalsValues.Last(), CultureInfo.InvariantCulture) : null;

                        foreach (var dim in point)
                        {
                            mp.x.Add(Convert.ToDouble(dim, CultureInfo.InvariantCulture));
                        }
                       // mp.y = Convert.ToDouble(parts[1].TrimStart(' ').Split(' ')[0], CultureInfo.InvariantCulture);

                        iter++;
                        mp.id_process = 0;
                        ExperimentPointsReal.Add(mp);

                    }
                }
        }

        public override bool IsEnded()
        {
            return true;
        }

        private void Init()
        {
            if ((string) GetProperty(_examinDll) == "" ||
                (string) GetProperty(_examinExe) == "")
                return;
            var t = "-Dimension " + ((int) GetProperty("Dimension")).ToString() +
                    " -r " + ((double) GetProperty("r")).ToString(CultureInfo.InvariantCulture) +
                    " -Eps " + ((double) GetProperty("Precision")).ToString(CultureInfo.InvariantCulture) +
                    " -m " + (int) GetProperty("Evolvent") +
                    " -MaxNumOfPoints " + (int) GetProperty("Maximum Iteration") +
                    " -sip " + @"""out_point.txt""" +
                    " - libConf " + @"""" + ((string)GetProperty("Examin_conf")).Replace("//", "/") + @"""" +
                    " -lib "+ @"""" +((string) GetProperty(_examinDll)).Replace("//", "/") + @"""";
            Process Examin_Process = new Process() {StartInfo =  new ProcessStartInfo()
            {
                FileName = (string)GetProperty(_examinExe),
                Arguments = t,
                UseShellExecute = false,
                RedirectStandardOutput = true,
                CreateNoWindow = true
            } };

            Examin_Process.Start();

            while (!Examin_Process.HasExited) ;
            var output = "";
           // var fileStream = File.Create(@"examinOutput" + System.DateTime.Now.Second +".txt" );

           // Examin_Process.StandardOutput.BaseStream.CopyTo(fileStream);
        }
    }
}