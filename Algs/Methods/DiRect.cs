﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using Functions;
using Properties;

namespace Algorithms.Methods
{
    class DiRect : GenericAlg
    {
        protected IFunction f;
        protected int maxCharIndex;


        public DiRect()
        {
            //RegisterProperty(new DoubleProperty("Precision", 0.000001, 0.1, 5, DoubleDeltaType.Log, 0.00001));
           RegisterProperty(new StringProperty("DiRect_dll", ""));
            RegisterProperty(new StringProperty("DiRect_conf", ""));
            RegisterProperty(new StringProperty("DiRect_exe", ""));
            RegisterProperty(new StringProperty("outDiRect", ""));
            //RegisterProperty(new IntProperty("Dimension", 2, 2, 0, 2));
            //RegisterProperty(new IntProperty("Evolvent", 8, 12, 1, 10));

        }
     

        protected virtual double CalcCharacteristic(int curIntervalIndex)
        {
            return 0;
        }

        protected virtual double CalcNewPoint()
        {
            return 0;
        }

        public override void RecalcPoints()
        {
            if ((string)GetProperty("DiRect_conf") == "" ||
                (string)GetProperty("DiRect_dll") == "" ||
                (string)GetProperty("DiRect_exe") == "" ||
                (string)GetProperty("outDiRect") == "" ||
                f == null) return;
            Restart();
            ExperimentPoints.Clear();
            Init();

            string path = (string)GetProperty("outDiRect");
            if (File.Exists(path))

                // Открываем файл для чтения и читаем из него все строки, построчно
                using (StreamReader sr = File.OpenText(path))
                {
                    int iter = 0;
                    string firstLine = sr.ReadLine();
                    //string[] info = firstLine.Split(' ');
                    //realIteration = Convert.ToInt32(info[0]);
                    CurrentIteration = Convert.ToInt32(firstLine);
                    while (!sr.EndOfStream)
                    {
                        var s = sr.ReadLine();
                        string[] point = s.Split(' ');
                        MethodPoint mp;
                        mp = new MethodPoint
                        {
                            iteration = iter
                        };
                        List<double> x_iter = new List<double>();
                        mp.x.Add(Convert.ToDouble(point[0], CultureInfo.InvariantCulture));
                        mp.x.Add(Convert.ToDouble(point[1], CultureInfo.InvariantCulture));
                        mp.y = f.Calc(mp.x);
                        iter++;
                        mp.id_process = 0;
                        ExperimentPointsReal.Add(mp);

                    }
                }
        }
        public override bool IsEnded()
        {
            return true;
        }

        private void Init()
        {
           
            if ((string)GetProperty("DiRect_conf") == "" || (string)GetProperty("DiRect_dll") == "" ||
                (string)GetProperty("DiRect_exe") == "")
                return;
            Process diRectProcess = new Process()
            {
                StartInfo = new ProcessStartInfo()
                {
                    FileName = (string)GetProperty("DiRect_exe"),
                    Arguments = (string)GetProperty("DiRect_dll") + (string)GetProperty("DiRect_conf") +
                    (string)GetProperty("outDiRect"),
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                    CreateNoWindow = true
                }
            };
            diRectProcess.Start();
            while (!diRectProcess.HasExited);
            var output = "";
            while (!diRectProcess.StandardOutput.EndOfStream)
            {
                output += diRectProcess.StandardOutput.ReadLine();
                // do something with line
            }
        }
    }
}
