﻿using System;
using System.Collections.Generic;

namespace Properties
{
    public enum DoubleDeltaType
    {
        Step,
        Log
    }

    public class DoubleProperty : PropertyInfo
    {
        public double min { get; protected set; }
        public double max { get; protected set; }
        public int delta { get; set; }
        public DoubleDeltaType deltaType { get; protected set; }


        public DoubleProperty(string name, double min, double max, int delta, DoubleDeltaType type, double defaultValue)
        {
            Type = PropertyType.Double;
            if (defaultValue > max || defaultValue < min) throw new ArgumentException();
            if (delta <= 0) throw new ArgumentOutOfRangeException();
            this.name = name;
            this.min = min;
            this.max = max;
            this.delta = delta;
            this.defaultValue = defaultValue;
            deltaType = type;
        }


        public override bool CheckValue(object value)
        {
            double val = 0;
            try
            {
                val = (double)value;
            }
            catch (Exception)
            {
                return false;
            }
            return !(val < min) && !(val > max);
        }
    }
    public class StringProperty : PropertyInfo
    {


        public StringProperty(string name, string defaultValue)
        {
            Type = PropertyType.String;
            this.name = name;
            this.defaultValue = defaultValue;

        }


        public override bool CheckValue(object value)
        {
            string val = "";
            try
            {
                val = (string)value;
            }
            catch (Exception)
            {

                return false;
            }
            return true;
        }
    }
    public class IntProperty : PropertyInfo
    {
        public int min { get; protected set; }
        public int max { get; protected set; }
        public int delta { get; set; }

        public IntProperty(string name, int min, int max, int delta, int defaultValue)
        {
            Type = PropertyType.Integer;
            if (defaultValue > max || defaultValue < min) throw new ArgumentException();
            this.name = name;
            this.min = min;
            this.max = max;
            this.delta = delta;
            this.defaultValue = defaultValue;
        }

        public override bool CheckValue(object value)
        {
            int val;
            try
            {
                val = (int)value;
            }
            catch (Exception)
            {
                return false;
            }
            return !(val < min) && !(val > max);
        }
    }
    public class PointProperty : PropertyInfo
    {


        public PointProperty(string name, List<double> defaultValue)
        {
            Type = PropertyType.String;
            this.name = name;
            this.defaultValue = defaultValue;

        }


        public override bool CheckValue(object value)
        {
            try
            {
                var val = value as List<double>;
                if (val == null)
                {
                    return false;
                }
            }
            catch (Exception)
            {

                return false;
            }
            return true;
        }
    }
}