﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Properties
{
    public interface IPropertyUser
    {
        string Name { get;}
        void SetProperty(string name, object value);
        void SetProperties(Dictionary<string, object> propsValues);
        object GetProperty(string name);
        List<PropertyInfo> GetPropertiesInfo();
        void RegisterProperty(PropertyInfo p);
    }
    public abstract class PropertyInfo
    {
        public string name { get; protected set; }
        public object defaultValue { get; protected set; }
        public abstract bool CheckValue(object value);
        public PropertyType Type { get; protected set; }
    }
    public enum PropertyType
    {
        Double,
        Integer,
        Func,
        Bool,
        Color,
        String,
        Point
    }

}
