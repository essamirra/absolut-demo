﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Properties
{
    [Serializable]
    public class PropertyProvider : IPropertyUser, ISerializable
    {
        protected Dictionary<string, object> properties = new Dictionary<string, object>();
        protected  List<PropertyInfo> PropertyInfos = new List<PropertyInfo>();

        public string Name => "Property Provider";

        public void SetProperty(string name, object value)
        {
            if (!properties.ContainsKey(name)) throw new ArgumentException("Can not set non existing property");
            PropertyInfo info = PropertyInfos.Find(t => t.name == name);
            if (!info.CheckValue(value)) throw new ArgumentException("Can not set wrong value");
            properties[name] = value;
        }

        public void SetProperties(Dictionary<string, object> propsValues)
        {
            foreach (var p in propsValues)
            {
                SetProperty(p.Key, p.Value);
            }
        }

        public object GetProperty(string name)
        {
            if (!properties.ContainsKey(name)) throw new ArgumentException("Property does not exist");
            return properties[name];
        }

        public List<PropertyInfo> GetPropertiesInfo()
        {
            return PropertyInfos;
        }

        public void RegisterProperty(PropertyInfo p)
        {
          PropertyInfos.Add(p);
            if (properties.ContainsKey(p.name)) properties[p.name] = p;
           else properties.Add(p.name, p.defaultValue);
        }

        public void RemoveProperty(PropertyInfo p)
        {
            properties.Remove(p.name);
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            throw new NotImplementedException();
        }
    }
}
