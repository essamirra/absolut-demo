﻿using System;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using Algorithms;
using Algorithms.Methods;
using Functions;
using NUnit.Framework;

namespace Tests
{
    [TestFixture]
    public class AlgSerializationTest
    {
        
        [Test]
        public void ProblemToXML()
        {
            IProblem p = ProblemFactory.BuildNotConstrainedProblem(ProblemFactory.NotConstrainedOptions.Ackley1);
            ProblemFactory.ProblemToXML(p, "E://problem.xml");
        }

        [Test]
        public void AlgToXML()
        {
            IProblem p = ProblemFactory.BuildNotConstrainedProblem(ProblemFactory.NotConstrainedOptions.Ackley1);
            ISearchAlg a = AlgFactory.Build("Agp");
            a.Problem = p;
            AlgFactory.SaveToXML(a, "E://method.xml");
            
        }
        [Test]
        public void RestoreProblemFromXML()
        {
            IProblem p = ProblemFactory.BuildNotConstrainedProblem(ProblemFactory.NotConstrainedOptions.Ackley1);
            var eProblemXml = "E://problem.xml";
            ProblemFactory.ProblemToXML(p, eProblemXml);
            IProblem res = ProblemFactory.ProblemFromXML(eProblemXml);
            Assert.AreEqual(res.Functionals[0].GetType().ToString(), "Functions.Functions.AckleyFunction1");
        }

    }
}