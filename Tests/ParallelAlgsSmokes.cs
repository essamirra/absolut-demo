﻿using System.Collections.Generic;
using System.Linq;
using Algorithms;
using Algorithms.Methods;
using Functions;
using NUnit.Framework;

namespace Tests
{
    [TestFixture]
    public class ParallelAlgsSmokes
    {
        private IProblem p = ProblemFactory.BuildNotConstrainedProblem("Parabaloid");
        [Test]
        public void AgpParalSmoke()
        {
            ISearchAlg a = AlgFactory.Build("AGP_paral");
            a.Problem = p;
            a.RecalcPoints();
            List<int> pointsFromDifferentProcesses = new List<int>((int)a.GetProperty(StandartProperties.ProccessCount));
            for (int i = 0; i < (int)a.GetProperty(StandartProperties.ProccessCount); i++)
            {
                pointsFromDifferentProcesses.Add(0);
            }
            foreach (var point in a.CalculatedPoints())
            {
                pointsFromDifferentProcesses[point.id_process]++;

            }
            foreach (var var in pointsFromDifferentProcesses)
            {
                Assert.AreNotEqual(0, var);
            }
        }
        [Test]
        public void ScanningParalSmoke()
        {

            ISearchAlg a = AlgFactory.Build("Scanning_alg_paral");
            a.Problem = p;
            a.RecalcPoints();
            List<int> pointsFromDifferentProcesses = new List<int>((int)a.GetProperty(StandartProperties.ProccessCount));
            for (int i = 0; i < (int)a.GetProperty(StandartProperties.ProccessCount); i++)
            {
                pointsFromDifferentProcesses.Add(0);
            }
            foreach (var point in a.CalculatedPoints())
            {
                pointsFromDifferentProcesses[point.id_process]++;

            }
            foreach (var var in pointsFromDifferentProcesses)
            {
                Assert.AreNotEqual(0, var);
            }
        }
     }
}