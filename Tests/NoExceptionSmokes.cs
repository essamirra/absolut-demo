﻿using System;
using System.CodeDom.Compiler;
using Algorithms;
using Algorithms.Methods;
using Executors.Impl;
using Functions;
using NUnit.Framework;
using NUnit.Framework.Internal;

namespace Tests
{
    [TestFixture]
    public class NoExceptionSmokes
    {
        private IProblem p = ProblemFactory.BuildNotConstrainedProblem("Parabaloid");
        [Test]
        public void AgpSmoke()
        {
           ISearchAlg a = new Agp();
           a.Problem = p;
            Assert.DoesNotThrow(a.RecalcPoints);
        }

        [Test]
        public void ScanningSmoke()
        {
          ISearchAlg a = new ScanningAlg();
            a.Problem = p;
            Assert.DoesNotThrow(a.RecalcPoints);
        }

        [Test]
        public void PiavskyNoParalSmoke()
        {

            AlgExecutor.Create().WithAlgorithm(MethodsNames.Piyavsky).WithProblem(p).Execute();
        }
        [Test]
        public void GASmoke()
        {
            ISearchAlg a = AlgFactory.Build("GeneticSimpleAlg");
            a.Problem = p;
            Assert.DoesNotThrow(a.RecalcPoints);
        }
        [Test]
        public void PiyavskySmoke()
        {

            ISearchAlg a = AlgFactory.Build("Piyavskiy");
            a.Problem = p;
            Assert.DoesNotThrow(a.RecalcPoints);
        }

        [Test]
        public void AgpParalSmoke()
        {
            ISearchAlg a = AlgFactory.Build("AGP_paral");
            a.Problem = p;
            Assert.DoesNotThrow(a.RecalcPoints);
        }
        [Test]
        public void ScanningParalSmoke()
        {

            ISearchAlg a = AlgFactory.Build("Scanning_alg_paral");
            a.Problem = p;
            Assert.DoesNotThrow(a.RecalcPoints);
        }

        [Test]
        public void AgpReductionSmoke()
        {

            ISearchAlg a = AlgFactory.Build( "Reduction_AGP");
            a.Problem = p;
            Assert.DoesNotThrow(a.RecalcPoints);
        }
        [Test]
        public void DESmoke()
        {

            ISearchAlg a = AlgFactory.Build("DE");
            a.Problem = p;
            Assert.DoesNotThrow(a.RecalcPoints);
        }

        [Test]
        public void AlgLibSmoke()
        {

            ISearchAlg a = AlgFactory.Build(MethodsNames.Bleic);
            a.Problem = p;
            Assert.DoesNotThrow(a.RecalcPoints);
        }

        [Test]
        public void Nsga2Smoke()
        {
            ISearchAlg a = new NSGA2();
            a.Problem = p;
            Assert.DoesNotThrow(a.RecalcPoints);
        }

        [Test]
        public void AlglibNlcSmoke()
        {
            ISearchAlg a = new AlglibAul();
            a.Problem = ProblemFactory.BuildStandartConstrainedProblem("RosenbrokCubeLine");
            Assert.DoesNotThrow(a.RecalcPoints);
        }

    }
}