﻿using System;
using System.IO;
using System.Text;
using Algorithms;
using Algorithms.Methods;
using Functions;
using NUnit.Framework;

namespace Tests
{
    [TestFixture]
    public class ProblemSmokes
    {
        private IProblem p = ProblemFactory.BuildStandartConstrainedProblem("RosenbrokCubeLine");
        [Test]
        public void LeftCountEquals2()
        {
            var newFunction = p.Criterions[0];
            Assert.AreEqual(newFunction.Left.Count, 2);
        }

        [Test]
        public void GenerateFiles()
        {
            string s = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n<config>\r\n  <function_number>";
            string s1 = "</function_number >\r\n </config >\r\n";
            string path = "E:\\grish\\grish_conf";
            string path2 = ".xml";
            for (int i = 1; i < 101; i++)
            {
                using (FileStream fs = File.Create(path+i+path2))
                {
                    Byte[] info = new UTF8Encoding(true).GetBytes(s+i+s1);
                    // Add some information to the file.
                    fs.Write(info, 0, info.Length);
                }
            }
        }


    }
}