﻿using System.Linq;
using Algorithms;
using Algorithms.Methods;
using Functions;
using NUnit.Framework;

namespace Tests
{
    [TestFixture]
    public class PointsPopulatedSmokes
    {
        private IProblem p = ProblemFactory.BuildNotConstrainedProblem("Parabaloid");
        [Test]
        public void AgpSmoke()
        {
            ISearchAlg a = new Agp();
            a.Problem = p;
            a.RecalcPoints();
            Assert.AreNotEqual(0,a.CalculatedPoints().Count());
        }

        [Test]
        public void ScanningSmoke()
        {
            ISearchAlg a = new ScanningAlg();
            a.Problem = p;
            a.RecalcPoints();
            Assert.AreNotEqual(0, a.CalculatedPoints().Count());
        }

        [Test]
        public void PiavskyNoParalSmoke()
        {

            ISearchAlg a =AlgFactory.Build(MethodsNames.Piyavsky);
            a.Problem = p;
            a.RecalcPoints();
            Assert.AreNotEqual(0, a.CalculatedPoints().Count());
        }
        [Test]
        public void GASmoke()
        {

            ISearchAlg a = AlgFactory.Build("GeneticSimpleAlg");
            a.Problem = p;
            a.RecalcPoints();
            Assert.AreNotEqual(0, a.CalculatedPoints().Count());
        }
        [Test]
        public void PiyavskySmoke()
        {

            ISearchAlg a = AlgFactory.Build("Piyavskiy");
            a.Problem = p;
            a.RecalcPoints();
            Assert.AreNotEqual(0, a.CalculatedPoints().Count());
        }

        [Test]
        public void AgpParalSmoke()
        {
            ISearchAlg a = AlgFactory.Build("AGP_paral");
            a.Problem = p;
            a.RecalcPoints();
            Assert.AreNotEqual(0, a.CalculatedPoints().Count());
        }
        [Test]
        public void ScanningParalSmoke()
        {

            ISearchAlg a = AlgFactory.Build("Scanning_alg_paral");
            a.Problem = p;
            a.RecalcPoints();
            Assert.AreNotEqual(0, a.CalculatedPoints().Count());
        }
       
        [Test]
        public void AgpReductionSmoke()
        {

            ISearchAlg a = AlgFactory.Build( "Reduction_AGP");
            a.Problem = p;
            a.RecalcPoints();
            Assert.AreNotEqual(0, a.CalculatedPoints().Count());
        }
        [Test]
        public void DESmoke()
        {

            ISearchAlg a = AlgFactory.Build("DE");
            a.Problem = p;
            a.RecalcPoints();
            Assert.AreNotEqual(0, a.CalculatedPoints().Count());
        }

        [Test]
        public void AlgLibSmoke()
        {

            ISearchAlg a = AlgFactory.Build(MethodsNames.Bleic);
            a.Problem = p;
            a.RecalcPoints();
            Assert.AreNotEqual(0, a.CalculatedPoints().Count());
        }

        [Test]
        public void AlglibNlcSmoke()
        {
            ISearchAlg a = new AlglibAul();
            a.Problem = ProblemFactory.BuildStandartConstrainedProblem("RosenbrokCubeLine");
            a.RecalcPoints();
            Assert.AreNotEqual(0, a.CalculatedPoints().Count());
        }
    }
}