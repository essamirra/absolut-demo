﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Executors
{
    interface IAlgExecutor
    {
        AlgorithmResult Execute();
        IAlgExecutor WithProblem(string problemName);
        IAlgExecutor WithAlgorithm(string algorithmName);
        IAlgExecutor Create();

    }
}
