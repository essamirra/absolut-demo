﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Algorithms.Methods;

namespace Executors
{
    public class AlgorithmResult
    {
        public readonly List<MethodPoint> ExperimentPoints;
        public readonly int Iterations;
        public readonly TimeSpan Time;
        public readonly string Problem;
        public readonly string Algorithm;

        public AlgorithmResult(List<MethodPoint> experimentPoints, int iterations, TimeSpan time, string problem, string algorithm)
        {
            ExperimentPoints = experimentPoints;
            Iterations = iterations;
            Time = time;
            Problem = problem;
            Algorithm = algorithm;
        }
    }
}
