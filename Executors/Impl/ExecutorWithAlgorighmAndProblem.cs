﻿using System;
using System.Collections.Generic;
using Algorithms;
using Algorithms.Methods;

namespace Executors
{
    public class ExecutorWithAlgorighmAndProblem
    {
        
        private ISearchAlg _alg;
        private IProblem _problem;
        private string _algName;
        private string _problemName;

        public AlgorithmResult Execute()
        {
            _alg.Problem = _problem;
            DateTime startTime = DateTime.Now;
            _alg.RecalcPoints();
            AlgorithmResult result = new AlgorithmResult(new List<MethodPoint>(_alg.CalculatedPoints()),
                _alg.IterationsPerformed, DateTime.Now - startTime, _problemName, _algName);
            return result;
        }

        ExecutorWithAlgorighmAndProblem SetParameters(Dictionary<String, Object> paramDictionary)
        {
            _alg.SetProperties(paramDictionary);
            return this;
        }

        internal ExecutorWithAlgorighmAndProblem(ISearchAlg alg, IProblem problem, string algName, string problemName)
        {
            _alg = alg;
            _problem = problem;
            _algName = algName;
            _problemName = problemName;
        }
    }
}