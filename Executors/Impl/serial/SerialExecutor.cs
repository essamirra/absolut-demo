﻿using System.Dynamic;
using Algorithms;
using Executors.Impl.serial.alg;

namespace Executors.Impl.serial
{
    public class SerialExecutor
    {
        public static SerialExecutor Create()
        {
            return new SerialExecutor();
        }

        public SerialExecutorWithOneAlg WithOneAlg(string algName)
        {
            return new SerialExecutorWithOneAlg(algName, AlgFactory.Build(algName));
        }

    }
}