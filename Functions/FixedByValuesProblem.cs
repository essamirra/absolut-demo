﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Algorithms;

namespace Functions
{
    public class FixedByValuesProblem : Problem
    {
        public FixedByValuesProblem(string configPath, List<double> lowerBound, List<double> upperBound, double? optimalValue, List<double> optimalPoint, List<IFunction> criterions, List<IFunction> constraints) : base(configPath, lowerBound, upperBound, optimalValue, optimalPoint, criterions, constraints)
        {
        }

        public FixedByValuesProblem(IProblem problemToSlice, Dictionary<int, double> _fixedVars
        ) : base(problemToSlice.ConfigPath,problemToSlice.LowerBound, problemToSlice.UpperBound, problemToSlice.OptimalValue, problemToSlice.OptimalPoint,problemToSlice.Criterions, problemToSlice.Constraints)
        {
            throw new NotImplementedException();
        }
    }
}
