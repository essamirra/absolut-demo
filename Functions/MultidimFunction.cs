﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace Functions
{
    [Serializable]
    public class MultidimFunction : IFunction, IXmlSerializable
    {
        public List<double> Right { get; set; }
        public List<double> Left { get; set; }
        public List<double> Min_x { get; set; }

        public int Dimension
        {
            get => _dimension;
            set => _dimension = value;
        }

        private Func<List<double>, double> _calcFunc;
        public double? Min
        {
            get { return _min; }
            set { _min = value; }
        }

        private PeanoEvolvent p;
        private double? _min;
        private string _name;
        private int _dimension;

        public virtual double Calc(List<double> arg)
        {
            if (_calcFunc != null)
                return _calcFunc(arg);
            else throw new NotSupportedException();
        }

        public MultidimFunction()
        {
            _calcFunc = null;
        }

        public MultidimFunction(Func<List<double>, double> calcValue)
        {
            _calcFunc = calcValue;

        }
        public double Calc(double x)
        {
            return Calc(GetImage(x));
        }

        public List<double> GetImage(double x)
        {
            if (p == null)
            {
                p = new PeanoEvolvent(Left.Count, 19);
                p.SetBounds(Left.ToArray(), Right.ToArray());
            }
            double[] image;
            p.GetImage(x, out image);
            if(!this.CheckPoint(image.ToList())) throw new ArgumentOutOfRangeException();
            return image.ToList();
        }

        public double GetPrototype(List<double> x)
        {
            double[] image = x.ToArray();
            double pr_x = 0;
            if (p == null)
            {
                p = new PeanoEvolvent(Left.Count, 19);
                p.SetBounds(Left.ToArray(), Right.ToArray());
            }
            p.GetPreimages(image, out pr_x);
            return pr_x;
        }

        public virtual int GetNumberOfFunctions()
        {
            return 1;
        }

        public virtual void SetFunctionNumber(int index)
        { }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(XmlReader reader)
        {
            Type type = Type.GetType(reader.GetAttribute("AssemblyQualifiedName"));
            XmlSerializer serial = new XmlSerializer(type);
            reader.ReadStartElement("Function");
            serial.Deserialize(reader);
            reader.ReadEndElement();
        }

        public void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("Function");
            writer.WriteAttributeString("AssemblyQualifiedName", this.GetType().AssemblyQualifiedName ?? throw new InvalidOperationException());
            XmlSerializer xmlSerializer = new XmlSerializer(GetType());
            xmlSerializer.Serialize(writer, this);
            writer.WriteEndElement();

        }
    }
}