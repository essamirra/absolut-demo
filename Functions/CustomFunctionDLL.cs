﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms.ComponentModel.Com2Interop;
using Properties;

namespace Functions
{
    [Serializable]
    public class CustomFunctionDLL:MultidimFunction, IPropertyUser, ICloneable
    {
        private ProblemInterface problem;
        private PropertyProvider p = new PropertyProvider();

        public CustomFunctionDLL(ProblemInterface problem)
        {
            this.problem = problem;
            double[] left = new double[2];
            double[] right = new double[2];
            problem.GetBounds(left,right);
            Left = left.ToList();
            Right = right.ToList();
            RegisterProperty(new IntProperty("Function number", 0, this.problem.GetNumberOfFunctions(), 1, 0));
        }

        public override double Calc(List<double> arg)
        {
            return problem.CalculateFunctionals(arg.ToArray(), (int) GetProperty("Function number"));
        }

        public override int GetNumberOfFunctions()
        {
            return problem.GetNumberOfFunctions();
        }

        public override void SetFunctionNumber(int index)
        {
            SetProperty("Function number", index);
        }

        public string Name
        {
            get { return "Function from dll"; }
        }

        public void SetProperty(string name, object value)
        {
            p.SetProperty(name, value);
        }

        public void SetProperties(Dictionary<string, object> propsValues)
        {
            p.SetProperties(propsValues);
        }

        public object GetProperty(string name)
        {
            return p.GetProperty(name);
        }

        public List<PropertyInfo> GetPropertiesInfo()
        {
            return p.GetPropertiesInfo();
        }

        public void RegisterProperty(PropertyInfo p)
        {
            this.p.RegisterProperty(p);
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}