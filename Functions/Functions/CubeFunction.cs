﻿using System;
using System.Collections.Generic;

namespace Functions.Functions
{
    internal class CubeFunction : MultidimFunction
    {
        public CubeFunction()
        {
            Right = new List<double> {10, 10};
            Left = new List<double> {-10, -10};
        }


        public override double Calc(List<double> arg)
        {
            if (!this.CheckPoint(arg)) throw new ArgumentException();
            return 100 * (arg[1] - Math.Pow(arg[0], 3)) * (arg[1] - Math.Pow(arg[0], 3)) +
                   (1 - arg[0]) * (1 - arg[0]);
        }
    }
}