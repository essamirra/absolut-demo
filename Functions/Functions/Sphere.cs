﻿using System;
using System.Collections.Generic;

namespace Functions
{
    internal class Paraboloid : MultidimFunction
    {
        public Paraboloid()
        {
            Right = new List<double> {10, 10};
            Left = new List<double> {-10, -10};
        }

        public override double Calc(List<double> arg)
        {
           // if (!this.CheckPoint(arg)) throw new ArgumentException();
            return arg[0] * arg[0] + arg[1] * arg[1];
        }
    }

    internal class Pyramid : MultidimFunction
    {
        public Pyramid()
        {
            Right = new List<double> {500, 500};
            Left = new List<double> {-500, -500};
        }


        public override double Calc(List<double> arg)
        {
           // if (!this.CheckPoint(arg)) throw new ArgumentException();
            return 1 - Math.Abs(arg[0] + arg[1]) - Math.Abs(arg[1] - arg[0]);
        }
    }

    internal class Ripple : MultidimFunction
    {
        public Ripple()
        {
            Right = new List<double> {10, 10};
            Left = new List<double> {-10, -10};
        }

        public override double Calc(List<double> arg)
        {
            if (!this.CheckPoint(arg)) throw new ArgumentException();
            return Math.Sin(10 * (arg[0] * arg[0] + arg[1] * arg[1]) * Math.PI / 180.0 / 10);
        }
    }

    internal class Tube : MultidimFunction
    {
        public Tube()
        {
            Right = new List<double> {10, 10};
            Left = new List<double> {-10, -10};
        }


        public override double Calc(List<double> arg)
        {
            if (!this.CheckPoint(arg)) throw new ArgumentException();
            return 1 / (15 * (arg[0] * arg[0] + arg[1] * arg[1]));
        }
    }

    internal class Bumps : MultidimFunction
    {
        public Bumps()
        {
            Right = new List<double> {100, 100};
            Left = new List<double> {-50, -50};
        }

        public override double Calc(List<double> arg)
        {
            return Math.Sin(5 * arg[0] * Math.PI / 180.0) * Math.Cos(5 * arg[1] * Math.PI / 180.0) / 5;
        }
    }
}