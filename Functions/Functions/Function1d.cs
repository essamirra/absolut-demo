﻿using System;
using System.Collections.Generic;

namespace Functions
{
    class Function1d : IFunction
    {
        private double? _min;
        private string _name;
        private int _dimension;
        public List<double> Right { get; set; }
        public List<double> Left { get; set; }
        public List<double> Min_x { get; set; }

        public int Dimension
        {
            get { return 1; }
            set { _dimension = 1; }
        }

        public double? Min
        {
            get { return _min; }
            set { _min = value; }
        }

        public virtual double Calc(List<double> arg)
        {
            if(!this.CheckPoint(arg))throw new ArgumentOutOfRangeException();
            return Calc(arg[0]);
        }

        public virtual double Calc(double x)
        {
            return 0;
        }

        public  List<double> GetImage(double x)
        {
            if (!this.CheckPoint(new List<double>() { x })) throw new ArgumentOutOfRangeException();
            return new List<double>() {x};
        }
        public double GetPrototype(List<double> x)
        {
            if (!this.CheckPoint(x)) throw new ArgumentOutOfRangeException();
            return x[0];
        }

        public virtual int GetNumberOfFunctions()
        {
            return 1;
        }

        public virtual void SetFunctionNumber(int index)
        { }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
    }
}