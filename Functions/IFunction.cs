﻿using System;
using System.Collections.Generic;

namespace Functions
{
    public interface IFunction
    {
        List<double> Right { get; set; }
        List<double> Left { get; set; }
        List<double> Min_x { get; set; }
        int Dimension { get; set; }
        double? Min { get; set; }
        double Calc(List<double> arg);
        double Calc(double x);
        List<double> GetImage(double x);
        double GetPrototype(List<double> x);
        int GetNumberOfFunctions();
        void SetFunctionNumber(int index);
        String Name { get; set; }

    }
}