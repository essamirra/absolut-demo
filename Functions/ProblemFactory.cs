﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using Algorithms;
using Functions.Functions;

namespace Functions
{
    public static class ProblemFactory
    {
        public struct NotConstrainedOptions
        {
            public const string Ackley1 = "Ackley1";
            public const string Ackley2 = "Ackley2";
            public const string Adjiman = "Adjiman";
            public const  string Beale = "Beale";
            public const string Cube = "Cube";
            public const string Bumps =  "Bumps";
            public const string Tube =  "Tube";
            public const string Parabalod = "Parabaloid";
            public const string BartelsConn = "Bartels Conn";
            public const string GKLS = "GKLS";
            public const string Grishagin = "Grishagin";
            public const string Squares = "Squares";
            public const string Ripple = "Ripple";
            public const string Pyramid = "Pyramid";
            public const string Rastrigin = "Rastrigin";
            public const string GoldsteinPrice = "Goldstein - Price";
    };
        public static List<string> NotConstrainedOptionsList = new List<string>
        {
            "Ackley1",
            "Ackley2",
            "Adjiman",
            "Beale",
            "Cube",
            "Bumps",
            "Parabaloid",
            "Bartels Conn",
            "GKLS",
            "Squares",
            "Ripple",
            "Pyramid",
            "Rastrigin",
            "Goldstein - Price"
        };

        public static List<string> ConstrainedOptions = new List<string>
        {
            "RosenbrokCubeLine"
        };

        public static IProblem BuildProblem(IFunction f)
        {
            return new Problem(null, f.Left, f.Right, null, null, new List<IFunction>() {f}, new List<IFunction>() ) ;
        }

        public static IProblem BuildProblemFromDll(string pathToDll, string pathToConfig)
        {
            var p = new ProblemManager.ProblemManager();

            if (p.LoadProblemLibrary(pathToDll) != ProblemManager.ProblemManager.OK_)
                throw new ArgumentException("Can not load lib");

            var problem = p.GetProblem();
           // problem.SetDimension(2);
            problem.SetConfigPath(pathToConfig);
            problem.Initialize();
            problem.SetDimension(2);
            
            return new ProblemDll(problem) {Name =  "DLL", ConfigPath = pathToConfig, DllPath = pathToDll};
        }

        public static IProblem BuildProblem(List<IFunction> constraints, List<IFunction> criterions, List<double> lower, List<double> upper)
        {
            return new Problem(null, lower, upper, null, null, criterions, constraints);
        }


        public static IProblem BuildStandartConstrainedProblem(string name)
        {
            if(name == "RosenbrokCubeLine")
                return new RosenbrokCubeLine() {Name = "RosenbrokCubeLine" };
             return new RosenbrokCubeLine();
        }

        public static IProblem BuildNotConstrainedProblem(string name)
        {
            var buildNotConstrainedProblem = BuildProblem(FunctionFactory.Build(name));
            buildNotConstrainedProblem.Name = name;
            return buildNotConstrainedProblem;
        }

        public static IProblem BuildFromTextInput(List<double> left, List<double> right, string formula)
        {
            throw new NotImplementedException();
        }

        public static IProblem BuildGKLS(int functionNum)
        {
            GKLSDefault f = new GKLSDefault(functionNum);
            return new Problem(null, f.Left, f.Right, f.GetOptimumValue(), f.GetOptimumCoordinates(), new List<IFunction>() {f}, new List<IFunction>()  ) {Name = "GKLS DLL"};
        }

        public static void ProblemToXML(IProblem p, string path)
        {
            XmlWriter writer = null;
            XmlWriterSettings settings = new XmlWriterSettings
            {
                Indent = true,
                IndentChars = ("\t"),
                OmitXmlDeclaration = true
            };

            // Create the XmlWriter object and write some content.
            writer = XmlWriter.Create(path,settings);
            const string startElement = "Problem";
            writer.WriteStartElement(startElement);
            writer.WriteAttributeString("name", p.Name);
            writer.WriteEndElement();
            writer.Flush();
            writer.Close();
        }

        public static IProblem ProblemFromXML(string path)
        {
            XmlReader reader =  XmlReader.Create(new FileStream(path, FileMode.Open));
            
            reader.ReadToFollowing("Problem");
            reader.MoveToFirstAttribute();
            return BuildNotConstrainedProblem(reader.Value);

        }


        public static IProblem BuildProblemWithFixedVars(Dictionary<int, double> fixedVars, IProblem problem)
        {
            List<IFunction> newConstraints = new List<IFunction>(problem.NumberOfConstratins);
            foreach (var problemConstraint in problem.Constraints)
            {
                newConstraints.Add(new FixedByValuesFunction(problemConstraint, fixedVars));
            }

            List<IFunction> newCriterions = new List<IFunction>(problem.NumberOfCriterions);
            foreach (var problemConstraint in problem.Criterions)
            {
                newConstraints.Add(new FixedByValuesFunction(problemConstraint, fixedVars));
            }

            return BuildProblem(newConstraints, newCriterions, newCriterions[0].Left, newCriterions[0].Right);

        }
    }
}
