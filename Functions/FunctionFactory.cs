﻿using System;
using System.Collections.Generic;
using System.IO;
using Functions.Functions;

namespace Functions
{
    public static class FunctionFactory
    {
        
        public static List<string> Options = new List<string>
        {
            "Ackley1",
            "Ackley2",
            "Adjiman",
            "Beale",
            "Cube",
            "Bumps",
            "Tube",
            "Parabaloid",
            "Bartels Conn",
            "GKLS",
            "Grishagin",
            "Squares",
            "Ripple",
            "Pyramid",
            "Rastrigin",
            "Goldstein - Price"
        };

        public static IFunction Build(string name)
        {
            switch (name)
            {
                case "Ackley1":
                    return new AckleyFunction1();
                case "Ackley2":
                    return new AckleyFunction2();
                case "Adjiman":
                    return new AdjimanFunction();
                case "Beale":
                    return new BealeFunction();
                case "Cube":
                    return new CubeFunction();
                case "Bumps":
                    return new Bumps();
                case "Parabaloid":
                    return new Paraboloid();
                case "Bartels Conn":
                    return new BartelsConnFunction();
                case "GKLS":
                    return new GKLSDefault();
                case "Squares":
                    return new Squares();
                case "Ripple":
                    return new Ripple();
                case "Pyramid":
                    return new Pyramid();
                case "Rastrigin":
                    return new Rastrigin(2);
                case "Goldstein - Price":
                    return new GoldsteinPrice();
                    
            }


            throw new ArgumentOutOfRangeException();
        }
        
        
        public static IFunction BuildFromFile(string path)
        {
            var left = new List<double>();
            var right = new List<double>();

            var file =
                new StreamReader(path);

            for (var i = 0; i < 2; i++)
                left.Add(Convert.ToDouble(file.ReadLine()));
            for (var i = 0; i < 2; i++)
                right.Add(Convert.ToDouble(file.ReadLine()));
            var formula = file.ReadLine();
            file.Close();
            return new CustomFunction(left, right, formula);
        }
       
        public static IFunction BuildFromStream(Stream path)
        {
            var left = new List<double>();
            var right = new List<double>();
            string formula;

            var file =
                new StreamReader(path);

            for (var i = 0; i < 2; i++)
                left.Add(Convert.ToDouble(file.ReadLine()));
            for (var i = 0; i < 2; i++)
                right.Add(Convert.ToDouble(file.ReadLine()));
            formula = file.ReadLine();
            file.Close();
            return new CustomFunction(left, right, formula);
        }
       
        public static IFunction BuildFromTextInput(List<double> left, List<double> right, string expression)
        {
            return new CustomFunction(left, right, expression);
        }
       
        public static IFunction BuildFromDLL(string pathToDll, string pathToConfig = "")
        {
            var p = new ProblemManager.ProblemManager();

            if (p.LoadProblemLibrary(pathToDll) != ProblemManager.ProblemManager.OK_)
                throw new ArgumentException("Can not load lib");

            var problem = p.GetProblem();
            problem.SetDimension(2);
            problem.SetConfigPath(pathToConfig);
            problem.Initialize();
            return new CustomFunctionDLL(problem);
        }
    }
}