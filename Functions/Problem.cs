﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Functions;

namespace Algorithms
{
    [Serializable]
    public class Problem : IProblem
    {

        private int _dimension;
        private string _configPath;
        private readonly List<double> _lowerBound;
        private readonly List<double> _upperBound;
        private readonly double? _optimalValue;
        private readonly List<double> _optimalPoint;
        private int _numberOfFunctions;
        private int _numberOfConstratins;
        private int _numberOfCriterions;
        private readonly List<IFunction> _criterions;
        private readonly List<IFunction> _constraints;
        private List<IFunction> _allFunctionals;
        private string _name;
        private string _dllPath;


        public Problem(string configPath, List<double> lowerBound, List<double> upperBound, double? optimalValue, List<double> optimalPoint, List<IFunction> criterions, List<IFunction> constraints)
        {
            _configPath = configPath;
            _lowerBound = lowerBound;
            _upperBound = upperBound;
            _optimalValue = optimalValue;
            _optimalPoint = optimalPoint;
            _criterions = criterions;
            _constraints = constraints;
            Initialize();
        }

        public int Dimension
        {
            get { return _dimension; }
            set { _dimension = value; }
        }

        public string ConfigPath
        {
            get { return _configPath; }
            set { _configPath = value; }
        }

        public string DllPath
        {
            get { return _dllPath; }
            set { _dllPath = value; }
        }

        public List<double> LowerBound
        {
            get { return _lowerBound; }
        }

        public List<double> UpperBound
        {
            get { return _upperBound; }
        }

        public double? OptimalValue
        {
            get { return _optimalValue; }
        }

        public List<double> OptimalPoint
        {
            get { return _optimalPoint; }
        }

        public double GetOptimalValue(int index)
        {
            return _allFunctionals[index].Calc(OptimalPoint);
        }

        public void Initialize()
        {
            _numberOfConstratins = _constraints.Count;
            _numberOfCriterions = _criterions.Count;
            _numberOfFunctions = _numberOfConstratins + _numberOfCriterions;
            _allFunctionals = new List<IFunction>(NumberOfFunctions);
            _allFunctionals.AddRange(_constraints);
            _allFunctionals.AddRange(_criterions);
            _dimension = LowerBound.Count;
            foreach (var func in Functionals)
            {
                func.Left = LowerBound.GetRange(0, func.Dimension);
                func.Right = UpperBound.GetRange(0, func.Dimension);
            }
        }

        public int NumberOfFunctions
        {
            get { return _numberOfFunctions; }
        }

        public int NumberOfConstratins
        {
            get { return _numberOfConstratins; }
        }

        public int NumberOfCriterions
        {
            get { return _numberOfCriterions; }
        }

        public double CalculateFunctionals(List<double> y, int fNumber)
        {
            return _allFunctionals[fNumber].Calc(y);
        }

        public List<IFunction> Criterions
        {
            get { return _criterions; }
        }

        public List<IFunction> Constraints
        {
            get { return _constraints; }
        }

        public List<double> CalculateCriterions(List<double> x)
        {
            return Criterions.Select(f => f.Calc(x)).ToList();
        }

        public List<double> CalculateConstraints(List<double> toList)
        {
            return Constraints.Select(f => f.Calc(toList)).ToList();
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public List<IFunction> Functionals => _allFunctionals;
    }
}
