﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Absolut.Functions
{
    class Function_xy : IFunction
    {
        public List<double> Left { get; set; }
        public List<double> Right { get; set; }
        public List<double> Left1 { get; set; }
        public List<double> Right1 { get; set; }
        public double Calc(List<double> x)
        {
            return x[0] * x[1];
        }

        public Function_xy()
        {
            Left = new List<double>();
            Right = new List<double>();
            Left1 = new List<double>();
            Right1 = new List<double>();
            Left.Add(-1.0);
            Right.Add(1.0);
            Left1.Add(-1.0);
            Right1.Add(1.0);
                       
        }

    }
}
