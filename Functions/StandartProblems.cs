﻿using System;
using System.Collections.Generic;
using System.Runtime.Remoting.Messaging;
using Algorithms;
using Functions.Functions;

namespace Functions
{
    [Serializable]
    class RosenbrokCubeLine : Problem
    {
        private static IFunction _firstConstraint =
            new MultidimFunction(list => (list[0] - 1) * (list[0] - 1) * (list[0] - 1) - list[1] + 1)
                {Dimension = 2};

        private static IFunction _secondConstraint = new MultidimFunction(list => list[0] + list[1] - 2)
        {
            Dimension =  2
        };

        private static IFunction _criteria =
            new MultidimFunction(x => (1 - x[0]) * (1 - x[0]) + 100 * (x[1] - x[0] * x[0]) * (x[1] - x[0] * x[0]))
            {
                Dimension = 2
            };
        

        public RosenbrokCubeLine() : base(null, new List<double>() {-1.5, -0.5},
            new List<double>() {1.5, 2.5}, 0, new List<double>() {1, 1}, new List<IFunction>() {_criteria},
            new List<IFunction>() {_firstConstraint, _secondConstraint})
        {
            Dimension = 2;
        }
    }

    [Serializable]
    class RosenbrokDisk : Problem
    {
        private static IFunction _firstConstraint =
           new MultidimFunction(list => list[0]*list[0] + list[1] * list[1]);
        private static IFunction _criteria =
            new MultidimFunction(x => (1 - x[0]) * (1 - x[0]) + 100 * (x[1] - x[0] * x[0]) * (x[1] - x[0] * x[0]));

        public RosenbrokDisk() : base(null, new List<double>() {-1.5, -1.5},
            new List<double>() {1.5, 1.5}, 0, new List<double>() {1, 1}, new List<IFunction>() {_criteria},
            new List<IFunction>() {_firstConstraint})
        {
        }
    }


}