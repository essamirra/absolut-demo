﻿using System;
using System.Collections.Generic;

namespace Functions
{
    [Serializable]
    public class FixedByFormulaFunction : MultidimFunction
    {
        private readonly Dictionary<int, IFunction> _fixedVars;
        private readonly IFunction _originalFunction;

        public FixedByFormulaFunction(IFunction originalFunction, Dictionary<int, IFunction> fixedVars)
        {
            _originalFunction = originalFunction;
            _fixedVars = fixedVars;
            List<double> newLeft = new List<double>(), newRight = new List<double>();
            for (var i = 0; i < RealDim; i++)
            {
                if (_fixedVars.ContainsKey(i))
                    continue;
                newLeft.Add(_originalFunction.Left[i]);
                newRight.Add(_originalFunction.Right[i]);
            }
            Left = newLeft;
            Right = newRight;
        }

        private int RealDim => _originalFunction.Left.Count;

        public override double Calc(List<double> arg)
        {
            var newArgs = new List<double>();
            var existingArgCounter = 0;
            for (var i = 0; i < RealDim; i++)
                if (_fixedVars.ContainsKey(i))
                {
                    newArgs.Add(_fixedVars[i].Calc(arg));
                }
                else
                {
                    newArgs.Add(arg[existingArgCounter]);
                    existingArgCounter++;
                }
            return _originalFunction.Calc(newArgs);
        }
    }

    public class FixedByValuesFunction : MultidimFunction
    {
        private readonly Dictionary<int, double> _fixedVars;
        private readonly IFunction _originalFunction;

        public FixedByValuesFunction(IFunction originalFunction, Dictionary<int, double> fixedVars)
        {
            _originalFunction = originalFunction;
            _fixedVars = fixedVars;
            List<double> newLeft = new List<double>(), newRight = new List<double>();
            for (var i = 0; i < RealDim; i++)
            {
                if (_fixedVars.ContainsKey(i))
                    continue;
                newLeft.Add(_originalFunction.Left[i]);
                newRight.Add(_originalFunction.Right[i]);
            }
            Left = newLeft;
            Right = newRight;
        }

        private int RealDim => _originalFunction.Left.Count;

        public override double Calc(List<double> arg)
        {
            var newArgs = new List<double>();
            var existingArgCounter = 0;
            for (var i = 0; i < RealDim; i++)
                if (_fixedVars.ContainsKey(i))
                {
                    newArgs.Add(_fixedVars[i]);
                }
                else
                {
                    newArgs.Add(arg[existingArgCounter]);
                    existingArgCounter++;
                }
            return _originalFunction.Calc(newArgs);
        }
    }
}