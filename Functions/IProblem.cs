﻿using System;
using System.Collections.Generic;
using Functions;

namespace Algorithms
{
    public interface IProblem
    {
        int Dimension { get; set; }
        string ConfigPath { get; set; }
        string DllPath { get; set; }
        List<double> LowerBound { get; }
        List<double> UpperBound { get; }
         List<IFunction> Criterions { get; }
        List<IFunction> Constraints { get; }
        List<IFunction> Functionals { get; }
        double? OptimalValue { get; }
        List<double> OptimalPoint { get; }
        double GetOptimalValue(int index);
        void Initialize();
        int NumberOfFunctions { get; }
        int NumberOfConstratins { get; }
        int NumberOfCriterions { get; }
        double CalculateFunctionals(List<double> y, int fNumber);
        List<double> CalculateCriterions(List<double> x);

        List<double> CalculateConstraints(List<double> toList);
        string Name { get; set; }
    }
}